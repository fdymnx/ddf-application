module.exports = {
  // Respect "browser" field in package.json when resolving modules
  // browser: true,
  // resolver: 'browser-resolve',
  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // A map from regular expressions to module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '\\.(css|less|md)$': 'identity-obj-proxy'
  },
  // An array of regexp pattern strings that are matched against all test paths, matched tests are skipped
  testPathIgnorePatterns: ['/node_modules/', '/__gql_mocks__/'],

  setupFilesAfterEnv: ['./jest/setup.js'],
  transform: {
    '^.+\\.js$': './jest/babelRootModeUpwardTransform.js',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/jest/staticFileAssetTransform.js',
  },
};
