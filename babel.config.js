const regeneratorRuntime = require('regenerator-runtime');

module.exports = function (api) {
  api.cache(true);

  const presets = ['@babel/preset-env', '@babel/preset-react'];
  const plugins = [];

  return {
    presets,
    plugins,
    sourceMaps: 'inline',
    ignore: [
      ".pnp.js",
      process.env.NODE_ENV !== 'test' ? '**/*.test.js' : null
    ].filter((n) => n),
  };
};
