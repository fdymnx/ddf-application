import {getFilesDirectory, HEADER_CODE, removeFile, writeDirectory, writeFile} from './common';


const upperFirstCase = (string) => string.charAt(0).toUpperCase() + string.slice(1);


/**
 * Generate a Synaptix create schema code for modelName in parameter
 *
 * @param {model} model
 */
const buildSchemaCreateMutationCode = (namespace, model) => {
  const result = `${HEADER_CODE}

import {generateCreateMutationDefinitionForType, generateCreateMutationResolverForType} from "@mnemotix/synaptix.js";

export let Create${model.name} = generateCreateMutationDefinitionForType('${model.name}');
export let Create${model.name}Resolvers = generateCreateMutationResolverForType('${model.name}');

`;

  return result;
};


/**
 * Generate a Synaptix update schema code for modelName in parameter
 *
 * @param {model} model
 */
const buildSchemaUpdateMutationCode = (namespace, model) => {
  const result = `${HEADER_CODE}

import {generateUpdateMutationDefinitionForType, generateUpdateMutationResolverForType} from "@mnemotix/synaptix.js";

export let Update${model.name} = generateUpdateMutationDefinitionForType('${model.name}');
export let Update${model.name}Resolvers = generateUpdateMutationResolverForType('${model.name}');

`;

  return result;
};


/**
 * Build code for index.js file in namespace schema mutations directory
 *
 * @param {*} namespace
 * @param {*} mutationsNames
 */
const buildSchemaMutationsIndexCode = (namespace, mutationsNames) => {
  const result = `${HEADER_CODE}

import {mergeResolvers} from "@mnemotix/synaptix.js";
${mutationsNames.map(name => (
    `import {${name}, ${name}Resolvers} from "./${name}.graphql";`
  )).join('\n')}


export let ${upperFirstCase(namespace)}MutationsResolvers = mergeResolvers(
  ${mutationsNames.map(name => `${name}Resolvers`).join(',\n\t')}
);

export let ${upperFirstCase(namespace)}Mutations = [
  ${mutationsNames.join(',\n\t')}
];

`;

  return result
};


/**
 * Build code for index.js in base schema mutations directory
 *
 * @param {*} namespaces
 */
const buildGlobalSchemaMutationsIndex = (namespaces) => {

  const result = `${HEADER_CODE}

import {mergeResolvers} from "@mnemotix/synaptix.js";
${namespaces.map(ns => (
    `import {${upperFirstCase(ns)}Resolvers, ${upperFirstCase(ns)}Mutations} from "./${ns}";`
  )).join('\n')}

export let Mutations = [
  ${namespaces.map(ns => (`...${upperFirstCase(ns)}Mutations`)).join(',\n\t')}
];

export let MutationsResolverMap = mergeResolvers(
  ${namespaces.map(ns => (`${upperFirstCase(ns)}Resolvers`)).join(',\n\t')}
);

`;

  return result
};


/**
 * Regenerate index.js for JS modules contained in namespace schema mutations directory
 *
 * @param {*} baseDir
 * @param {*} namespace
 *
 */
const regenerateSchemaMutationsIndex = (baseDir, namespace) => {
  const namespaceDir = `${baseDir}/${namespace}/schema/mutations`;
  writeDirectory(namespaceDir);

  // Get JS modules to index in namespac eschema mutations
  let indexedNames = getFilesDirectory(namespaceDir);
  indexedNames = indexedNames
    .filter(fn => fn !== 'index.js')
    .filter(fn => fn.endsWith('.graphql.js'))
    .map(fn => fn.split('.graphql.js')[0]);

  writeFile(`${namespaceDir}/index.js`, buildSchemaMutationsIndexCode(namespace, indexedNames), true);
};


/**
 * Generate schema mutations files corresponding to namespace and model names in argument
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 * @param {*} force
 */
export const generateSchemaMutations = (baseDir, namespace, models, force = false) => {
  const mutationsDir = `${baseDir}/${namespace}/schema/mutations`;
  writeDirectory(mutationsDir);

  // Write Synaptix schema type file for each model and return the ones written
  const writtenCreateMutations = Object.values(models).filter(model =>
    writeFile(`${mutationsDir}/Create${model.name}.graphql.js`, buildSchemaCreateMutationCode(namespace, model), force)
  );
  const writtenUpdateMutations = Object.values(models).filter(model =>
    writeFile(`${mutationsDir}/Update${model.name}.graphql.js`, buildSchemaUpdateMutationCode(namespace, model), force)
  );

  regenerateSchemaMutationsIndex(baseDir, namespace);

  console.log(`Create mutations generated : ${writtenCreateMutations.map(mut => mut.name).join(', ')}`);
  console.log(`Update mutations generated : ${writtenUpdateMutations.map(mut => mut.name).join(', ')}`);
};


/**
 * Remove schema mutations files contained in directory baseDir
 *
 * @param {*} baseDir
 * @param {*} namespace
 * @param {*} models
 */
export const removeSchemaMutations = (baseDir, namespace, models) => {
  Object.values(models).map(model => removeFile(`${baseDir}/${namespace}schema/mutations/Create${model.name}.graphql.js`));
  Object.values(models).map(model => removeFile(`${baseDir}/${namespace}/schema/mutations/Update${model.name}.graphql.js`));

  regenerateSchemaMutationsIndex(baseDir, namespace);
};