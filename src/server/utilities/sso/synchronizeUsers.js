/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import yargs from "yargs";
import ora from "ora";
import dotenv from "dotenv";
import {initSSOCommand} from "./initSSOCommand";

dotenv.config();

process.env.UUID = "index-data";
process.env.RABBITMQ_RPC_TIMEOUT = "360000";

export async function synchronizeUsers(){
  let {
    dataModelPath,
    environmentPath
  } = yargs
    .usage("yarn sso:sync")
    .example("yarn data:index -dm ")
    .option("m", {
      alias: "dataModelPath",
      describe: "Datamodel file location",
      default: "src/server/datamodel/dataModel.js"
    })
    .option("e", {
      alias: "environmentPath",
      describe: "Environment file location",
      default: "src/server/config/environment.js"
    })
    .help("h")
    .alias("h", "help")
    .epilog("Copyright Mnemotix 2019")
    .help().argv;

  const {synaptixSession, ssoApiClient}  = await initSSOCommand({environmentPath, dataModelPath});

  let first = 0, max = 1;
  let users;

  let spinner = ora().start();
  spinner.spinner = "clock";

  do {
    users = await ssoApiClient.getUsers({first, max});
    first += max;

    for(const user of users){
      try {
        spinner.info(`Syncing "${user.getUsername()} (${user.id})".`);
        const userAccount = await synaptixSession.getUserAccountForUser(user);

        if (!userAccount) {
          spinner.info(`UserAccount missing in Graph Store. Synchronyzing...`);
          await synaptixSession.getSSOControllerService().synchronizeUserAccount({
            user,
            requestParams: {
              firstNameAsNickName: true
            }
          });
          spinner.succeed(`Now up to date!`);
        } else {
          spinner.info(`Already up to date in Graph Store.`);
        }
      } catch (e) {
        spinner.fail(e.message);
      }
    }
  } while (users.length > 0);
  spinner.succeed("SSO Synchronized.");
  process.exit(0);
}