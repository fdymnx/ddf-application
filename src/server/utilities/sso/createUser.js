/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import yargs from "yargs";
import ora from "ora";
import dotenv from "dotenv";
import { initSSOCommand } from "./initSSOCommand";

dotenv.config();
process.env.LOG_LEVEL = "DEBUG";

export async function createUser(){
  let spinner = ora().start();
  spinner.spinner = "clock";

  let {
    username,
    email,
    password,
    lastName,
    firstName,
    nickName,
    isTemporaryPassword,
    dataModelPath,
    environmentPath
  } = yargs
    .usage("yarn sso:user:add [options] -u [Username] -p [Password]")
    .option("u", {
      alias: "username",
      describe: "Username",
      demandOption: true,
      nargs: 1
    })
    .option("e", {
      alias: "email",
      describe: "email",
      nargs: 1,
      defaultDescription: "Same as username if not given"
    })
    .option("p", {
      alias: "password",
      describe: "Password",
      demandOption: true,
      nargs: 1
    })
    .option("o", {
      alias: "isTemporaryPassword",
      describe: "Is password temporary and must be reasked",
      default: false,
      nargs: 1
    })
    .option("f", {
      alias: "firstName",
      describe: "First name",
      nargs: 1
    })
    .option("l", {
      alias: "lastName",
      describe: "Last name",
      nargs: 1
    })
    .option("n", {
      alias: "nickName",
      describe: "Nick name",
      nargs: 1
    })
    .option("dm", {
      alias: "dataModelPath",
      describe: "Datamodel file location",
      default: "src/server/datamodel/dataModel.js"
    })
    .option("ep", {
      alias: "environmentPath",
      describe: "Environment file location",
      default: "src/server/config/environment.js"
    })
    .help("h")
    .alias("h", "help")
    .epilog("Copyright Mnemotix 2019")
    .help().argv;

  const { synaptixSession } = await initSSOCommand({
    environmentPath,
    dataModelPath
  });

  if(!nickName && !lastName){
    throw new Error("At least one of following parameters must be defined. 'nickName' or 'lastName'.")
  }
  if (!email) {
    email = username;
  }

  try {
    spinner.info(`Creating user (${username}:${password.replace(/./gi, "*")})`);
    let user = await synaptixSession
      .getSSOControllerService()
      .registerUserAccount({
        username,
        password,
        isTemporaryPassword,
        firstName,
        lastName,
        nickName,
        email
      });
    spinner.succeed(`User created with id : ${user.getId()}`);
  } catch (e) {
    spinner.fail(e.message);
  }

  process.exit(0);
}
