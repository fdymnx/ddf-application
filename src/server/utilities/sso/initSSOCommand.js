/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import path from "path";
import fs from "fs";
import env from "env-var";
import {
  initEnvironment,
  SSOApiClient,
  GraphQLContext
} from "@mnemotix/synaptix.js";
import { generateDataModel } from "../../datamodel/generateDataModel";
import { generateDatastoreAdapater } from "../../middlewares/generateDatastoreAdapter";

/**
 * @param environmentPath
 * @param dataModelPath
 * @return {Promise<{synaptixSession: SynaptixDatastoreRdfSession, ssoApiClient: SSOApiClient}>}
 */
export async function initSSOCommand({ environmentPath, dataModelPath }) {
  const environmentDefinition = require(path.resolve(
    process.cwd(),
    environmentPath
  )).default;
  let extraDataModels;
  let dataModelAbsolutePath = path.resolve(process.cwd(), dataModelPath);

  if (fs.existsSync(dataModelAbsolutePath)) {
    extraDataModels = [
      require(path.resolve(process.cwd(), dataModelAbsolutePath)).dataModel
    ];
  }

  initEnvironment(environmentDefinition);

  const dataModel = generateDataModel({
    extraDataModels,
    environmentDefinition
  });

  const ssoApiClient = new SSOApiClient({
    apiTokenEndpointUrl: env
      .get("OAUTH_ADMIN_TOKEN_URL")
      .required()
      .asString(),
    apiEndpointUrl: env
      .get("OAUTH_ADMIN_API_URL")
      .required()
      .asString(),
    apiLogin: env
      .get("OAUTH_ADMIN_USERNAME")
      .required()
      .asString(),
    apiPassword: env
      .get("OAUTH_ADMIN_PASSWORD")
      .required()
      .asString()
  });

  /** @type {SynaptixDatastoreRdfAdapter} */
  let { datastoreAdapter } = await generateDatastoreAdapater({
    graphMiddlewares: [],
    dataModel,
    ssoApiClient
  });
  /** @type {SynaptixDatastoreRdfSession} */
  let synaptixSession = datastoreAdapter.getSession({
    context: new GraphQLContext({
      anonymous: true
    })
  });

  return {synaptixSession, ssoApiClient}
}
