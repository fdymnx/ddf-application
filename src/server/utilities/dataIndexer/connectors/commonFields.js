export let commonFields = [
  {
    fieldName: "entityId",
    propertyChain: [
      "$self"
    ],
    analyzed: false,
    multivalued: false
  }
];

export let commonMapping = {
  entityId: {
    type: "keyword"
  },
  query: {
    type: "percolator"
  },
  percoLabel : {
    "type": "text",
    "analyzer": "french"
  }
}

export let commonEntityFilter = "!bound(?hasDeletionAction)";
