/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {GraphQLTypeDefinition, LinkDefinition, LabelDefinition, LinkPath, ModelDefinitionAbstract} from "@mnemotix/synaptix.js";
import {VerbDefinition} from "./VerbDefinition";
import {GrammaticalPropertyDefinition} from "../ontolex/GrammaticalPropertyDefinition";

export class VerbalInflectionDefinition extends ModelDefinitionAbstract {
  /**
   * Method to simulate multiple inheritance.
   *
   * @return  {typeof ModelDefinitionAbstract[]}  List of parent definitions to inherit from.
   */
  static getParentDefinitions() {
    return [VerbDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ddf:VerbalInflection";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return super.getIndexType();
  }

  /**
 * @inheritDoc
 */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'mood',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:hasMood',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,      
        graphQLInputName: "moodInput"
      }),
      new LinkDefinition({
        linkName: 'tense',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:hasTense',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,
        graphQLInputName: "tenseInput"
      }),
      new LinkDefinition({
        linkName: 'person',
        symmetricLinkName: 'lexicalEntries',
        rdfObjectProperty: 'ddf:hasPerson',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,
        graphQLInputName: "personInput"
      })
    ]
  }


  static getLabels() {
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "moodLabels",
        isPlural: true,
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("mood")
          })
          .property({
            propertyDefinition: GrammaticalPropertyDefinition.getLabel("prefLabel")
          })
      }), 
      new LabelDefinition({
        labelName: "tenseLabels",
        isPlural: true,
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("tense")
          })
          .property({
            propertyDefinition: GrammaticalPropertyDefinition.getLabel("prefLabel")
          })
      }),
      new LabelDefinition({
        labelName: "personLabels",
        isPlural: true,
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("person")
          })
          .property({
            propertyDefinition: GrammaticalPropertyDefinition.getLabel("prefLabel")
          })
      })
      
    ]
  }

}

