/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {GraphQLTypeDefinition, LinkDefinition, LabelDefinition, ModelDefinitionAbstract, LinkPath} from "@mnemotix/synaptix.js";
import { LexicalEntryDefinition } from "./LexicalEntryDefinition";
import { GrammaticalPropertyDefinition } from "./GrammaticalPropertyDefinition";

export class WordDefinition extends ModelDefinitionAbstract {
  /**
   * Method to simulate multiple inheritance.
   *
   * @return  {typeof ModelDefinitionAbstract}  List of parent definitions to inherit from.
   */
  static getParentDefinitions(){
    return [LexicalEntryDefinition];
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static isExtensible() {
    return true;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "ontolex:Word";
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: 'partOfSpeech',
        symmetricLinkName: "lexicalEntries",
        rdfObjectProperty: 'ddf:hasPartOfSpeech',
        relatedModelDefinition: GrammaticalPropertyDefinition,
        isCascadingUpdated: true,
        isCascadingRemoved: true,
        isPlural: false,
        relatedInputName: "partOfSpeechInput"
      })
    ]
  }

  static getLabels(){
    return [
      ...super.getLabels(),
      new LabelDefinition({
        labelName: "partOfSpeechLabels",
        isPlural: true,
        linkPath: new LinkPath()
          .step({
            linkDefinition: this.getLink("partOfSpeech")
          })
          .property({
            propertyDefinition: GrammaticalPropertyDefinition.getLabel("prefLabel")
          })
      })
    ]
  }
}