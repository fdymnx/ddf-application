import {
  GraphQLTypeDefinition,
} from '@mnemotix/synaptix.js';
import {getLinkedGeonamesPlaceResolver} from "@mnemotix/synaptix-api-toolkit-geonames";
import {PersonDefinition} from "../PersonDefinition";

export class PersonGraphQLDefinition extends GraphQLTypeDefinition {
  static getExtraResolvers(){
    return {
      Person: {
        /**
         * @param _
         * @param {string} formQs
         * @param {object} args
         * @param {SynaptixDatastoreSession} synaptixSession
         */
        place: getLinkedGeonamesPlaceResolver.bind(this, PersonDefinition.getLink("place"))
      }
    }
  }
}