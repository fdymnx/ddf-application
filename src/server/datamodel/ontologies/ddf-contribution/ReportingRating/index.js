/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {AdvertisementReportingDefinition} from "./AdvertisementReportingDefinition";
import {ContributorBullyingReportingDefinition} from "./ContributorBullyingReportingDefinition";
import {CopyrightInfringementReportingDefinition} from "./CopyrightInfringementReportingDefinition";
import {DiscriminationInducementReportingDefinition} from "./DiscriminationInducementReportingDefinition";
import {LayoutReportingDefinition} from "./LayoutReportingDefinition";
import {MeaninglessContentReportingDefinition} from "./MeaninglessContentReportingDefinition";
import {PrivacyInfringementReportingDefinition} from "./PrivacyInfringementReporting";
import {SlanderReportingDefinition} from "./SlanderReportingDefinition";
import {UncategorizedReportingDefinition} from "./UncategorizedReportingDefinition";
import {ViolenceThreatReportingDefinition} from "./ViolenceThreatReportingDefinition";

export const ReportingRatingSubDefinitions = {
  AdvertisementReportingDefinition,
  ContributorBullyingReportingDefinition,
  CopyrightInfringementReportingDefinition,
  DiscriminationInducementReportingDefinition,
  LayoutReportingDefinition,
  MeaninglessContentReportingDefinition,
  PrivacyInfringementReportingDefinition,
  SlanderReportingDefinition,
  UncategorizedReportingDefinition,
  ViolenceThreatReportingDefinition
}