import React, { useState, useEffect } from 'react';
import { useQuery } from '@apollo/client';
import { MainView } from './MainView';
import { getUserAuthenticationService } from './services/UserAuthenticationService';
import { gqlEnvironmentQuery } from './hooks/useEnvironment';
import { UserContext } from './hooks/UserContext';
import { useObservable } from './hooks/useObservable';

export default function DDFApplication(props) {
  const { data: envData } = useQuery(gqlEnvironmentQuery);

  const currentUser = getUserAuthenticationService().currentUser;
  const { user: useObservableUser, isLogged, loading } = useObservable(currentUser, { loading: true });
  const [user, setUser] = useState({ isLogged, ...useObservableUser });
  const [isLoadingUser, setIsLoadingUser] = useState(true); // to not redirect in router while user is being loaded


  useEffect(() => {
    setUser({isLogged, ...useObservableUser});
    if (isLoadingUser !== !!loading) {
      setIsLoadingUser(!!loading);
    }
  }, [useObservableUser, isLogged, loading]);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <MainView envData={envData} user={user} isLoadingUser={isLoadingUser} />
    </UserContext.Provider>
  );
}
