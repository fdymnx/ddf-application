/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {ApolloProvider} from '@apollo/client';
import {CookiesProvider} from 'react-cookie';
import {HelmetProvider} from 'react-helmet-async';

import DDFApplication from './DDFApplication';
import {i18nInitialize} from './utilities/i18nInitialize';
import {dayjsInitialize} from './utilities/dayjsInitialize';
import {getDDFApolloClient} from './services/DDFApolloClient';
import {DdfLoadingSplashScreen} from './components/general/DdfLoadingSplashScreen';
import {ScrollToTop} from './utilities/ScrollToTop';
import {ErrorBoundary} from './components/general/ErrorBoundary';
import {BrowsingHistoryHelperProvider} from './services/BrowsingHistoryHelperService';
import {disableReactDevTools} from '@fvilers/disable-react-devtools';
import {SnackbarProvider} from 'notistack';
import {MatomoProvider} from "./hooks/matomo";
import './assets/stylesheets/main.css';
import './assets/images/ddf_logo_squared.png';

let reactRootElement = document.getElementById('react-root');

if (location.path !== "localhost") {
  disableReactDevTools();
}


i18nInitialize();
dayjsInitialize();

ReactDOM.render(
  <BrowserRouter>
    <BrowsingHistoryHelperProvider>
      <MatomoProvider>
        <HelmetProvider>
          <CookiesProvider>
            <SnackbarProvider
              maxSnack={4}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
            >
              <ApolloProvider client={getDDFApolloClient()}>
                <React.Suspense fallback={<DdfLoadingSplashScreen />}>
                  <ErrorBoundary>
                    <ScrollToTop>
                      <DDFApplication />
                    </ScrollToTop>
                  </ErrorBoundary>
                </React.Suspense>
              </ApolloProvider>
            </SnackbarProvider>
          </CookiesProvider>
        </HelmetProvider>
      </MatomoProvider>
    </BrowsingHistoryHelperProvider>
  </BrowserRouter>,
  reactRootElement
);
