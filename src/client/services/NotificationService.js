import {NotificationService} from '@mnemotix/synaptix-client-toolkit';


export const notificationService = new NotificationService({
  yesDefaultText: "Oui",
  noDefaultText: "Non"
});