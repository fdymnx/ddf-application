Conditions Générales d’Utilisation
======================================

1- Objet
---------

Les présentes Conditions Générales d’Utilisation ont pour objet de
définir les modalités d’accès, d’utilisation et de réutilisation des
contenus et données présents dans le *Dictionnaire des francophones*.

2- Définitions
--------------

« *Dictionnaire des francophones* » désigne le site internet accessible
à l’adresse :
[*http://www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles dénommées « *Dictionnaire des
francophones* » et disponibles sur les plates-formes Google Play et
Apple Store.

« Utilisateur » : désigne toute personne physique accédant au
*Dictionnaire des francophones* et/ou utilisant une des offres
éditoriales ou services qui y sont associés.

« Partenaire(s) » : désigne tout partenaire participant au projet du
*Dictionnaire des francophones*.

« Contenus » : désigne l’ensemble des données et plus généralement des
informations diffusées sur le *Dictionnaire des francophones*.

« Services » : désigne l’une ou l’ensemble des fonctionnalités fournies
sur le site et accessibles en ligne à partir du *Dictionnaire des
francophones*.


3- Avertissement
----------------

Les Conditions Générales d’Utilisation applicables sont celles dont la
version en vigueur est accessible en ligne sur le site du *Dictionnaire
des francophones* à l’adresse suivante et à la date de la connexion de
l’utilisateur :
[*http://www.dictionnairedesfrancophones.org/cgu*](http://www.dictionnairedesfrancophones.org/cgu).

4- Accès
--------

Toute personne accède gratuitement au site internet *Dictionnaire des francophones* ainsi qu’au téléchargement de l’application « *Dictionnaire des francophones* », à ses mises à jour et à l’intégralité de ses fonctionnalités depuis la boutique d’applications présente sur
son appareil mobile. Il n’y a pas de contenus ou de fonctionnalités payantes (achats intégrés) sur le site internet ni dans
l’application « *Dictionnaire des francophones* ».

5- Propriété intellectuelle et réutilisation
---------------------------------------------


**5.1 Structure générale et code de l’application**

L’infrastructure logicielle du site internet « *Dictionnaire des
francophones* » est basée sur du code source mis à disposition sous
licence libre. La licence utilisée est la licence Apache, version 2.0 :
[*http://www.apache.org/licenses/LICENSE-2.0*](http://www.apache.org/licenses/LICENSE-2.0).

Les applications « *Dictionnaire des francophones* » proposées sur
Google Play et Apple Store sont basées sur du code source mis à
disposition sous licence libre. La licence utilisée est la licence
Apache, version 2.0 :
[*http://www.apache.org/licenses/LICENSE-2.0*](http://www.apache.org/licenses/LICENSE-2.0).

Les logos « *Dictionnaire des francophones* » et « DDF » ainsi que la
charte graphique et les différents pictogrammes utilisés sur le site
internet et les applications mobiles du *Dictionnaire des francophones*
sont mises à disposition du public sous la licence libre Creative
Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

**5.2 Marques**

Les marques de l’Institut international pour la Francophonie, de
l’université Jean Moulin Lyon 3, du ministère de la Culture et de
leurs Partenaires ainsi que leurs logos sont des marques protégées par
un droit de propriété industrielle.

Toute reproduction totale ou partielle de ces marques et de leur logo
sans l’autorisation expresse de leurs ayants droit est donc strictement
interdite et constitue un délit de contrefaçon au sens du code de la
propriété intellectuelle.

**5.3 Contenus textuels**

Tous les contenus de nature textuelle (termes, définitions, nomenclature
et tout autre renseignement associé aux entrées) proposés sur le
*Dictionnaire des francophones*, autre que les catégories de données
énumérées à l’article 5.6, sont mis à disposition du public selon les
termes de la licence libre Creative Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

Cela signifie notamment que tout Utilisateur est libre d’accéder aux
données, de les réutiliser, de les modifier, de les rediffuser, sous
réserve notamment des conditions suivantes :


* Attribution : Vous devez créditer l’Œuvre, intégrer un lien vers la
licence et indiquer si des modifications ont été effectuées à l’Œuvre.
Vous devez indiquer ces informations par tous les moyens raisonnables,
sans toutefois suggérer que l’Offrant vous soutient ou soutient la façon
dont vous avez utilisé son Œuvre.

* Partage dans les Mêmes Conditions : Dans le cas où vous effectuez un
remix, que vous transformez, ou créez à partir du matériel composant
l’Œuvre originale, vous devez diffuser l’Œuvre modifiée dans les mêmes
conditions, c’est-à-dire avec la même licence avec laquelle l’Œuvre
originale a été diffusée.

**5.4 Contributions des Utilisateurs au Dictionnaire des francophones**

Les Utilisateurs qui contribuent au *Dictionnaire des francophones*
acceptent expressément de placer ces contributions sous la licence libre
Creative Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/).

L’ensemble des contributions des Utilisateurs sont mises à disposition
du public sous la licence Creative Commons CC-BY-SA 3.0.

**5.5 Contenus sonores et audiovisuels**

Tous les contenus de nature sonore ou audiovisuelle (prononciations et
enregistrements sonores des termes proposés notamment) proposés sur le
*Dictionnaire des francophones*, sont mis à disposition du public selon
les termes de la licence libre Creative Commons CC-BY-SA 3.0 :
[*https://creativecommons.org/licenses/by-sa/3.0/fr/*](https://creativecommons.org/licenses/by-sa/3.0/fr/) ou plus permissive, selon les crédits associés à chaque fichier.

**5.6 Contenus culturels et données dictionnairiques produits par les partenaires de l’Université Jean Moulin Lyon 3 et présentés dans le cadre du Dictionnaire des francophones**

Par exception à l’article 5.3 des présentes conditions générales
d’utilisation, les données suivantes proposées par certains Partenaires
de l’Université Jean Moulin Lyon 3, sont soumises à des conditions d’utilisation
particulières :

-   Les données issues de la *Base de données lexicographiques panfrancophone* (BDLP), éditée par l’université Laval à Québec ([*www.bdlp.org*](http://www.bdlp.org)) ;

-   Les données issues du *Dictionnaire des régionalismes français* (DRF), édité par le laboratoire ATILF-CNRS ([*www.atilf.fr*](http://www.atilf.fr)) ;

-   Les données issues du *Grand Dictionnaire terminologique* (GDT), édité par l’Office québécois de la langue française ([*www.oqlf.gouv.qc.ca/*](http://www.oqlf.gouv.qc.ca/)) ;

-   Les données issues du *Dictionnaire des Belgicismes*, édité en 1994 par un collectif d’auteurs sous la coordination de Mme Lenoble-Pinson.

Les conditions d’utilisation de ces données sont les suivantes :

- Les données sont mises à disposition du public selon les termes de la
licence Creative Commons CC-BY-NC-ND 4.0
([*https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr*](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr)) ;

- La reproduction des données des Partenaires respecte leur intégrité et
la présentation qui en est faite sur le *Dictionnaire des francophones* ;

- Lorsqu’elles sont réutilisées, les données des Partenaires sont
toujours accompagnées de la mention claire et explicite de leur source
et d’un lien hypertexte renvoyant au site (externe) de l’auteur (lorsque
celui-ci est disponible), afin que l’Utilisateur puisse accéder à
davantage d’informations ;

- Les données des Partenaires ne peuvent pas être téléchargées dans leur
intégralité (nomenclature complète, extractions automatisées, etc.) à
partir du *Dictionnaire des francophones, ce qui les rends indisponibles dans le point d’accès SPARQL ;

- Les données des Partenaires ne peuvent pas être versées (cédées, transférées) dans des projets ou bases de données autres que le *Dictionnaire des francophones*, sans un accord préalable auprès de l’auteur ;

- Les données des Partenaires sont accessibles au sein du *Dictionnaire
des francophones*, sur tous supports actuels ou à venir (site internet,
site pour mobile, applications mobiles pour App Store et Google Play) et
sont réutilisables à des fins pédagogiques ou non commerciales ;

- Les utilisations commerciales des données des Partenaires par des
tiers requièrent un accord préalable auprès de leurs auteurs.

6- Liens hypertextes
--------------------

**6.1 Liens sortants du Dictionnaire des francophones**

Le *Dictionnaire des francophones* fournit des liens hypertextes
profonds pointant vers des sites tiers. Ces liens ou références ne
constituent ni une approbation ni une validation de leurs contenus. Le
*Dictionnaire des francophones* ne pourra en aucun cas être tenu
responsable de la teneur des pages du site, et ne sera tenu responsable
d’aucun dommage ou préjudice en découlant. 

Les hyperliens profonds vers d’autres sites sont proposés aux Utilisateurs
uniquement par pure commodité et n’engendre à ce titre aucune obligation
de quelque nature que ce soit à la charge du *Dictionnaire des
francophones*, qui ne dispose d’aucun moyen de contrôle et de
surveillance des sites ainsi référencés ni de leurs évolutions et
mises à jour. Les Utilisateurs doivent se reporter aux mentions légales
et conditions générales d’utilisation des sites des institutions
partenaires.

**6.2 Liens vers le Dictionnaire des francophones**

La création de liens hypertextes en direction d’une page du
*Dictionnaire des francophones* est libre à la condition qu’elle ne
porte pas atteinte aux intérêts matériels ou moraux de l’Université Jean Moulin Lyon 3 et qu’elle ne crée pas de confusion sur la source des Services
et/ou Contenus.

7- Accès aux documents administratifs et réutilisations des informations publiques
----------------------------------------------------------------------------------

Aux termes des dispositions des articles L. 330-1 et R. 330-2 du code
des relations entre le public et l’administration (CRPA), le
responsable de l’accès aux documents administratifs est le
Service des Affaires Juridiques, Générales et des Archives, qu’il
est possible de contacter en envoyant un message à l’adresse suivante :
[*cellule.juridique@univ-lyon3.fr*](mailto:cellule.juridique@univ-lyon3.fr).

Vous pouvez également saisir la personne responsable de l’accès aux
documents administratifs par courrier en écrivant à l’adresse suivante :

Service des Affaires Juridiques, Générales et des Archives\
1C, avenue des Frères Lumière\
CS 78242\
69372 Lyon Cedex 08

8- Responsabilité, contributions et modération
------------------------------------------------

Les informations présentées dans le *Dictionnaire des francophones* sont
données à titre purement indicatif. Elles sont modifiables à tout moment
et sans préavis.

L’Université Jean Moulin Lyon 3 et ses Partenaires ne sauraient en aucun cas
être tenus responsables des opinions éventuellement exprimées dans les
contributions des Utilisateurs, qui n’engagent que leurs auteurs. Il est
précisé que la modération des contributions des utilisateurs est
effectuée *a posteriori* par la communauté d’Utilisateurs du *Dictionnaire
des francophones*.

L’Université Jean Moulin Lyon 3 ne peut être tenue responsable des dommages
directs et indirects, prévisibles et imprévisibles tels que des pertes
de gains ou de profits, pertes de données, pertes de matériel ainsi que
des frais de réparation, récupération ou reproduction résultant de
l’utilisation et/ou de l’impossibilité d’utiliser les services et
contenus de l’application.

Malgré les soins et les contrôles de l’équipe en charge du *Dictionnaire
des francophones*, des erreurs, bogues ou omissions involontaires
peuvent subsister. Si vous souhaitez nous faire part de vos remarques ou
réclamations, vous pouvez nous contacter à l’adresse suivante :

[*contact@dictionnairedesfrancophones.org*](mailto:contact@dictionnairedesfrancophones.org)

9- Données illicites
--------------------

Conformément à la loi pour la confiance dans l’économie numérique du 21
juin 2004, le site et les applications mobiles du *Dictionnaire des
francophones* permettent à tout individu ou usager de signaler tout
contenu susceptible de revêtir les caractères des infractions visées aux
cinquième et huitième alinéas de l’article 24 de la loi du 29 juillet
1881 sur la liberté de presse et aux articles 227-23 et 227-24 du Code
pénal.

Pour nous signaler un tel contenu, nous vous invitons à nous contacter
en envoyant un message à
[*contact@dictionnairedesfrancophones.org*](mailto:contact@dictionnairedesfrancophones.org)
en précisant la localisation exacte au sein du *Dictionnaire des
francophones* du contenu que vous estimez illicite.

10- Loi applicable et juridiction compétente
-------------------------------------------

Les présentes conditions générales d’utilisation sont entièrement
soumises au droit français. L’utilisateur du *Dictionnaire des
francophones* reconnaît la compétence exclusive des tribunaux compétents
de Paris.
