Le *Dictionnaire des francophones* n’est pas qu’un objet numérique, il vise à rassembler une communauté de personnes parlant la langue française partout autour du monde, à faciliter l’apprentissage de la langue, à permettre un réusage de son contenu pour la production de jeux, à orienter vers d’autres ressources utiles et intéressantes sur la langue. 

# Découvertes pédagoqiques

Le CAVILAM, Alliance française de Vichy, propose [six fiches pédagogiques et une application](https://www.leplaisirdapprendre.com/portfolio/decouvrons-le-ddf/) s’appuyant sur le Dictionnaire des francophones. Ces à utiliser dans un cadre scolaire.

Le CAVILAM, en partenariat avec l’OIF et l’Institut de la Francophonie pour l’éducation et la formation propose également [cinq fiches pédagogiques sur l’égalité femmes-hommes](https://reliefh.francophonie.org/resource/notice/ftRZO_8IKuEFFgPwbvyG1A).

Deux présentations en ligne défrichent les possibilités d’un usage pédagogique du Dictionnaire des scientifiques : [présentation de Nadia Sefiane et Magali Foulon-Delcombel](https://www.youtube.com/watch?v=d5UvN8UZGjA) et [présentation par Marie Steffens](https://www.youtube.com/watch?v=56aZEu9tiWU)

# Découvertes ludiques

Le 20 mars 2022, l’application mobile [Exploratio](https://www.geeknplay.fr/exploratio-un-jeu-mobile-pour-approfondir-votre-vocabulaire-francophone/) propose une visite ludique dans la langue française en s’appuyant sur le Dictionnaire des francophones.

# Découvertes scientifiques

L’Institut international pour la Francophonie, le conseil scientifique et le comité de relecture œuvrent à rassembler des chercheurs et chercheuses du monde entier, issus d’une grande diversité de discipline : linguistique, lexicographie, sociolinguistique, informatique, ingénierie des connaissances, sciences politiques, etc.

Une [première journée d’étude s’est déroulée à l’INALCO en octobre 2021](https://blogue.dictionnairedesfrancophones.org/2021/11/18/une-premiere-journee-detude-dediee-au-ddf/), organisée par le groupe [Recherche en lexicographie collaborative](https://relco.org/) avec l’Institut international pour la Francophonie.

# Découvertes culturelles

Le *Dictionnaire des francophones* entre en résonnance avec des opérations culturelles telle que [Dis moi dix mots](https://dismoidixmots.culture.gouv.fr/presentation-de-l-operation/presentation-de-l-operation).

En 2022, diverses créations culturelles ont été produites en lien avec le *Dictionnaire des francophones*, résumée sur [le blogue](https://blogue.dictionnairedesfrancophones.org/2022/12/08/oeuvres-culturelles-inspirees-du-ddf/).
