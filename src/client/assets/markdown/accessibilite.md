Déclaration d’accessibilité RGAA 4.0
====================================

# 0. Définitions
--------------

« *Dictionnaire des francophones* » désigne le site internet accessible
à l’adresse :
[*http://www.dictionnairedesfrancophones.org*](http://www.dictionnairedesfrancophones.org)
ainsi que les applications mobiles dénommées « *Dictionnaire des
francophones* » et disponibles sur les plates-formes Google Play et
Apple Store.

« Utilisateur » : désigne toute personne physique accédant au
*Dictionnaire des francophones* et/ou utilisant une des offres
éditoriales ou services qui y sont associés.

« Partenaire(s) » : désigne tout partenaire participant au projet du
*Dictionnaire des francophones*.

« Contenus » : désigne l’ensemble des données et plus généralement des
informations diffusées sur le *Dictionnaire des francophones*.

« Services » : désigne l’une ou l’ensemble des fonctionnalités fournies
sur le site et accessibles en ligne à partir du *Dictionnaire des
francophones*.

# 1. Droits de reproduction
Ce document est placé sous [licence ouverte 2.0 ou ultérieure](https://www.etalab.gouv.fr/licence-ouverte-open-licence).

Vous êtes libres de :

* Reproduire, copier, publier et transmettre ces informations ;
* Diffuser et redistribuer ces informations ;
* Adapter, modifier, extraire et transformer ces informations, notamment pour créer des informations dérivées ;
* Exploiter ces informations à titre commercial, par exemple en la combinant avec d’autres informations, ou en l’incluant dans votre propre produit ou application.

Ces libertés s’appliquent sous réserve de mentionner la paternité de l’information d’origine : sa source et la date de sa dernière mise à jour. Le réutilisateur peut notamment s’acquitter de cette condition en indiquant un ou des liens hypertextes (URL) renvoyant vers le présent site et assurant une mention effective de sa paternité.

Cette mention de paternité ne doit ni conférer un caractère officiel à la réutilisation de ces informations, ni suggérer une quelconque reconnaissance ou caution par le producteur de l’information, ou par toute autre entité publique, du réutilisateur ou de sa réutilisation.

# 2. Déclaration d’accessibilité
L’Université Jean Moulin Lyon 3 s’engage à rendre ses sites internet, intranet, extranet et ses progiciels accessibles [et ses applications mobiles et mobilier urbain numérique] conformément à l’article 47 de la loi n°2005-102 du 11 février 2005.

À cette fin, il met en œuvre la stratégie et les actions suivantes :

* Schéma pluriannuel de mise en accessibilité 2020-2022 : audit (2020) et mise en conformité (2021), puis corrections selon les retours usagers.
* Actions réalisées en 2019-2020 : première version non publique du site.
* Plan d’actions 2020-2021 : audit d'accessibilité (décembre 2020, [disponible ici](https://docs.google.com/document/d/1rRZV6cvHGnZGWLcwriqVTFwL1v80gQEZ5cmfT3CYyoM/edit?usp=sharing)) et mise en oeuvre de correctifs en juillet 2021 (cf section suivante).

Cette déclaration d’accessibilité s’applique à [https://www.dictionnairedesfrancophones.org/](https://www.dictionnairedesfrancophones.org/) .


<br/>

## 2.1. État de conformité

Un effort particulier a été porté sur l’accessibilité du _Dictionnaire des francophones_ aux personnes en situation de handicap. Le _Dictionnaire des francophones_, [https://www.dictionnairedesfrancophones.org/](https://www.dictionnairedesfrancophones.org/), est en conformité partielle avec le référentiel général d’amélioration de l’accessibilité (RGAA), version 4.0 en raison des non-conformités et des dérogations énumérées ci-dessous.

Certaines fonctionnalités présentées au sein du _Dictionnaire des francophones_ peuvent fonctionner de manière dégradée sur certains
navigateurs adaptés. N’hésitez pas à nous contacter pour tout problème
d’accessibilité rencontré lors de l’utilisation du _Dictionnaire des
francophones_.

<br/>

## 2.2. Résultats des tests
L’audit de conformité réalisé par Mnémotix révèle que :

* **86 %** des critères du RGAA version 4.0 sont respectés
* [Facultatif] Le taux moyen de conformité du site s’élève à 89,2 %
* [Facultatif] Accès à la grille d’audit RGAA [[url pour télécharger la grille d’audit]](https://docs.google.com/spreadsheets/d/1eeW9KuZqKI0H3nydVRfjiyO3VZN5LOyVZE0yMCzZQN8/edit?usp=sharing)



<br/>

## 2.3. Contenus non accessibles

### Non-conformité

* Faible contraste de certains éléments, textes ou images, consistant en du texte blanc sur fond coloré
* Dans chaque page web, l’information n’est pas toujours parfaitement compréhensible lorsque les feuilles de styles sont désactivées
* Dans chaque formulaire, le contrôle de saisie reste perfectible, notamment sur le statut obligatoire de certains champs
* Il manque un lien d’accès rapide ou d’évitement de la zone de contenu principal

### Dérogations pour charge disproportionnée

### Contenus non soumis à l’obligation d’accessibilité

<br/>

## 2.4. Établissement de cette déclaration d’accessibilité
Cette déclaration a été établie le 16/07/2021. 

<br/>

## 2.5. Technologies utilisées pour la réalisation du Dictionnaire des francophones

* HTML5
* CSS
* Javascript


<br/>

### 2.6. Environnement de test
Les vérifications de restitution de contenus ont été réalisées sur la base de la combinaison fournie par la base de référence du RGAA 4.0, avec les versions suivantes :

* Navigateur Chrome version 91.0.4472.114
* Mac OS X 11.2.3


<br/>

## 2.7. Outils pour évaluer l’accessibilité

* WAVE Web Accessibility Evaluation Tool (https://wave.webaim.org/)
* Asqatasun https://asqatasun.org/
* Accessibility Insights for Web https://accessibilityinsights.io/ (carte des zones de focus)


<br/>

## 2.8. Pages du site ayant fait l’objet de la vérification de conformité

<br/>

  - [Accueil](https://www-dev.dictionnairedesfrancophones.org/)
  - [Authentification](https://www-dev.dictionnairedesfrancophones.org/login)
  - [Ajout d’une définition](https://www-dev.dictionnairedesfrancophones.org/sense/new)
  - [Mes contributions](https://www-dev.dictionnairedesfrancophones.org/my_account/contributions)
  - [Mentions légales](https://www-dev.dictionnairedesfrancophones.org/cgu)
  - [Aide](https://www-dev.dictionnairedesfrancophones.org/aide)
  - [Résultats de recherche](https://www-dev.dictionnairedesfrancophones.org/form/pomme)
  - [Détails d’une définition](https://www-dev.dictionnairedesfrancophones.org/form/pomme/sense/wkt%3Asense%2F62ff97cb591da2ad46aecda432a9b26c)

<br/>

## 2.9. Retour d’information et contact
Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez contacter le responsable du _Dictionnaire des francophones_, pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre forme.

* Envoyer un message [url du formulaire en ligne]
* Contacter [Nom de l’entité responsable du service en ligne] [url d’une page avec les coordonnées de l’entité]

<br/>

## 2.10. Voies de recours
Cette procédure est à utiliser dans le cas suivant.

Vous avez signalé au responsable du site internet un défaut d’accessibilité qui vous empêche d’accéder à un contenu ou à un des services du portail et vous n’avez pas obtenu de réponse satisfaisante.

* Écrire un message au Défenseur des droits [[https://formulaire.defenseurdesdroits.fr/](https://formulaire.defenseurdesdroits.fr/)]
* Contacter le délégué du Défenseur des droits dans votre région [[https://www.defenseurdesdroits.fr/saisir/delegues](https://www.defenseurdesdroits.fr/saisir/delegues)]
* Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre)  \
Défenseur des droits \
Libre réponse 71120 \
75342 Paris CEDEX 07


# Optimisation

Le site internet « _Dictionnaire des francophones_ » est conforme aux
spécifications du W3C relatives au standard HTML5.

Il a été conçu pour fonctionner de manière optimale à partir des
navigateurs internet suivants :

- Chrome, v. 77

- Firefox, v. 70

- Safari, v. 12

- Chrome Android, v. 78

- Safari iOS, v. 12


# Raccourcis clavier

- Touche 0 : Page d’accessibilité et liste des raccourcis

- Touche 3 : Plan du site

- Touche 1 : Page d’accueil

- Touche 4 : Formulaire de recherche

- Touche 6 : Pages d’aide

- Touche 9 : Contact

