import React from 'react';
import { useEnvironment } from './useEnvironment';

/**
 * if true hide and disable all buttons to login/create account/add definition or vote etc
 */
export function useIsReadOnly() {
  return useEnvironment('READ_ONLY_MODE_ENABLED', { isBoolean: true });
}

/**
 *  Display a warning texte
 */
export function IsReadOnlyWarning() {
  const isReadOnly = useIsReadOnly();
  if (isReadOnly) {
    return <i style={{ textAlign: 'center', color: 'orange' }}> (En lecture seule) </i>;
  } else {
    return null;
  }
}
