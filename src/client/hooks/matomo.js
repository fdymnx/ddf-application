import React, {useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {MatomoProvider as MtmoProvider, createInstance, useMatomo} from '@datapunt/matomo-tracker-react';
import {getCookieConsentValue} from "react-cookie-consent";
import Config from '../Config';

const prodUrl = "www.dictionnairedesfrancophones.org";
// const prodUrl = "www-test.dictionnairedesfrancophones.org";

// rajout d'un second tracker par le minister de la culture... utilise smarttag.js
// https://developers.atinternet-solutions.com/home/ 
var ATTag;
try {
  ATTag = new ATInternet.Tracker.Tag();
} catch (e) {
  // console.log(e);
}


export const instance = createInstance({
  urlBase: 'https://analyseweb.huma-num.fr/',
  siteId: window.location.hostname.includes(prodUrl) ? 309 : 231
});

export function MatomoProvider({children}) {
  return (
    <MtmoProvider value={instance}>
      <MatomoRouteTracker>{children}</MatomoRouteTracker>
    </MtmoProvider>
  );
}

function MatomoRouteTracker({children}) {
  const history = useHistory();

  const {trackPageView} = useMatomo();
  
  useEffect(() => {
    const useTracking = getCookieConsentValue(Config.cookie.name) === "true" || getCookieConsentValue(Config.cookie.name) === true;
    // track original route ( on first page open , so index or f5 )
    if (useTracking) {
      trackPageView();
    }
  }, []);

  useEffect(() => {
    // track all other route change
    return history.listen((location) => {
      const useTracking = getCookieConsentValue(Config.cookie.name) === "true" || getCookieConsentValue(Config.cookie.name) === true;
      if (useTracking) {
        trackPageView();
        if (ATTag) {
          ATTag.page.send({name: `${location.pathname}${location.search}`});
        }
      }
    });
  }, [history]);

  return <>{children}</>;
}
