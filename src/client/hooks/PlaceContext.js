import React, {useState, useContext, createContext} from 'react';

import {isValidPlace} from '../utilities/helpers/countries';
import {LOCAL_STORAGE_CURRENT_PLACE_KEY} from "../services/GeolocService";





// Use localStorage to store the last place known. Fallback strategy, localize at France
function getCookieCurrentPlace() {
  let place = {};
  if (typeof localStorage !== "undefined") {
    let tmp = localStorage.getItem(LOCAL_STORAGE_CURRENT_PLACE_KEY);
    place = (tmp !== undefined && tmp !== "undefined" && tmp !== null) ? JSON.parse(tmp) : {}
  }
  /*
  if (!isValidPlace(place)) {
    // Default place is France    
    place = {      id: "geonames:3017382",      __typename: "Country",      name: "France",      countryCode: "FR"    };  
  }
  */
  return place;
}

//new value setter for localstorage
export function setCookieCurrentPlace(currentPlace) {
  if (typeof localStorage !== "undefined") {
    localStorage?.setItem(LOCAL_STORAGE_CURRENT_PLACE_KEY, JSON.stringify(currentPlace))
  }
}

// initialize the context with last selected or gelocalized place
const PlaceContext = createContext([getCookieCurrentPlace(), () => { }]);

// the provider avoid to write <PlaceContext.provider /> for main classe
export function PlaceContextProvider({children}) {
  const {Provider} = PlaceContext;
  const [currentPlace, setCurrentPlace] = useState(getCookieCurrentPlace());
  return <Provider value={{currentPlace, setCurrentPlace}}>{children}</Provider>;
}

// à utiliser dans les classes enfants qui veulent récupérer le context
export function usePlaceContext() {
  const {currentPlace, setCurrentPlace} = useContext(PlaceContext);

  const writeCookieAndContext = (currentPlace) => {
    setCookieCurrentPlace(currentPlace);
    setCurrentPlace(currentPlace)
  }

  return {currentPlace, setCurrentPlace: writeCookieAndContext};
}