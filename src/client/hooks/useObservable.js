import { useEffect, useLayoutEffect } from 'react';
import { useState } from 'react';

const isBrowser = typeof window !== 'undefined';

const useIsomorphicLayoutEffect = useEffect;

export function useObservable(observable, initialValue = undefined) {
  const [value, update] = useState(initialValue);

  useIsomorphicLayoutEffect(() => {
    const s = observable.subscribe(update);
    return () => s.unsubscribe();
  }, [observable]);

  return value;
}
