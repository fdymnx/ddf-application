

import {useRouteMatch} from 'react-router-dom';
import {ROUTES} from '../routes';
/**
 * React hook. Returns boolean, true if the "improve DDF" button must be displayed, false otherwise
 */
export function useDdfButton() {
  /** List of routes for which the improve ddf button should not appear */
  return !(
    useRouteMatch(ROUTES.FORM_LEXICAL_SENSE_EDIT) ||    
    useRouteMatch(ROUTES.CREATE_LEXICAL_SENSE) 
    // || useRouteMatch(ROUTES.FORM_LEXICAL_SENSE)
  );
}