import React from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  grid: {
    maxWidth: "800px",
    marginLeft: "auto",
    marginRight: "auto",
    paddingTop: Config.spacings.medium,

    "& .mainContent, .firstSection": {
      width: "100%"
    },
    "& .row": {
      display: "flex"
    }
  },

  wideWidth: {
    maxWidth: "1200px"
  },
  veryWideWidth: {
    maxWidth: "1600px"
  },
  withSideColumn: {
    maxWidth: "1050px",
    "& .mainContent, .firstSection": {
      width: `calc(75% - ${Config.spacings.medium / 2}px)`,
      marginRight: Config.spacings.medium,
    },
    "& .sideColumn": {
      width: `calc(25% - ${Config.spacings.medium / 2}px)`
    }
  }

}));




/**
 * This is a grid to insert into the main desktop layout content section. This grid handles the generic layout for the Ddf desktop pages
 * which can be either : 
 *
 *
 *        +-------------------------------------------+
 *        |                                           |
 *        |    First section                          |
 *        |                                           |
 *        +-------------------------------------------+
 *        |                                           |
 *        |    Content                                |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        |                                           |
 *        +-------------------------------------------+
 *
 *
 * Or : 
 *
 *  +-----------------------------------------+
 *  |                                         |
 *  |     First section                       |
 *  |                                         |
 *  +-----------------------------------------+--------------+
 *  |                                         |              |
 *  |     Content                             | Side         |
 *  |                                         | column       |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  |                                         |              |
 *  +-----------------------------------------+--------------+
 *
 */


/**
 *
 * Usage :
 *
 * <DesktopGrid
 *    sideColumn={SideColumnContent} 
 *    firstSection={FirstSectionContent}
 *    wideWidth={false}
 *    veryWideWidth={false}
 * >
 *   main content
 *
 * </DesktopGrid>
 *
 * If a side column is given, the layout will be 3/4 for main content and 1/4 for side column content,
 * otherwise the layout is a single centered column.
 *
 * If a first section is given, it will be on top of the main content with the same width, but the side column
 * will be aligned with the main content
 * 
 * 
 * wideWidth set to true is used in admin panel to render wide table correctly at 1200px wide
 * veryWideWidth set to true is used in explorer for 1600px wide
 */




DesktopGrid.propTypes = {
  wideWidth: PropTypes.bool,
  veryWideWidth: PropTypes.bool,
  sideColumn: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.bool
  ]),
  firstSection: PropTypes.func,
};
export function DesktopGrid({firstSection, sideColumn, wideWidth = false, veryWideWidth = false, children}) {
  const classes = useStyles();
  return (
    <div className={clsx(classes.grid, sideColumn && classes.withSideColumn, wideWidth && classes.wideWidth, veryWideWidth && classes.veryWideWidth)}>
      <If condition={firstSection}>
        <div className={"firstSection"}>{firstSection()}</div>
      </If>

      <div className={"row"}>
        <div className={"mainContent"}>{children}</div>
        <If condition={sideColumn}>
          <div className={"sideColumn"}>{sideColumn()}</div>
        </If>
      </div>
    </div>
  );
}
