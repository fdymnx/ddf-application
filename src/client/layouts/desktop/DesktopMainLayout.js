import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {DesktopHeader} from './DesktopMainLayout/DesktopHeader';
import {DesktopFooter} from './DesktopMainLayout/DesktopFooter';
import {DesktopGrid} from './DesktopGrid';
import {CookieDisclaimer} from '../../components/general/CookieDisclaimer';
import {displayFooterExplanationText} from "../../components/widgets/FooterExplanationText";
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../Config';


const totalNonContentHeight = Config.heights.desktopHeader + Config.heights.footerLinksBanner + Config.heights.footerStoresLinks + Config.heights.footerPartnersLinks + Config.heights.footerImproveButton;
const totalWithExplanation = totalNonContentHeight + Config.heights.footerFooterExplanationText;


export const useStyles = makeStyles((theme) => ({
  indexPageContent: {
    minHeight: `calc(100vh - ${totalWithExplanation}px)`
  },
  otherPagesContent: {
    minHeight: `calc(100vh - ${totalNonContentHeight}px)`
  }
}));



/**
 *
 * Usage :
 *
 * <DesktopMainLayout>
 *   Content
 * </DesktopMainLayout>
 *
 */

export function DesktopMainLayout({children, wideWidth, veryWideWidth = false, headerHideSearch, useDefaultGrid, currentForm, hideContributionFooter = false}) {
  const classes = useStyles();
  const isIndex = displayFooterExplanationText();

  return (
    <React.Fragment>
      <DesktopHeader hideSearch={headerHideSearch} />
      <main role="main" className={clsx(isIndex ? classes.indexPageContent : classes.otherPagesContent)}>
        {useDefaultGrid ? (
          <DesktopGrid wideWidth={wideWidth} veryWideWidth={veryWideWidth}>{children}</DesktopGrid>
        ) : (
          <>{children}</>
        )}
      </main>

      <If condition={!hideContributionFooter}>
        <DesktopFooter currentForm={currentForm} />
      </If>

      <CookieDisclaimer />
    </React.Fragment>
  );
}


DesktopMainLayout.propTypes = {
  /**
   * If true, the content will be inserted into a DesktopGrid component, with only one column centered. Otherwise,
   * the content will be inserted as it is without being wrapped in a grid
   */
  useDefaultGrid: PropTypes.bool,
  /**
   * This property is passed to DesktopHeader
   */
  headerHideSearch: PropTypes.bool,
  /** The current form written representation, used for the "improve DDF" button */
  currentForm: PropTypes.string,
  hideContributionFooter: PropTypes.bool,
  /** true for admin pages only to display wide table correctly  */
  wideWidth: PropTypes.bool
};