import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Config from '../../Config';
import {BackArrowLink} from '../../components/widgets/BackArrowLink';
import {ShowWhereIAM} from '../../components/widgets/ShowWhereIAM';
import globalStyles from '../../assets/stylesheets/globalStyles.js';
import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  desktopSubHeader: {
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      display: 'none'
    }
  },
  primaryTitle: {
    fontSize: Config.fontSizes.xlarge,
    fontWeight: 'normal',
    marginBottom: Config.spacings.medium,
    color: Config.colors.darkgray
  },
  secondaryTitleRow: {
    display: 'flex',
    justifyContent: 'space-between',
  },

}));


export function DesktopSubHeader({primaryTitle, secondaryTitle, backLinkText, backLinkTo, color, desktopHeaderPrimaryTitleColor}) {
  const gS = globalStyles();
  const classes = useStyles();

  return (
    <div className={clsx(classes.desktopSubHeader, !!color && gS[color])}>
      <If condition={primaryTitle}>
        <ShowWhereIAM path="src/client/layouts/desktop/DesktopSubHeader.js around primaryTitle">
          <h1 className={clsx(classes.primaryTitle, !!desktopHeaderPrimaryTitleColor && gS[desktopHeaderPrimaryTitleColor])}>{primaryTitle}</h1>
        </ShowWhereIAM>
      </If>
      <If condition={secondaryTitle || backLinkText}>
        <ShowWhereIAM path="src/client/layouts/desktop/DesktopSubHeader.js around secondaryTitle">
          <div className={classes.secondaryTitleRow}>
            <div className={gS.secondaryTitle}>{secondaryTitle}</div>
            <If condition={backLinkText && backLinkTo}>
              <BackArrowLink text={backLinkText} to={backLinkTo} />
            </If>
          </div>
        </ShowWhereIAM>
      </If>
    </div>
  );
}

DesktopSubHeader.propTypes = {
  // The primary title
  primaryTitle: PropTypes.node,
  // The secondary title
  secondaryTitle: PropTypes.string,
  // The back link text
  backLinkText: PropTypes.string,
  // The back link target
  backLinkTo: PropTypes.string,
  // A color string among the application color palette as defined in Config.js
  color: PropTypes.string,
  desktopHeaderPrimaryTitleColor: PropTypes.string,
};