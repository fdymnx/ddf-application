import React from 'react';
import {useTranslation} from 'react-i18next';
import {MetaSEO} from "../../components/widgets/MetaSEO";
import {SimpleModalPage} from './SimpleModalPage';
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  root: {
    "& #desktopContent": {
      backgroundColor: Config.colors.purple,
      color: Config.colors.white,
      "& a": {
        color: 'inherit',
        textDecoration: 'underline'
      },
      "& ul": {
        /* for bullet points */
        paddingLeft: '2em'
      },
      "& h1": {
        "&:first-child": {
          marginTop: 0
        },
        marginTop: Config.spacings.medium,
        marginBottom: Config.spacings.small
      },
      "& h2": {
        "&:first-child": {
          marginTop: 0
        },
        marginTop: Config.spacings.medium,
        marginBottom: Config.spacings.small
      },
      "& p": {
        marginBottom: Config.spacings.small
      }
    }
  }
}));

/**
 * Usage :
 *
 * <InformationPage 
 *   title="Mon titre"  // facultatif, dans ce cas passer metaTitle
 *   metaTitle="Mon titre"
 *   content="Mon contenu" 
 *   mobileControls={['close','back]}
 *   onClose={callback}
 *   desktopContentClassname={classes.desktopContent}
 * />
 */
export function InformationPage(props) {
  const {t} = useTranslation();
  const {title, metaTitle} = props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <MetaSEO
        title={t('DOCUMENT_TITLES.GENERIC', {title: title || metaTitle})}
      />
      <div className={classes.root}>
        <SimpleModalPage {...props} />
      </div>
    </React.Fragment>
  );
}




