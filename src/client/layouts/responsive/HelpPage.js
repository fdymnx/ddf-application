import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import clsx from "clsx";
import {MetaSEO} from "../../components/widgets/MetaSEO";
import {ROUTES} from '../../routes';
import {ResponsiveMainLayout} from './ResponsiveMainLayout';
import {HelpMobileHeader} from '../mobile/MobileMainLayout/HelpMobileHeader';
import {DesktopSubHeader} from '../desktop/DesktopSubHeader';
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  content: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingBottom: Config.spacings.medium,

    "& p": {
      marginLeft: -(Config.spacings.medium),
      marginRight: -(Config.spacings.medium),
      padding: Config.spacings.medium,
      backgroundColor: Config.colors.white,
    },
    "& p + p ": {
      paddingTop: 0,
    },

    "& h1": {
      marginLeft: -(Config.spacings.medium),
      marginRight: -(Config.spacings.medium),
      marginTop: Config.spacings.medium,
      color: Config.colors.white,
      fontWeight: Config.fontWeights.semibold,
      fontSize: Config.fontSizes.medium,
      height: "30px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      textAlign: "center",
      backgroundColor: props => Config.colors[props.color] || Config.colors.purple,
    },
    "& h2": {
      backgroundColor: Config.colors.white,
      marginLeft: -(Config.spacings.medium),
      marginRight: -(Config.spacings.medium),
      paddingTop: Config.spacings.medium,
      paddingLeft: Config.spacings.medium,
      paddingRight: Config.spacings.medium,
    },
    "& ul": {
      /* for bullet points */
      // marginBottom: Config.spacings.medium,
      marginLeft: -(Config.spacings.medium),
      marginRight: -(Config.spacings.medium),
      // paddingTop: Config.spacings.medium,
      paddingBottom: Config.spacings.medium,
      paddingLeft: Config.spacings.huge,
      paddingRight: Config.spacings.huge,
      backgroundColor: Config.colors.white,
    },    
    "& h1 + ul ": {
      paddingTop: Config.spacings.medium,
    },

  }
}));
/**
 * Usage :
 *
 * <HelpPage
 *  title="Aide et documentation"
 *  subtitle="Effectuer une recherche simple"
 *  content={<div>My content</div>}
 * />
 *
 */
HelpPage.propTypes = {
  /** The title of the page */
  title: PropTypes.string,
  /** The subtitle of the page */
  subtitle: PropTypes.string,
  /** The title of the help section */
  helpSectionTitle: PropTypes.string,
  /** A React node to insert as the main body of the help page */
  content: PropTypes.node,
  /** A color string among the application color palette as defined in Config.js */
  color: PropTypes.string,
};

export function HelpPage({color, content, title, helpSectionTitle}) {
  const {t} = useTranslation();

  const classes = useStyles({color});

  useEffect(() => {
    try {
      if (window?.location?.hash.length > 1) {
        let anchor = window.location.hash.replace("#", "");

        setTimeout(() => {
          document.getElementById(anchor).scrollIntoView({behavior: "smooth", block: "start", inline: "start"})
        }, 300);
      }
    } catch (error) {
      console.log(error)
    }

  }, []);

  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.HELP')} />
      <ResponsiveMainLayout mobileHeader={renderMobileHeader()} desktopUseDefaultGrid>
        {renderDesktopSubHeader()}
        <div className={clsx(classes.content)}>{content}</div>
      </ResponsiveMainLayout>
    </React.Fragment>
  );

  function renderMobileHeader() {
    return <HelpMobileHeader title={title} search={false} ddfLogo={true} />;
  }

  function renderDesktopSubHeader() {
    return (
      <DesktopSubHeader
        primaryTitle={title}
        secondaryTitle={helpSectionTitle}
        backLinkText={t('HELP.DESKTOP.BACK_LINK')}
        backLinkTo={ROUTES.HELP_INDEX}
        color={color}
      />
    );
  }
}
