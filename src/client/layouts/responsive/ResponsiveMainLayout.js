import React from 'react';
import PropTypes from 'prop-types';

import {MobileMainLayout} from '../mobile//MobileMainLayout';
import {DesktopMainLayout} from '../desktop/DesktopMainLayout';
import {Desktop, Mobile} from '../MediaQueries';


ResponsiveMainLayout.propTypes = {
  /** The element to use as header for the mobile layout. Passed as it is to MobileMainLayout */
  mobileHeader: PropTypes.node,
  /** The element to use as footer for the mobile layout. Passed as it is to MobileMainLayout */
  mobileFooter: PropTypes.node,
  /**
   *  A flag that if provided, will put the props.children content into a DesktopGrid, otherwise the content
   *  will be inserted as is in the desktop layout
   */
  desktopUseDefaultGrid: PropTypes.bool,
  /**
   * This property is passed to DesktopHeader
   */
  desktopHeaderHideSearch: PropTypes.bool,

  /** The current form written representation, used for the "improve DDF" button (desktop layout) */
  currentForm: PropTypes.string,
};

export function ResponsiveMainLayout({
  currentForm,
  children,
  desktopUseDefaultGrid,
  desktopHeaderHideSearch,
  mobileHeader,
  mobileFooter,
}) {
  return (
    <>
      <Desktop>
        <DesktopMainLayout useDefaultGrid={desktopUseDefaultGrid} headerHideSearch={desktopHeaderHideSearch} currentForm={currentForm}>
          {children}
        </DesktopMainLayout>
      </Desktop>
      <Mobile>
        <MobileMainLayout header={mobileHeader} footer={mobileFooter}  >
          {children}
        </MobileMainLayout>
      </Mobile>
    </>
  );
}
