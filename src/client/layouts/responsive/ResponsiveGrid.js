import React from 'react';
import PropTypes from 'prop-types';

import {DesktopGrid} from '../desktop/DesktopGrid';
import {Desktop, Mobile} from '../MediaQueries';

/**
 *
 * Use the DesktopGrid if desktop, otherwise just use the provided children content
 *
 */

ResponsiveGrid.propTypes = {
  desktopSideColumn: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.bool 
  ]),
  desktopFirstSection: PropTypes.func,
};

export function ResponsiveGrid({desktopSideColumn, desktopFirstSection, fabButton, children}) {
  return (
    <>
      <Desktop>
        <DesktopGrid sideColumn={desktopSideColumn} firstSection={desktopFirstSection}>
          {children}
        </DesktopGrid>
      </Desktop>
      <Mobile>
        {children}
        {fabButton}
      </Mobile>
    </>
  );
}
