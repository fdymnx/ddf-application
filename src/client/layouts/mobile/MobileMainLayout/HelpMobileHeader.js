import React from 'react';
import {MobileHeader} from './MobileHeader';

import {makeStyles} from '@material-ui/core/styles';
import Config from '../../../Config';


const useStyles = makeStyles((theme) => ({
  "title": {
    fontSize: Config.fontSizes.medium,
    color: Config.colors.purple,
    fontWeight:  Config.fontWeights.semibold,
    textAlign: "center",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexGrow: 999
  }
}));


export function HelpMobileHeader({title, search, geolocMarker, ddfLogo, backButton, onBack}) {

  const classes = useStyles();

  return (
    <MobileHeader
      search={search}
      geolocMarker={geolocMarker}
      ddfLogo={ddfLogo}
      backButton={backButton}
      onBack={onBack}
      content={renderTitle()}
    />
  );

  function renderTitle() {
    return (
      <If condition={title}>
        <div className={classes.title}>{title}</div>
      </If>
    );
  }
}
