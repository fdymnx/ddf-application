import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {useHistory, Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import Modal from '@material-ui/core/Modal';
import {makeStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';

import {ROUTES} from '../../../routes';
import FormSearchInput from '../../../components/widgets/FormSearchInput';
import {GeolocPicker} from '../../../components/widgets/GeolocPicker';
import {menuService} from '../../../services/MenuService';
import burgerMenuIcon from '../../../assets/images/burger_menu.svg';
import searchIcon from '../../../assets/images/signs/search.svg';
import backArrowPurple from '../../../assets/images/back_arrow_purple.svg';
import geolocMarkerWhiteIcon from '../../../assets/images/geoloc_marker_white.svg';
import ddf_logo from '../../../assets/images/ddf_logo.svg';
import Config from '../../../Config';


MobileHeader.propTypes = {
  search: PropTypes.bool,
  geolocMarker: PropTypes.bool,
  currentForm: PropTypes.string,
  currentPlace: PropTypes.object,
  content: PropTypes.node,
  ddfLogo: PropTypes.bool,
  showBackIcon: PropTypes.bool
};

const mobileHeaderDefaultHeight = "60px";
const menuButtonDimension = 18;

const useStyles = makeStyles((theme) => ({
  geoContainer: {
    minHeight: Config.heights.mobileHeader,
    backgroundColor: Config.colors.lightgray,
    display: "flex",
    alignItems: "center",
    padding: '10px'
  },
  container: {
    height: mobileHeaderDefaultHeight,
    backgroundColor: Config.colors.white,
    paddingRight: Config.spacings.medium,
    paddingLeft: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    display: "flex",
    alignItems: "center"
  },
  actions: {
    display: "flex",
    alignItems: "center",
  },
  postContent: {
    justifyContent: "flex-end",
    flexShrink: 0,
    flexGrow: 1
  },
  postContent100: {
    width: "100%",
    backgroundColor: Config.colors.orange
  },
  withSearchClosed: {
    position: "relative",
    display: "flex",
    justifyContent: "space-between",
    flexShrink: 0,
    flexGrow: 1,
    zIndex: 0,
  },
  withSearchOpened: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    height: mobileHeaderDefaultHeight
  },

  greyCircle: {
    backgroundColor: `${Config.colors.lightgray} !important`
  },
  ddfLogo: {
    width: "60px",
    "& img": {
      width: "100%",
      height: "100%"
    }
  },
  backlogo: {
    width: "14px",
    marginRight: "16px",
    "& img": {
      width: "100%",
      height: "100%"
    }
  },

  purpleCircle: {
    marginRight: Config.spacings.small,
    backgroundColor: Config.colors.purple,
    height: "30px",
    width: "30px",
    borderRadius: "50%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },

  searchButton: {
    cursor: 'pointer',
    "& img": {
      width: "18px",
      height: "18px"
    }
  },
  geolocButton: {
    cursor: 'pointer',
    "& img": {
      width: "18px",
      height: "18px"
    }
  },
  menuButton: {
    width: `${menuButtonDimension + Config.spacings.small * 2}px`,
    height: `${menuButtonDimension}px`,
    background: "none",
    border: "none",
    paddingRight: Config.spacings.small,
    paddingLeft: Config.spacings.small,

    "& img": {
      width: "100%",
      height: "100%"
    }
  },
  searchBarContainer: {
    marginRight: Config.spacings.small,
    flexBasis: '30px',
    overflow: 'hidden',
    flexGrow: 1,
    zIndex: 2,
  },
  hidden: {
    display: "none"
  },
  backgroundOverlay: {
    backgroundColor: Config.colors.purple,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  zIndex2: {
    zIndex: 2
  }
}));


/**
 *
 * Usage :
 *
 * - With content
 *   <MobileHeader content={<div>my content</div>} />
 *
 * - With search field
 *   <MobileHeader search={true} />
 *
 * - With geolocation marker
 *   <MobileHeader geolocMarker={true} />
 *
 * - With ddf logo
 *   <MobileHeader ddfLogo={true} />
 * 
 * - With back button
 *   <MobileHeader showBackIcon={bool} />
 *
 * - With just menu button
 *   <MobileHeader />
 */

export function MobileHeader({search, showBackIcon, geolocMarker, currentForm, content, ddfLogo, currentPlace}) {

  const classes = useStyles();
  const gS = globalStyles();
  let history = useHistory();
  const [searchOpened, setSearchOpened] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  // let searchQuery = ''; // fix dic-684, effet de bord avec useState 
  const [geolocOpened, setGeolocOpened] = useState(false);
  const searchResetRef = useRef();

  return (
    <>
      {renderGeolocInformationHeader()}
      <div className={clsx(classes.container, classes.withSearchClosed)}>
        {renderBackAction()}
        {renderPreContentActions()}
        {renderContent()}
        {(search && searchOpened) ? renderSearchOpenedOverlay() : renderPostContentActions()}
      </div>
    </>
  );

  function contentClickHandler() {
    if (search && currentForm) {
      openSearchForm({inputValue: currentForm});
    }
  }

  function renderContent() {
    /*
     * Setup a click handler on the content, to open the search field pre populated with the current form. Behavior only for a mobile header
     * with searchbar enabled and a current form specified
     */
    if (search && currentForm) {
      return <div role="button"
        aria-label="Ouvrir la zone de saisie pour chercher un terme"
        onClick={contentClickHandler}>
        {content}
      </div>;

    } else {
      return content;
    }
  }

  function renderGeolocInformationHeader() {
    if (geolocMarker) {
      return (
        <Modal
          open={geolocOpened}
          onClose={() => setGeolocOpened(false)}
          className={classes.center}
          role="alertdialog"
          aria-modal="true"
          aria-labelledby="Choisir sa géolocalisation"
          aria-describedby="Soit via recherche soit via localisation automatique"
        >
          <div className={classes.geoContainer}>
            <GeolocPicker closeGeolocParentModal={() => setGeolocOpened(false)} />
          </div>
        </Modal>
      );
    } else {
      return null;
    }
  }

  function renderPostContentActions({searchOpenMode} = {}) {
    console.log({searchOpened, search, searchOpenMode})
    return (
      <div className={clsx(classes.actions, classes.postContent, searchOpened && classes.postContent100)}>
        {renderSearchField(searchOpenMode)}
        {renderGeolocMarker({searchOpenMode})}
        <button className={clsx(gS.clickable, gS.outlineFocus, classes.menuButton, search && classes.zIndex2)}
          tabIndex={0} aria-label="Ouvrir ou fermer le menu latéral"
          id="renderPostContentActionsButton"
          onClick={() => menuService.toggleMenu()}>
          <img alt="Menu" src={burgerMenuIcon} />
        </button>
      </div>
    );
  }

  function renderBackAction() {
    if (showBackIcon) {
      return (
        <div className={clsx(classes.actions, classes.preContent)}>
          <Link role="link" aria-label="Retourner à la page d'accueil" to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: currentForm})} className={classes.backlogo}>
            <img alt="<" src={backArrowPurple} />
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }

  function renderPreContentActions() {
    if (ddfLogo) {
      return (
        <div className={clsx(classes.actions, classes.preContent)}>
          <Link role="link" aria-label="Retourner à la page d'accueil" to="/" className={classes.ddfLogo}>
            <img alt="Accueil" src={ddf_logo} />
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }

  function renderSearchOpenedOverlay() {
    return (
      <div className={clsx(classes.container, classes.withSearchOpened)}        >
        {renderPostContentActions({searchOpenMode: true})}
        <div className={classes.backgroundOverlay} />
      </div>
    );
  }

  function renderSearchField(searchOpenMode) {
    if (search) {
      return searchOpenMode ? renderOpenedSearchField() : renderSearchButton();
    } else {
      return null;
    }
  }

  function renderSearchButton() {
    return (
      <div className={clsx(classes.purpleCircle, classes.searchButton)} onClick={() => openSearchForm()}
        role="button" tabIndex={0} aria-label="Ouvrir la zone de saisie pour chercher un terme">
        <img alt="Recherche" src={searchIcon} />
      </div>
    );
  }

  function renderOpenedSearchField() {
    return (
      <div className={classes.searchBarContainer}>
        <FormSearchInput
          theme="purple"
          size="medium"
          closeButton={true}
          onClose={() => closeSearchForm()}
          onChange={(value) => setSearchQuery(value)}
          // onChange={(value) => searchQuery = value} // fix dic-684
          onSubmit={performSearch}
          ref={searchResetRef}
        />
      </div>
    );
  }

  function openSearchForm({inputValue} = {}) {
    setSearchOpened(true);
    searchResetRef?.current?.resetInput({inputValue});
  }

  function closeSearchForm() {
    setSearchOpened(false);
  }

  function performSearch() {
    if (!searchQuery?.label) {
      return;
    }
    history.push(formatRoute(ROUTES.FORM_SEARCH, {formQuery: searchQuery.label}));
    closeSearchForm();
  }

  function renderGeolocMarker({searchOpenMode}) {
    if (!geolocMarker) {
      return null;
    }
    // let icon = searchOpenMode ? geolocMarkerWhiteIcon : geolocMarkerIcon;
    let icon = geolocMarkerWhiteIcon;

    return (
      <div className={clsx(classes.purpleCircle, !currentPlace?.id && classes.greyCircle, gS.clickable, classes.geolocButton, search && classes.zIndex2)}
        onClick={() => toggleGeolocInfo()}
        role="button" tabIndex={0} aria-label="Afficher votre géolocalisation">
        <img alt="Géolocalisation" src={icon} />
      </div>
    );
  }

  function toggleGeolocInfo() {
    setGeolocOpened(!geolocOpened);
  }
}
