import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import {MobileHeader} from './MobileMainLayout/MobileHeader';
import {menuService} from '../../services/MenuService';
import {MobileMenu} from './MobileMainLayout/MobileMenu';
import {CookieDisclaimer} from '../../components/general/CookieDisclaimer';

import Config from '../../Config';

export const useStyles = makeStyles((theme) => ({
  mainContainer: {
    position: 'relative',
    /** 
     * These rules allow to have a full page height, with the footer exactly on bottom of the screen
     * even if the content is shorter 
     */
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    backgroundColor: Config.colors.grey,
    /*
    +desktop() {
      center($desktop-column-width);
    }*/
  },
  freeze: {
    height: '100vh',
    overflow: 'hidden'
  },
  contentContainer: {
    flexGrow: 1,
    overflowX: 'auto'
  },
  leftPanel: {
    height: '100vh',
    width: 'calc(100vw - min(7vw,50px ))',
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      width: `${Config.widths.desktopColumn} - 50px`
    },
    overflowY: 'scroll',
    backgroundColor: Config.colors.lightgray
  }
}));

/**
 * Usage :
 *
 * <MobileMainLayout
 *   header={<MobileHedear />}
 *   footer={<MobileFooter />} 
 * />
 *
 */
export function MobileMainLayout({header, children, footer}) {
  const classes = useStyles();
  const [leftPanelOpened, setLeftPanelOpened] = useState(false);

  useEffect(() => {
    const msSub = menuService.displayStatus.subscribe((displayStatus) => {
      setLeftPanelOpened(displayStatus === 'open');
    });

    return () => {
      msSub.unsubscribe();
    };
  }, [menuService]);

  return (
    <div className={clsx(classes.mainContainer, leftPanelOpened && classes.freeze)} >
      <header>{header || <MobileHeader />}</header>

      <main role="main" className={classes.contentContainer}>{children}</main>

      <If condition={footer}>
        <footer>{footer}</footer>
      </If>
      <Drawer anchor={"left"} open={leftPanelOpened} onClose={() => setLeftPanelOpened(false)}>
        <div className={classes.leftPanel}>
          <MobileMenu />
        </div>
      </Drawer>

      <CookieDisclaimer />
    </div>
  );
}


MobileMainLayout.propTypes = {
  /** The element to use as header for the mobile layout. If not provided, it uses MobileHeader by default */
  header: PropTypes.node,
  /** The element to use as footer for the mobile layout. If not provided, no footer is used */
  footer: PropTypes.node,

};