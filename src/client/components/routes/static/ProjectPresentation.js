import React from 'react';
import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/project_presentation.md';

export function ProjectPresentation() {
  const {t} = useTranslation();

  return <InformationPage
    metaTitle={t('PROJECT_PRESENTATION_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
  />
}
