import React from 'react';
import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/accessibilite.md';
import {makeStyles} from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  kbd: {
    "background": "linear-gradient(180deg,#eee,#fff)",
    "backgroundColor": "#eee",
    "border": "1px solid #cdd5d7",
    "borderRadius": "6px",
    "boxShadow": "0 1px 2px 1px #cdd5d7",
    "fontFamily": "consolas,\"Liberation Mono\",courier,monospace",
    "fontSize": ".9rem", "fontWeight": "700", "lineHeight": "1",
    "margin": "10px", "padding": "4px 6px", "whiteSpace": "nowrap",
    color: "black"
  },
  bbl: {
    textAlign: "center",
    border: "1px solid #fff",
    padding: '2px'
  }
}));

export function Accessibilite() {
  const classes = useStyles();
  const {t} = useTranslation();

  function bottomContent() {
    return (<table  >
      <tbody>
        <tr className={classes.bbl}>
          <th className={classes.bbl}></th>
          <th className={classes.bbl}>Windows</th>
          <th className={classes.bbl}>Linux</th>
          <th className={classes.bbl}>Mac</th>
        </tr>
        <tr>
          <th className={classes.bbl}>Firefox</th>
          <td className={classes.bbl} colSpan="2" >
            <br /><kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} >Shift</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br /><br />
          </td>
          <td className={classes.bbl}>
            Firefox 57 ou plus récent: <kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} >Option</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd>
            <br /><br />ou <kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br />
            <br />Firefox 14 ou plus récent: <kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br />
            <br />Firefox 13 ou plus vieux: <kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd></td>
          <br /><br /><br />
          <br />
        </tr>
        <tr>
          <th className={classes.bbl}>Internet Explorer</th>
          <td className={classes.bbl} rowSpan="3"><kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br /> ou <br />
            <br />
            <kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} >Shift</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd>
            <br />
          </td>
          <td className={classes.bbl} colSpan="2"> N/A</td>
        </tr>
        <tr>
          <th className={classes.bbl}>Edge</th>
          <td className={classes.bbl}>N/A</td>
          <td className={classes.bbl} rowSpan="3">
            <br /><kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} >Option</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br />
            <br /><kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} >Option</kbd> + <kbd className={classes.kbd} >Shift</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd>
            <br /><br />
          </td>
        </tr>
        <tr>
          <th className={classes.bbl}>Google Chrome</th>
          <td className={classes.bbl}>
            <br /><kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br /><br />
          </td>
        </tr>
        <tr>
          <th className={classes.bbl}>Safari</th>
          <td className={classes.bbl} colSpan="2">N/A</td>
        </tr>
        <tr>
          <th className={classes.bbl}>Opera 15+</th>
          <td className={classes.bbl} colSpan="2">
            <br /><kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd><br /><br />
          </td>
          <td className={classes.bbl}>
            <br />
            <kbd className={classes.kbd} >Control</kbd> + <kbd className={classes.kbd} >Alt</kbd> + <kbd className={classes.kbd} ><em>key</em></kbd>
            <br />
          </td>
        </tr>
        <tr>
          <th className={classes.bbl}>Opera 12</th>
          <td className={classes.bbl} colSpan="3" >
            <br />
            <kbd className={classes.kbd} >Shift</kbd> + <kbd className={classes.kbd} >Esc</kbd> ouvre une liste de contenus accessibles par accessKey, on peut choisir un élément en appuyant sur <kbd className={classes.kbd} ><em>key</em></kbd>
            <br /><br /><br />
          </td>
        </tr>
      </tbody>
    </table>
    )
  };

  return <InformationPage
    metaTitle={t('ACCESSIBILITE_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
    BottomContent={bottomContent}
  />

}
