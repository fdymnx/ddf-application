import React from 'react';
import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/plan_site.md';

export function PlanSite() {
  const {t} = useTranslation();
  return <InformationPage
    metaTitle={t('PLAN_DU_SITE_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
  />;
}

