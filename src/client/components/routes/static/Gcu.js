import React from 'react';
import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/gcu.md';

export function Gcu() {
  const {t} = useTranslation();
  return <InformationPage
    metaTitle={t('GCU_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
  />;
}