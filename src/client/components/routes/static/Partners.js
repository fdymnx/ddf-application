import React from 'react';
import {useTranslation} from 'react-i18next';
import {makeStyles} from '@material-ui/core/styles';
import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/partners.md';
import Config from '../../../Config';

const useStyles = makeStyles((theme) => ({
  content: {
    backgroundColor: `${Config.colors.white} ! important`,
    color: `${Config.colors.black} ! important`,
    textAlign: 'center',
    '& img': {
      maxWidth: '200px',
      maxHeight: '100px',
      marginRight: Config.spacings.small,
      marginBottom: Config.spacings.small,
    }
  },
  desktopContent: {
    backgroundColor: `${Config.colors.white} ! important`,
    color: `${Config.colors.black} ! important`,
    textAlign: 'center',
    '& img': {
      maxWidth: '200px',
      maxHeight: '100px',
      marginRight: Config.spacings.small,
      marginBottom: Config.spacings.small,
    }
  }
}));

export function Partners(props) {
  const {t} = useTranslation();
  const classes = useStyles();
  return <InformationPage
    metaTitle={t('PARTNERS_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
    theme={classes}
  />;
}
