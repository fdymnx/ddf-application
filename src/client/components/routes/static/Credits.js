import React from 'react';

import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';

import content from '../../../assets/markdown/credits.md';


export function Credits() {
  const {t} = useTranslation();
  return <InformationPage
    metaTitle={t('CREDITS_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
  />;
}