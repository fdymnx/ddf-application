import React from 'react';
import {useTranslation} from 'react-i18next';

import {ProcessedMarkdownWrapper} from '../../../utilities/ProcessedMarkdownWrapper';
import {InformationPage} from '../../../layouts/responsive/InformationPage';
import content from '../../../assets/markdown/mentions_legales.md';

export function MentionsLegales() {
  const {t} = useTranslation();
  return <InformationPage
    metaTitle={t('MENTIONS_LEGALES_PAGE_TITLE')}
    content={<ProcessedMarkdownWrapper content={content} />}
  />;
}

