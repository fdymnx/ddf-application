import React from 'react';
import { useTranslation } from 'react-i18next';

import { ProcessedMarkdownWrapper } from '../../../utilities/ProcessedMarkdownWrapper';
import { HelpPage } from '../../../layouts/responsive/HelpPage';
import content from '../../../assets/markdown/help_how_to_contribute.md';

export function HowToContribute() {
  const { t } = useTranslation();

  return (
    <HelpPage
      title={t('HELP.HEADER')}
      subtitle="Un titre"
      helpSectionTitle={t('HELP.SECTION_LINKS.HOW_TO_CONTRIBUTE')}
      content={<ProcessedMarkdownWrapper content={content} />}
      color="yellow"
    />
  );
}
