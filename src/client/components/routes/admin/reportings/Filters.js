import React, {useEffect, useState} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import DivButton from '../../../widgets/DivButton';
import {useStyles as useFilterStyles} from "../../../contribution/ContributionsList/Filters";
import {_reasonLists, _otherReasonLists, _UncategorizedReporting} from "../../../LexicalSense/LexicalSenseActions/ReportingRatingAction/ReportingRatingCause"



Filters.propTypes = {
  onFiltersUpdate: PropTypes.func.isRequired,
};

export function Filters({onFiltersUpdate, onResetFilters}) {
  const classes = useFilterStyles();
  let {t} = useTranslation();
  // for filter on group
  const [reportTypeFilter, setReportTypeFilter] = useState("")
  const types = [..._reasonLists, ..._otherReasonLists, _UncategorizedReporting].map((type) => {
    return {
      type: type,
      label: t(`LEXICAL_SENSE_DETAIL.MODAL.REASONS.${type}`)
    }
  }).sort((a, b) => (a.label > b.label ? 1 : -1));

  useEffect(() => {
    let filters = []
    if (reportTypeFilter?.length > 0) {
      filters.push(`types = http://data.dictionnairedesfrancophones.org/ontology/ddf#${reportTypeFilter}`);
    }
    onFiltersUpdate({filters});
  }, [reportTypeFilter]);


  return (
    <div className={clsx(classes.mediumPadding, classes.container)}>      
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Grid item zeroMinWidth className={classes.purpleText}>
          Filtres
        </Grid>
        <Grid item zeroMinWidth className={clsx(classes.purpleText, classes.underline)}>
          <DivButton
            aria-label="Supprimer tous les filtres"
            onClick={onResetFilters} >
            Supprimer le filtre
          </DivButton>
        </Grid>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="center"
      >
        <Grid item xs={12} md={4} lg={3} zeroMinWidth className={classes.flexCol}>
          {renderSelect()}
        </Grid>

      </Grid>
    </div>)


  function renderSelect() {
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          data-testid='selectType'
          inputProps={{'aria-label': 'Filtrer sur le type de signalement'}}
          value={reportTypeFilter}
          displayEmpty
          onChange={(event) => {
            setReportTypeFilter(event.target.value)
          }}
        >
          <MenuItem value={""}>
            <em>Type</em>
          </MenuItem>
          {types.map(({type, label}, index) =>
            <MenuItem key={type} value={type}>{label}</MenuItem>
          )}
        </Select>
      </FormControl>
    )
  }

}

