import React, {useState, useRef} from 'react';
import {useTranslation} from 'react-i18next';
import {MetaSEO} from "../../../widgets/MetaSEO";
import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';
import {ReportingsList} from './ReportingsList';
import {Filters} from './Filters';


export function Reportings(props) {
  const {t} = useTranslation();
  const ReportingsReloadRef = useRef();
  function newFilters() {
    return {}
  }

  const [filters, setFilters] = useState(newFilters());
  function handleFilters(newfilters) {
    setFilters(newfilters)
  }


  return (
    <React.Fragment>
      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_REPORTING')} />
      <SimpleModalPage wideWidth={true} title="Signalements" content={renderContent()} titleBgColor="pink" />
    </React.Fragment>
  );

  function renderContent() {
    return <React.Fragment>
      <Filters
        onFiltersUpdate={handleFilters}
        onResetFilters={() => setFilters(newFilters())}
      />
      <ReportingsList filters={filters} reloadRef={ReportingsReloadRef} />
    </React.Fragment>;
  }
}
