import React from 'react';
import {useTranslation} from 'react-i18next';
import {MetaSEO} from "../../../../components/widgets/MetaSEO";
import {SimpleModalPage} from '../../../../layouts/responsive/SimpleModalPage';
import {ContributionsList} from '../../../contribution/ContributionsList/ContributionsList';

import {makeStyles} from '@material-ui/core/styles';
import Config from '../../../../Config';

export const useStyles = makeStyles((theme) => ({
  description : {
    fontSize:  Config.fontSizes.large,
    marginBottom: Config.spacings.medium
  }
}));

/**
 *
 * @param {int} [pageSize] - Controls how many results are displayed
 * @return {*}
 * @constructor
 */
export function ContributionsReview({pageSize} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();

  return (
    <React.Fragment>

      <MetaSEO title={t('DOCUMENT_TITLES.ADMIN_CONTRIBUTIONS')} />
      <SimpleModalPage
        wideWidth={true}
        title="Les apports récents"
        hideContributionFooter
        titleBgColor="pink"
        content={(
          <React.Fragment>
            <div className={classes.description}>
              {"Voici les informations ajoutées récemment dans le Dictionnaire des francophones :"}
            </div>
            <ContributionsList
              pageSize={pageSize}
              enableFiltering
              filterOnLoggedUser={false}
              filtersInitialValues={{
                processedStatus: false
              }}
            />
          </React.Fragment>
        )}
      />
    </React.Fragment>
  );
}
