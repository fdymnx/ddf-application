import React from 'react';
import {useTranslation} from 'react-i18next';
import {ContributionsList} from '../../../contribution/ContributionsList/ContributionsList';
import Config from '../../../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  topCaption: {
    marginBottom: Config.spacings.medium,
  }
}));



//notistackService is for test only
export function Contributions({pageSize, notistackService = null} = {}) {
  let {t} = useTranslation();
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className={classes.topCaption}>{t('MY_ACCOUNT.CONTRIBUTIONS.TITLE')}</div>

      <ContributionsList
        pageSize={pageSize}
        filterOnLoggedUser={true}
        hideColumns={['person', 'actions']}
        notistackService={notistackService}
      />
    </React.Fragment>
  );
}
