import React, {useEffect, useState} from 'react';
import {useQuery, gql} from "@apollo/client";
import {useTranslation} from 'react-i18next';

import {EditPassword} from './ProfileSettings/EditPassword';
import {DeleteAccount} from './ProfileSettings/DeleteAccount';
import {PlaceSetting} from './ProfileSettings/PlaceSetting';
import {VocabularyDomainSetting} from './ProfileSettings/VocabularyDomainSetting';
import {formatPlace} from '../../../../utilities/helpers/places';
import {gqlCountryFragment} from '../../../../utilities/helpers/countries';


export const gqlCurrentUserSettings = gql`
  query Me {
    me {
      id
      place {
        id
        ...CountryFragment
      }
      preferredDomain {
        id
        prefLabel
      }
    }
  }

  ${gqlCountryFragment}
`;


export function ProfileSettings(props) {
  const {t} = useTranslation();
  const {loading, error, data: {me: currentUser = {}} = {}} = useQuery(gqlCurrentUserSettings, {fetchPolicy: 'cache-and-network'});

  const [displayValue, setDisplayValue] = useState(formatPlace(currentUser.place, ['name', 'state', 'country']));
  useEffect(() => {
    setDisplayValue(formatPlace(currentUser.place, ['name', 'state', 'country']));
  }, [currentUser.place]);


  return (
    <React.Fragment>
      <PlaceSetting
        displayValue={displayValue?.label || displayValue}
        setDisplayValue={setDisplayValue}
        inputValue={currentUser.place?.name}
        idValue={currentUser.place?.id}
        meId={currentUser.id}
      />
      <hr />
      <VocabularyDomainSetting
        displayValue={currentUser.preferredDomain?.prefLabel}
        idValue={currentUser.preferredDomain?.id}
        meId={currentUser.id}
      />
      <hr />
      <EditPassword />
      <hr />
      <DeleteAccount />
    </React.Fragment>
  );
}
