import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {FoldableContainer} from '../../../../widgets/FoldableContainer';
import {Button} from '../../../../widgets/forms/Button';
import {ButtonRow} from '../../../../widgets/forms/ButtonRow';

import {notificationService as appNotificationService} from '../../../../../services/NotificationService';
import {getUserAuthenticationService} from '../../../../../services/UserAuthenticationService';
import {withRouter} from 'react-router-dom';
import Config from '../../../../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    fontSize: Config.fontSizes.xsmall,
    color: Config.colors.darkgray,
    "& p": {
      marginBottom: Config.spacings.small
    }
  }
}));

/***
 *
 * List of error messages :
 *
 * - Backend defined :
 *
 */
export function _DeleteAccount(props) {
  const classes = useStyles();
  const history = props.history;
  const {t} = useTranslation();
  const notificationService = props.notificationService || appNotificationService;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {unregisterUserAccount, } = userAuthenticationService.useUnregisterUserAccount();
  const [isSubmitting, setSubmitting] = useState(false);

  return (
    <FoldableContainer
      title={t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.TITLE')}
    >
      {({open, close}) => {
        return (
          <div className={classes.container}>
            <p>
              {t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.TOP_CAPTION')}
            </p>
            <ButtonRow>
              <Button aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.CANCEL')} secondary onClick={close}>
                {t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.CANCEL')}
              </Button>
              <Button aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.SUBMIT')}
                disabled={isSubmitting}
                loading={isSubmitting}
                onClick={deleteAccountHandler}
              >
                {t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.SUBMIT')}
              </Button>
            </ButtonRow>
          </div>
        );
      }}
    </FoldableContainer>
  );

  async function deleteAccountHandler() {
    let {confirmationResult} = await notificationService.info(
      t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.ARE_YOU_SURE'),
      {confirm: true}
    );
    if (confirmationResult === 'yes') {
      setSubmitting(true);
      let result = await unregisterUserAccount();

      if (result?.success) {
        await notificationService.success(
          t('MY_ACCOUNT.PROFILE.SETTINGS.ACCOUNT_DELETION.SUCCESS')
        );
        history.push('/');
      }

      if (result?.globalErrorMessage) {
        notificationService.error(result.globalErrorMessage);
      }

      setSubmitting(false);
    }
  }
}

export let DeleteAccount = withRouter(_DeleteAccount);
