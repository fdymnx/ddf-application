
import React from 'react';
import PropTypes from 'prop-types';
import {useMutation, gql} from "@apollo/client";
import {PlaceAutocompleteMUIA} from '../../../../widgets/PlaceAutocompleteMUIA';
import {GenericSettingField} from './GenericSettingField';
import {getUserAuthenticationService} from '../../../../../services/UserAuthenticationService';

const gqlEditPersonPlace = gql`
  mutation EditPersonPlace($personId: ID!, $objectId: ID){
    updatePerson(input: {
      objectId: $personId, 
      objectInput: {
        placeInput: {id: $objectId}
      }
    }){
      updatedObject{
        id
        place {
          id
        }
      }
    }
  }
`;


PlaceSetting.propTypes = {
  displayValue: PropTypes.string,
  inputValue: PropTypes.string,
  idValue: PropTypes.string,
  meId: PropTypes.string
};
export function PlaceSetting(props) {
  const {displayValue, setDisplayValue, inputValue, idValue, meId} = props;
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const [editPersonPlaceMutation] = useMutation(gqlEditPersonPlace);


  return (<GenericSettingField
    displayValue={displayValue}
    setDisplayValue={setDisplayValue}
    inputValue={inputValue}
    idValue={idValue}
    meId={meId}
    i18nSectionKey="PLACE"
    mutation={editPersonPlaceMutation}
    AutocompleteComponent={PlaceAutocompleteMUIA}
    onSubmit={() => userAuthenticationService.refreshCurrentUser()}
  />);
}
