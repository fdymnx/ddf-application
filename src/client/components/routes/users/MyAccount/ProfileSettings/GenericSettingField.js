
import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Formik} from 'formik';
import {Button} from '../../../../widgets/forms/Button';
import {ButtonRow} from '../../../../widgets/forms/ButtonRow';
import {Form} from '../../../../widgets/forms/formik/Form';
import {notificationService as appNotificationService} from '../../../../../services/NotificationService';
import {GraphQLErrorHandler} from '../../../../../services/GraphQLErrorHandler';
import Config from '../../../../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  settingField: {
    fontSize: Config.fontSizes.small
  },
  title: {
    color: Config.colors.pink,
    fontSize: Config.fontSizes.medium,
    marginBottom: Config.spacings.tiny,
  },
  currentSetting: {
    marginBottom: Config.spacings.tiny
  },
  currentSettingValue: {
    color: Config.colors.purple,
  },
  hint: {
    marginBottom: Config.spacings.tiny,
    fontSize: Config.fontSizes.xsmall,
    color: Config.colors.darkgray,
  },
  editLinkA: {
    textDecoration: "underline",
    color: Config.colors.purple,
  }
}));



GenericSettingField.propTypes = {
  /**
   * The value to display in non edit mode (it's in static html tag, e.g "span")
   */
  displayValue: PropTypes.string,
  /**
   * The initial value to display in the html input (before user edit it)
   */
  inputValue: PropTypes.string,
  /**
   * The ID of the setting's object current value (for example if this is a place setting, the current
   * Place ID)
   */
  idValue: PropTypes.string,
  /**
   * The current user ID, needed for the update mutation
   */
  meId: PropTypes.string,
  /**
   * The key for the translations. The i18n prefix used will be "MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}".
   * The i18n entries used are : 
   *
   * - TITLE
   * - HINT
   * - EMPTY
   */
  i18nSectionKey: PropTypes.string,
  /**
   * The mutation function to use (as provided by useMutation hook) for updating the setting. The mutation must take the variables `meId` (the current user
   * Id), and `objectId` (the setting object ID)
   */
  mutation: PropTypes.any,
  /**
   * The autocomplete component to use to edit the setting. Must work like PlaceAutocomplete component : 
   *      <AutocompleteComponent
   *          name="objectId" 
   *          placeholder="myPlaceholder"
   *          selectNullOnEmpty             
   *      />
   */
  AutocompleteComponent: PropTypes.elementType
};
export function GenericSettingField(props) {
  const classes = useStyles();
  const {displayValue, setDisplayValue, meId, idValue, i18nSectionKey, mutation, AutocompleteComponent, onSubmit} = props;
  const {t} = useTranslation();
  const [editing, setEditing] = useState(false);
  const notificationService = props.notificationService || appNotificationService;

  return (
    <div className={classes.settingField}>
      <div className={classes.title}>{t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.TITLE`)}</div>
      {!editing && renderReadMode()}
      {editing && renderEditMode()}
    </div>
  );

  function renderReadMode() {
    return (
      <React.Fragment>
        <div className={classes.currentSetting}>
          <span>{t('MY_ACCOUNT.PROFILE.SETTINGS.CURRENT')}</span>
          <span className={classes.currentSettingValue}>{displayValue || t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.EMPTY`)}</span>
        </div>
        <div className={classes.hint}>{t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.HINT`)}</div>
        <div ><a role="link" className={classes.editLinkA} aria-label="Modifier mon profil" href="#" onClick={openEdit}>{t('MY_ACCOUNT.PROFILE.SETTINGS.MODIFY')}</a></div>
      </React.Fragment>
    );
  }

  function renderEditMode() {
    return (
      <Formik
        onSubmit={(values, formikOptions) => submitHandler({values, formikOptions})}
        initialValues={{objectId: idValue}}
        initialStatus={{autocompleteInputValues: {objectId: props.inputValue}}}>
        {({values, isSubmitting, setFieldValue, ...other}) => (
          <Form>
            <AutocompleteComponent
              name="objectId"
              value={displayValue}
              handleChange={(newValue) => {setDisplayValue(newValue); newValue?.id && setFieldValue("objectId", newValue.id)}}
              handleSubmit={() => { }}
              placeholder={t(`MY_ACCOUNT.PROFILE.SETTINGS.${i18nSectionKey}.TITLE`)}
              selectNullOnEmpty
            />
            <ButtonRow>
              <Button type="button" secondary onClick={closeEdit}
                aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.CANCEL')} >
                {t('MY_ACCOUNT.PROFILE.SETTINGS.CANCEL')}
              </Button>
              <Button type="submit" disabled={isSubmitting} loading={isSubmitting}
                aria-label={t('MY_ACCOUNT.PROFILE.SETTINGS.SUBMIT')} >
                {t('MY_ACCOUNT.PROFILE.SETTINGS.SUBMIT')}
              </Button>
            </ButtonRow>
          </Form>
        )}
      </Formik>
    );
  }




  async function submitHandler({values, formikOptions: {setSubmitting}}) {
    let {objectId} = values;

    if (typeof objectId === "object") {
      objectId = objectId.id
    }

    let variables = {
      personId: meId,
      objectId
    };

    try {
      await mutation({variables});
      closeEdit();
    } catch (error) {
      let errorMessage = GraphQLErrorHandler(error, {t});
      notificationService.error(errorMessage);
    } finally {
      setSubmitting(false);
      onSubmit && onSubmit(values);
    }
  }

  function openEdit() {
    setEditing(true);
  }

  function closeEdit() {
    setEditing(false);
  }
}
