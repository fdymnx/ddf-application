import React, {useState, useContext} from 'react';
import {useTranslation} from 'react-i18next';
import {Link, useHistory} from 'react-router-dom';
import {makeStyles} from "@material-ui/core/styles";
import clsx from 'clsx';
import {FormContainer} from '../../../widgets/forms/FormContainer';
import {Button} from '../../../widgets/forms/Button';
import {FormBottom} from '../../../widgets/forms/FormBottom';
import {ButtonRow} from '../../../widgets/forms/ButtonRow';
import {ROUTES} from '../../../../routes';
import {notificationService} from '../../../../services/NotificationService';
import {getUserAuthenticationService} from '../../../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../../../services/BrowsingHistoryHelperService';
import {formatPlace} from '../../../../utilities/helpers/places';
import {useMediaQueries, Desktop} from '../../../../layouts/MediaQueries';
import {UserContext} from "../../../../hooks/UserContext";



import Config from '../../../../Config';
import globalStyles from '../../../../assets/stylesheets/globalStyles.js';


const useStyles = makeStyles((theme) => ({

  topCaption: {
    fontSize: Config.fontSizes.small,
    marginBottom: Config.spacings.medium,
    textAlign: "center",
    [theme.breakpoints.up(Config.theme.breakpoint)]: {      
      textAlign: "left"
    },
  },
  profileSummary: {
    color: Config.colors.purple,
    marginBottom: Config.spacings.medium,
    textAlign: "center",
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      color: Config.colors.pink,
      textAlign: "left"
    },
    "& p": {
      marginBottom: Config.spacings.tiny
    }
  }


}));

export function Profile(props) {
  const classes = useStyles();
  const gS = globalStyles();
  /* Services DI */
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();

  /* Hooks */
  const {t} = useTranslation();
  const history = useHistory();
  const {user} = useContext(UserContext);
  const {logout} = userAuthenticationService.useLogout();
  const {isDesktop} = useMediaQueries();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();
  const [logoutIsPending, setLogoutIsPending] = useState(false);

  const ButtonContainer = isDesktop ? ButtonRow : React.Fragment;

  return (
    <React.Fragment>
      <div className={classes.topCaption}>{t('MY_ACCOUNT.PROFILE.INDEX.THIS_IS_YOUR_PROFILE')}</div>
      <div className={classes.profileSummary}>
        <p>{user.nickName}</p>
        <p>{user.email}</p>
        <p>{formatPlace(user.place, ['name', 'state', 'country'])}</p>
      </div>
      <FormContainer>
        <FormBottom>
          <ButtonContainer>
            <Button type="button" as={Link} role="link"
              aria-label={t('MY_ACCOUNT.PROFILE.INDEX.EDIT_PROFILE')}
              to={ROUTES.MY_ACCOUNT_PROFILE_EDIT}>
              {t('MY_ACCOUNT.PROFILE.INDEX.EDIT_PROFILE')}
            </Button>
            <Button type="button" as={Link} role="link"
              aria-label={t('MY_ACCOUNT.PROFILE.INDEX.EDIT_SETTINGS')}
              to={ROUTES.MY_ACCOUNT_PROFILE_SETTINGS}>
              {t('MY_ACCOUNT.PROFILE.INDEX.EDIT_SETTINGS')}
            </Button>
            <Desktop>
              <Button
                type="button"
                onClick={handleLogout}
                disabled={logoutIsPending}
                loading={logoutIsPending}
                className={gS.outlineFocus}
              >
                {t('LOGOUT')}
              </Button>
            </Desktop>
          </ButtonContainer>
        </FormBottom>
      </FormContainer>
    </React.Fragment>
  );

  async function handleLogout() {
    setLogoutIsPending(true);
    let res = await logout();
    setLogoutIsPending(false);
    if (res.success) {
      let route = browsingHistoryHelperService.getAfterLogoutLocation({lookAtPreviousPage: true});
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }
}
