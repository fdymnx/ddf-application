import React, {useState, useRef} from 'react';
import {Formik} from 'formik';
import * as Yup from 'yup';

import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import FormikInput from '../widgets/forms/formik/FormikInput';
import {Input} from '../widgets/forms/Input';
import {Textarea} from '../widgets/forms/Textarea';
import {Form} from '../widgets/forms/formik/Form';
import {Button} from '../widgets/forms/Button';
import {FormBottom} from '../widgets/forms/FormBottom';
import {PlaceAutocompleteMUIA} from '../widgets/PlaceAutocompleteMUIA';
import {Autocomplete} from '../widgets/forms/Autocomplete';
import {AutocompleteController} from '../widgets/forms/AutocompleteController';
import FormikRadioButton from '../widgets/forms/formik/FormikRadioButton';
import FormikCheckbox from '../widgets/forms/formik/FormikCheckbox';

const validationSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
});

const _COLOR = "yellow";

const initialValues = {
  name: '',
  email: '',
  gender: 'm',
};

export function FormDemo(props) {
  const [colorTheme, setColorTheme] = useState(null);
  const [currentDemo, setCurrentDemo] = useState('form'); // can be 'form' or 'standaloneInputs'
  const containerRef = useRef(null);
  const [inputValue, setInputValue] = useState('');

  return <SimpleModalPage title="S'inscrire" content={renderContent()} />;

  function colorThemeOnChange(evt) {
    console.log("colorThemeOnChange")
    setColorTheme(evt.target.value === _COLOR ? _COLOR : null);
  }

  function renderDemoSwitcher() {
    return (
      <React.Fragment>
        <div style={{display: 'flex'}}>
          Color theme :
          <span>
            <input type="radio" name="demoThemeColor" value="default" checked={colorTheme === null} onChange={(e) => colorThemeOnChange(e)} />
            Default
          </span>
          <span>
            <input
              type="radio"
              name="demoThemeColor"
              value={_COLOR}
              checked={colorTheme === _COLOR}
              onChange={(e) => colorThemeOnChange(e)}
            />
            {_COLOR}
          </span>
        </div>
        <div style={{display: 'flex'}}>
          Demo type :
          <span>
            <input
              type="radio"
              name="currentDemo"
              value="form"
              checked={currentDemo === 'form'}
              onChange={(e) => setCurrentDemo(e.target.value)}
            />
            Form
          </span>
          <span>
            <input
              type="radio"
              name="currentDemo"
              value="standaloneInputs"
              checked={currentDemo === 'standaloneInputs'}
              onChange={(e) => setCurrentDemo(e.target.value)}
            />
            Standalone Inputs
          </span>
        </div>
      </React.Fragment>
    );
  }

  function renderForm() {
    return (
      <div ref={containerRef} >
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values, {setSubmitting}) => {
            setTimeout(() => {
              setSubmitting(false);
            }, 1000);
          }}>
          {({setFieldValue, isSubmitting, values}) => (
            <Form colorTheme={colorTheme} >
              <FormikInput type="text" name="name" placeholder="Pseudonyme (ou nom)" />
              <FormikInput type="email" name="email" placeholder="Adresse de courriel" />
              <FormikInput type="password" name="password" placeholder="Mot de passe" />
              <PlaceAutocompleteMUIA
                placeholder="Lieu de vie"
                inputRef={null}
                value={inputValue}
                containerRef={containerRef}
                handleChange={(newObj) => {
                  setInputValue(newObj.label)
                  setFieldValue("placeId", newObj?.id);
                }}
                handleSubmit={() => null}
              />
              <Textarea
                name="biography"
                placeholder="Champ d'activité / connaissances particulières / champ d'expertise / passion / façon de se présenter aux autres"
              />
              <Textarea name="definition" placeholder="Définition" isRequired={true} />
              <FormikCheckbox name="gcuAccepted" label="Accepter les GCU" />
              <h2>Genre</h2>
              <FormikRadioButton name="gender" value="f" label="Féminin" />
              <FormikRadioButton name="gender" value="m" label="Masculin" />
              <FormikRadioButton name="gender" value="n" label="Ne se prononce pas" />
              <FormBottom>
                <Button aria-label="Soumettre le formulaire" type="submit" disabled={isSubmitting}>
                  Valider
                </Button>
                <Button aria-label="Annuler" type="submit" secondary>
                  Annuler
                </Button>
              </FormBottom>
              <pre>{JSON.stringify({values}, null, 2)}</pre>
            </Form>
          )}
        </Formik>
      </div>
    );
  }

  function renderStandaloneInputs() {
    let controller = new AutocompleteController();
    controller.onSuggestionsFetchRequested = () => {
      controller.updateSuggestions(['Paris', 'Londres', 'Milan']);
    };

    return (
      <React.Fragment>
        <Input type="text" name="name" placeholder="Pseudonyme (ou nom)" colorTheme={colorTheme} required />
        <Input type="password" name="password" placeholder="Mot de passe" colorTheme={colorTheme} />
        <Textarea
          name="biography"
          placeholder="Champ d'activité / connaissances particulières / champ d'expertise / passion / façon de se présenter aux autres"
        />
        <Textarea name="definition" placeholder="Définition" isRequired={true} colorTheme={colorTheme} />
        <Autocomplete controller={controller} name="place" placeholder="Lieu de vie" colorTheme={colorTheme} />
        <Button aria-label="Soumettre le formulaire" type="submit" colorTheme={colorTheme}>
          Valider
        </Button>
        <Button aria-label="Annuler" type="submit" secondary>
          Annuler
        </Button>
      </React.Fragment>
    );
  }

  function renderContent() {
    return (
      <React.Fragment>
        {renderDemoSwitcher()}
        <If condition={currentDemo === 'form'}>{renderForm()}</If>
        <If condition={currentDemo === 'standaloneInputs'}>{renderStandaloneInputs()}</If>
      </React.Fragment>
    );
  }
}
