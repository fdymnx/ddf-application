import {LexicalSenseQuery} from '../../LexicalSense';
import {FormTopPostsQuery} from '../../../Form/FormTopPosts';

export const lexicalSenseQuery_withTopPostAboutSense = {
  request: {
    query: LexicalSenseQuery,
    variables: {
      lexicalSenseId: 'inv/lexical-sense/1234',
      formQs: 'affairé'
    },
  },
  result: {
    data: {
      lexicalSensesCountForAccurateWrittenForm: 12,
      lexicalSense: {
        id: 'inv/lexical-sense/1234',
        definition: 'Affairiste',
        usageExamples: {
          edges: [],
          __typename: 'UsageExampleConnection',
        },
        lexicalEntryGrammaticalPropertyLabels: ["nom"],
        places: {
          edges: [],
          __typename: 'PlaceInterfaceConnection',
        },
        __typename: 'LexicalSense',
        semanticPropertyLabels: ['au figuré'],
        semanticProperties: {
          "edges": [
            {
              "node": {
                "id": "ddfa:domaine/societe",
                "prefLabel": 'au figuré',
                "__typename": "SemanticProperty"
              },
              "__typename": "SemanticPropertyEdge"
            }

          ]
        },
        semanticRelations: {
          edges: [
            {
              "node": {
                "id": "semantic-relation/b99kbp0mkxj18s",
                "semanticRelationType": {
                  "id": "ddfa:sense-relation/typographicalRelation",
                  "prefLabel": "abréviation",
                  "scopeNote": "distinctWrittenRep"
                },
                "lexicalSenses": {
                  "edges": [
                    {
                      "node": {
                        "id": "lexical-sense/a6uc6f91l6xp2d",
                        "canonicalFormWrittenRep": "poire",
                        "__typename": "LexicalSense"
                      },
                      "__typename": "LexicalSenseEdge"
                    }
                  ]
                }
              }
            }
          ]
        },
        topPostAboutSense: {
          id: 'test1234',
          content: 'Affairé signifie aussi, un homme accablé de dettes, dont les affaires sont embarassées...',
          __typename: 'Post',
        },
        validationRatingsCount: 1,
        suppressionRatingsCount: 0,
        reportingRatingsCount: 0,
        validationRating: null,
        suppressionRating: null,
        reportingRating: null,
        canUpdate: false,
        lexicographicResourceName: 'Dictionnaire des francophones',
        lexicographicResourceShortName: 'Ddf',
        lexicographicResourceDefinition: "Le ddf ....",
      },
    },
  },
};
 

export const FormTopPostsQuery_Mock = {
  request: {
    query: FormTopPostsQuery,
    variables: {
      formQs: 'affairé',
    },
  },
  result: {
    data: {
      topPostAboutEtymology: {
        content: 'Dérivé de affaire',
        id: 'inv/lexical-sense/12346',
        __typename: 'Post',
      },
      topPostAboutForm: {
        content: "Semble s'être dit d'abord de domestiques, d'intendants, etc.",
        id: 'inv/ltopPostAboutForm/12345',
        __typename: 'Post',
      },
    },
  },
};
