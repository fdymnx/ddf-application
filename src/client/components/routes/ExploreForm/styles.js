
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../../Config';

export const useStyles = makeStyles((theme) => ({
  littleText: {
    fontSize: Config.fontSizes.medium,
    marginBottom: Config.spacings.tiny,
    fontStyle: 'italic'
  },
  bigText: {
    fontSize: Config.fontSizes.xxlarge,
  },
  worldText: {
    display: "flex",
    justifyContent: "center",
    paddingTop: "10px"
  },
  definition: {
    fontSize: Config.fontSizes.medium,
    marginTop: Config.spacings.large,
    marginBottom: Config.spacings.large,
  },
  mbMedium: {
    marginBottom: Config.spacings.medium,
  },
  sticky: {
    position: "sticky",
    top: "10%"
  },
  expandIcon: {
    display: "inline-flex",
    verticalAlign: "top",
    cursor: "pointer"
  },
  MT24: {
    marginTop: "24px",
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      marginTop: "6px",
    }
  },
  paddingForMobile:{
    [theme.breakpoints.down(Config.mediaQueries.mobileMaxWidth)]: {
      padding: "6px !important"
    }
  },
  greyText: {
    fontSize: Config.fontSizes.sm,
    color: Config.colors.darkgray,
    fontStyle: 'italic'
  },
  purpleText: {
    color: Config.colors.purple
  },
  searchTypeContainer: {
    width: "100%",
    padding: `${Config.spacings.small}px ${Config.spacings.small * 2}px`,
     
    marginBottom: Config.spacings.small
  },


  flexspace: {
    display: "flex",
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  modalContent: {
    backgroundColor: 'white',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    padding: Config.spacings.large,
    boxShadow: 24,

    width: 'min(65vw,1100px)',
    maxHeight: '85vh',
    overflowY: 'auto'
  },
  modalCloseButton: {
    display: "flex",
    justifyContent: "flex-end",
    paddingBottom: Config.spacings.small,
  }

}));
