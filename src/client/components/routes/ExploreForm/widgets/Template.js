import React, {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import {useHistory} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import {Grid, Modal, Button, IconButton} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import FilterIcon from "@material-ui/icons/Search";
import {MetaSEO} from "../../../widgets/MetaSEO";
import {SimpleModalPage} from "../../../../layouts/mobile/SimpleModalPage";
import {DesktopMainLayout} from "../../../../layouts/desktop/DesktopMainLayout";
import {useMediaQueries} from "../../../../layouts/MediaQueries";
import {notificationService as appNotificationService} from "../../../../services/NotificationService";
import {GraphQLErrorHandler} from "../../../../services/GraphQLErrorHandler";
import Config from '../../../../Config';
import globalStyles from "../../../../assets/stylesheets/globalStyles";
import {ShowWhereIAM} from '../../../widgets/ShowWhereIAM';

const useStyles = makeStyles((theme) => ({
  flexCol: {
    display: "flex",
    flexDirection: "column"
  },
  modalContainer: {
    overflowY: 'auto'
  },
  modalContent: {
    margin: Config.spacings.medium,
    backgroundColor: 'white'
  },
  closeButton: {
    display: "flex",
    justifyContent: "flex-end",
    paddingBottom: Config.spacings.small,
  }
}));

/**
 * show mobile/desktop html body around content
 * @param {object} error from parent
 * @param {function} leftContent to display on the left 
 * @param {function} rightContent to display on the right
 * @param {string} title for mobile & metaSeo 
 * @param {boolean} useModalForLeftContent
 */
export function Template({error, leftContent = false, rightContent = false, title = "EXPLORE.TITLE", useModalForLeftContent = false}) {
  const classes = useStyles();
  const gS = globalStyles();
  const {t} = useTranslation();

  const {isMobile} = useMediaQueries();
  let history = useHistory();
  const [modalOpen, setModalOpen] = useState(false);

  const notificationService = appNotificationService;

  useEffect(() => {
    if (error) {
      console.log("Template error", error)
      let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
      notificationService.error(globalErrorMessage);
    }
  }, [error]);


  return isMobile ? renderMobile() : renderDesktop();


  function renderMobile() {
    return (
      <SimpleModalPage
        title={t(title)}
        content={renderContent()}
        titleBgColor="yellow"
        onBack={history.goBack}
        onClose={() => history.push("/")}
        controls={['back', 'close']}
      />
    );
  }

  function renderDesktop() {
    return (
      <ShowWhereIAM path="src/client/components/routes/ExploreForm/Template.js renderDesktop">
        <DesktopMainLayout useDefaultGrid veryWideWidth={true}>
          <div className={gS.SMPcontent}>
            {renderContent()}
          </div>
        </DesktopMainLayout>
      </ShowWhereIAM>
    );
  }

  function renderContent() {

    return (
      <ShowWhereIAM path="src/client/components/routes/ExploreForm/Template.js renderContent">
        <React.Fragment>
          <MetaSEO title={t(title)} />


          {isMobile ?
            <>
              <Modal
                open={modalOpen}
                onClose={() => setModalOpen(false)}
                className={classes.modalContainer}
                role="alertdialog"
                aria-modal="true"
                aria-labelledby="Filtres"
              >
                <div className={classes.modalContent}>
                  <div className={classes.closeButton} >
                    <IconButton aria-label="close" onClick={() => setModalOpen(false)}>
                      <CloseIcon />
                    </IconButton>
                  </div>
                  <div className={classes.flexCol}>
                    {leftContent}
                  </div>
                </div>
              </Modal>
              <div className={classes.flexCol}>
                <Button onClick={() => setModalOpen(true)}
                  variant="contained" color="default" size="small"
                  aria-label={"ouvrir les filtres"} endIcon={<FilterIcon />}>
                  Filtres à facettes
                </Button>
                <br/>
                {rightContent}
              </div>
            </>
            :
            <Grid container direction="row" justifyContent="space-between" alignItems="stretch" spacing={2}>
              <Grid item xs={12} sm={4} md={3} className={classes.flexCol}>
                {leftContent}
              </Grid>
              <Grid item xs={12} sm={8} md={9} className={classes.flexCol}>
                {rightContent}
              </Grid>
            </Grid>
          }
        </React.Fragment>
      </ShowWhereIAM>
    );
  }



}

