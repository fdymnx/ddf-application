import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import {useMediaQueries} from "../../../../layouts/MediaQueries";
import globalStyles from "../../../../assets/stylesheets/globalStyles";
import DivButton from '../../../widgets/DivButton';
import Config from "../../../../Config";
import clsx from "clsx";


const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: Config.colors.purple,
    overflowX: 'auto',
    "&::-webkit-scrollbar": {
      width: "1em",
      maxWidth: "4px"
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: Config.colors.purple
    },
    "&:hover": {
      "&::-webkit-scrollbar-thumb": {
        borderRadius: "10px",
        backgroundColor: Config.colors.darkpurple
      }
    }
  },
  flexRow: {
    display: "flex",
    flexDirection: "row",
  },
  letter: {
    fontSize: Config.fontSizes.xlarge,
    margin:"3px 3px 3px 3px",
    padding: "7px",    
    whiteSpace: "nowrap",
    color: Config.colors.white
  },
  selectedLetter: {
    color: Config.colors.purple,
    backgroundColor: Config.colors.white
  }
}));

const alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0-9", "*"];
const alphabetL1 = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n"];
const alphabetL2 = ["o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0-9", "?"];

/**
 * show mobile/desktop html body around content
 * @returns 
 */
export function Alphabet({selectedLetter, setSelectedLetter}) {
  const classes = useStyles();

  
  /*
  const {isMobile} = useMediaQueries();

  if (isMobile) {
    return (<div className={clsx(classes.container, classes.flexRow)}>
      {alphabet.map((letter) => renderButton(letter))}
    </div>)
  } else {
    return (<div className={classes.container}>
      <div className={classes.flexRow}>{alphabetL1.map((letter) => renderButton(letter))}</div>
      <div className={classes.flexRow}>{alphabetL2.map((letter) => renderButton(letter))}</div>
    </div>)
  }*/
  return (
    <div className={clsx(classes.container, classes.flexRow)}>
      {alphabet.map((letter) => renderButton(letter))}
    </div>
  )

  function renderButton(letter) {
    return <DivButton
      key={letter}
      aria-label={letter}
      classesName={clsx(letter === selectedLetter && classes.selectedLetter, classes.letter)}
      onClick={() => {
        setSelectedLetter(letter)
      }} >
      {letter.toUpperCase()}
    </DivButton>
  }


}

