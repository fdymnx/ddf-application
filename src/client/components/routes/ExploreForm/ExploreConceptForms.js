import React, {useState} from 'react';
import {Link, useParams} from 'react-router-dom';
import {useQuery} from '@apollo/client';
import {gql} from "@apollo/client";
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';
import {formatRoute} from 'react-router-named-routes';
import Tooltip from '@material-ui/core/Tooltip';
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import clsx from "clsx";
import {ROUTES} from '../../../routes';
import {Template} from './widgets/Template';
import {ListOfForms} from './widgets/ListOfForms';
import {FrameWithArrow} from './widgets/FrameWithArrow';
import {upperCaseFirstLetter, getTypeFromSchemes} from '../../../utilities/helpers/tools';
import {useStyles} from "./styles";

const TYPE_NAME = {
  "domain": {label: "domaine", link: "/aide/2#domaines"},
  "glossary": {label: "lexique", link: "/aide/2#listes-thematiques"},
  "temporality": {label: "temporalité", feminin: true, link: "/aide/2#temporalite"},
  "grammatical-constraint": {label: "contrainte grammaticale", plural: "contraintes grammaticales", feminin: true, link: "/aide/2#grammaires"},
  "sociolect": {label: "argot", link: "/aide/2#argot"},
  "register": {label: "registre", link: "/aide/2#registre"},
  "textual-genre": {label: "genre textuel", plural: "genres textuels", link: "/aide/2#genre-textuel"},
  "frequency": {label: "fréquence", feminin: true, link: "/aide/2#frequence"},
  "connotation": {label: "connotation", feminin: true, link: "/aide/2#connotation"},
};


/**
 * explore forms of a given concept
 * the concept can be of different type : domain, glossary, 
 * 
 * @param {*} param0 
 * @returns 
 */
export function ExploreConceptForms({props}) {
  let params = useParams();
  const classes = useStyles();
  const id = decodeURIComponent(params.id);
  const type = params.type;

  // cannot use filters now because we use a custom   "synaptixSession.getIndexService().getNodes()" in LexicalSenseGraphQLDefinition for lexicalSensesForWrittenRepStartWithCount
  const mustFilters = JSON.stringify([{
    "bool": {
      "minimum_should_match": 1,
      "should": [{
        "term": {
          [`has${upperCaseFirstLetter(camelCase(type))}`]: id.replace("ddfa:", "http://data.dictionnairedesfrancophones.org/authority/")
        }
      }]
    }
  }]);

  const gramFilters = [`inScheme:http://data.dictionnairedesfrancophones.org/authority/${type}`, "isTopInScheme:true"]

  const [showAllNarrower, setShowAllNarrower] = useState(false);

  const {data: {countAll, topConcepts, concept: {
    schemePrefLabel, prefLabel, definition, broaderConcept, narrowerConcepts, relatedConcepts
  } = {}} = {}, loading, error} = useQuery(ConceptQueryWithCount, {
    variables: {
      id, mustFilters, gramFilters,
      includeRelated: type === "glossary"
    }
  })

  return (
    <Template
      leftContent={renderLeftContent()}
      rightContent={renderRightContent()}
    />
  );

  function renderLeftContent(params) {
    if (loading) {
      return null;
    }
    const {label, feminin, plural, link} = TYPE_NAME[type];
    const pluralLabel = plural || (label + "s");
    const pluralLabelMain = feminin ? (pluralLabel + ' principales') : (pluralLabel + ' principaux');

    return <div className={classes.paddingForMobile} id="paddingForMobile ">
      <div className={classes.littleText}>
        {upperFirst(schemePrefLabel)}
      </div>
      <div className={classes.bigText}>
        {upperFirst(prefLabel)}
      </div>

      <div className={classes.definition}>
        {upperFirst(definition)}
      </div>
      {Number.isInteger(countAll) && <FrameWithArrow count={countAll} />}


      {relatedConcepts?.edges?.length > 0 &&
        <div className={classes.mbMedium}>
          Appartient au domaine : {renderNarrowerConcepts(relatedConcepts?.edges)}
        </div>
      }

      {broaderConcept?.id &&
        <div className={classes.mbMedium}>
          Sous-{label} de : {createLink(broaderConcept)}
        </div>
      }

      {narrowerConcepts?.edges?.length > 0 &&
        <div className={classes.mbMedium}>
          Contient : {renderNarrowerConcepts(narrowerConcepts?.edges)}
        </div>
      }

      {!broaderConcept?.id &&
        <div className={classes.mbMedium}>
          {upperFirst(pluralLabelMain)} : {renderTopConcepts(topConcepts?.edges)}
        </div>
      }

      <div className={clsx(classes.mbMedium, classes.greyText)}>
        Voir plus d'information sur les <Link
          role="link"
          className={classes.purpleText}
          aria-label={pluralLabel}
          to={link}
        >
          {pluralLabel}
        </Link>
      </div>

    </div>
  }



  function renderTopConcepts(topConcepts) {
    return renderConcept(topConcepts);
  }

  function renderNarrowerConcepts(narrowerConcepts) {

    const length = narrowerConcepts?.length;
    const _MAXLENGTH = 25;

    return <>
      {renderConcept(narrowerConcepts.slice(0, _MAXLENGTH))}
      {length > _MAXLENGTH && <>
        {!showAllNarrower ?
          <span className={classes.expandIcon}><ExpandMoreIcon onClick={() => {setShowAllNarrower(true)}} /></span>
          : <>
            <>, </>
            {renderConcept(narrowerConcepts.slice(_MAXLENGTH))}
            <span className={classes.expandIcon}><ExpandLessIcon onClick={() => {setShowAllNarrower(false)}} /></span>
          </>
        }
      </>}
    </>
  }

  function renderConcept(concepts) {
    return concepts?.map(({node}, index) =>
      <span key={index}>
        {createLink(node)}
        <If condition={index < concepts.length - 1}>, </If>
      </span>
    )
  }

  function createLink(item) {
    const {prefLabel, id, definition} = item;
    const _type = getTypeFromSchemes(item);

    const link = () => <Link key={prefLabel} role="link" className={classes.link} aria-label={prefLabel}
      to={formatRoute(ROUTES.EXPLORE_CONCEPT_FORM, {id, type: _type})}
    >
      {upperCaseFirstLetter(prefLabel)}
    </Link>;

    if (definition) {
      return <Tooltip title={definition}>
        {link()}
      </Tooltip>
    } else {
      return link()
    }
  }

  function renderRightContent() {
    return <div className={classes.MT24}>
      <ListOfForms mustFilters={mustFilters} loading={loading} countAll={countAll} />
    </div>
  }
}


const ConceptQueryWithCount = gql`  
query Concept( $id: ID!, $mustFilters: String, $gramFilters: [String], $includeRelated: Boolean!) {
  countAll: lexicalSensesForWrittenRepStartWithCount(mustFilters: $mustFilters)
  concept(id: $id) {
    id
    schemePrefLabel
    prefLabel
    definition
    broaderConcept {
      id
      prefLabel
      definition
      schemes {
        edges {
          node {
            id      
          }
        }
        __typename
      }
      __typename
    }
    relatedConcepts @include(if: $includeRelated) {
      edges {
        node {
          id
          prefLabel
          definition
          schemes {
            edges {
              node {
                id               
              }
            }
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
    narrowerConcepts {
      edges {
        node {
          id
          prefLabel
          definition
          schemes {
            edges {
              node {
                id               
              }
            }
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
    __typename
  }
  topConcepts: grammaticalProperties(
    qs: ""
    filters: $gramFilters
    first: 14
    sortings: [{ sortBy: "prefLabel" }]
  ) {
    edges {
      node {
        id
        prefLabel
        definition
        schemes {
          edges {
            node {
              id             
            }
          }
          __typename
        }
        __typename
      }
      __typename
    }
    __typename
  }
} 
`;


