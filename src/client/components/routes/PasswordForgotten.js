/*
 *  Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 *  and other contributors as indicated by the @author tags.
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
import React, {useEffect} from 'react';
import {Formik} from 'formik';
import {useTranslation} from 'react-i18next';
import {useHistory, Link} from 'react-router-dom';

import {SimpleModalPage} from '../../layouts/responsive/SimpleModalPage';
import FormikInput from '../widgets/forms/formik/FormikInput';
import {Form} from '../widgets/forms/formik/Form';
import {Button} from '../widgets/forms/Button';
import {FormBottom} from '../widgets/forms/FormBottom';
import {notificationService as appNotificationService} from '../../services/NotificationService';
import {
  getUserAuthenticationService
} from '../../services/UserAuthenticationService';
import {useBrowsingHistoryHelperService} from '../../services/BrowsingHistoryHelperService';
 
import formStyles from '../widgets/forms/FormStyles.js';

export function PasswordForgotten(props) {
  const formStyle = formStyles();
  const notificationService = props.notificationService || appNotificationService;
  const {t} = useTranslation();
  const userAuthenticationService = props.userAuthenticationService || getUserAuthenticationService();
  const {resetPassword} = userAuthenticationService.useResetPasswordByMail();
  const history = useHistory();
  const {browsingHistoryHelperService} = useBrowsingHistoryHelperService();

  return <SimpleModalPage
    title={t('PASSWORD_FORGOTTEN_PAGE.TITLE')}
    content={renderContent()}
    titleBgColor="pink"
  />;

  function renderContent() {
    return (
      <React.Fragment>
        <Formik
          initialValues={userAuthenticationService.getResetPasswordByMailFormInitialValues()}
          validationSchema={userAuthenticationService.getResetPasswordByMailValidationSchema()}
          validateOnChange={false}
          validateOnBlur={false}
          onSubmit={handleResetPassword}>
          {({isSubmitting}) => (
            <Form noValidate aria-label="formulaire pour récupérer un compte dont le mot de passe a été oublié">
              <p className={formStyle.info}>{t('PASSWORD_FORGOTTEN_PAGE.PROCEDURE')}</p>
              <FormikInput type="email" name="email" placeholder={t('PASSWORD_FORGOTTEN_PAGE.EMAIL')} aria-label={t('PASSWORD_FORGOTTEN_PAGE.EMAIL')} aria-required={true} />
              <FormikInput type="hidden" name="redirectUri" aria-label="Ne pas toucher ce champ, mesure anti robot" />
              <p className={formStyle.info}>{t('PASSWORD_FORGOTTEN_PAGE.NEXT_STEP')}</p>
              <FormBottom>
                <Button aria-label="Soumettre le formulaire de récupération de mot de passe" type="submit" disabled={isSubmitting} loading={isSubmitting}>{t('PASSWORD_FORGOTTEN_PAGE.SUBMIT')}</Button>
              </FormBottom>
            </Form>
          )}
        </Formik>
      </React.Fragment>
    );
  }

  async function handleResetPassword(values, formikOptions) {
    let res = await resetPassword(values, formikOptions);

    if (res.success) {
      let route = browsingHistoryHelperService.getBackFromSecondaryScreenLocation();
      await notificationService.success(t("PASSWORD_FORGOTTEN_PAGE.MAIL_SENT_SUCCESS"));
      history.push(route ? route.location : '/');
    }
    if (res.globalErrorMessage) {
      notificationService.error(res.globalErrorMessage);
    }
  }

}


