import React from 'react';
import Config from '../../Config';
import {Component} from 'react';

export class ErrorBoundary extends Component {

  constructor (props) {
    super(props);
    this.state = {hasError: false};
  }

  static getDerivedStateFromError(error) {
    return {hasError: true};
  }

  componentDidCatch(error, errorInfo) {
    console.error(error);
    console.error(errorInfo.componentStack);
  }

  render() {
    if (this.state.hasError) {
      return <div style={{
        padding: Config.spacings.medium,
        color: Config.colors.darkgray,
        fontStyle: "italic"
      }}>Un problème technique nous empêche d'afficher ce contenu.</div>;
    }
    return this.props.children;
  }
}
