import React from 'react';
import CookieConsent from "react-cookie-consent";
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../Config';

export const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    padding: Config.spacings.small,
    backgroundColor: Config.colors.lightgray,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      justifyContent: "center"
    }
  },
  text: {
    fontSize: Config.fontSizes.small,
    textAlign: "justify",
    marginRight: Config.spacings.small,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      maxWidth: "385px"
    }
  }
}));



export function CookieDisclaimer(props) {
  const COOKIE_NAME = Config.cookie.name;
  const COOKIE_DURATION = Config.cookie.duration;
  const classes = useStyles();

  return (
    <CookieConsent
      location="bottom"
      buttonText="J'accepte"
      enableDeclineButton
      declineButtonText="Je refuse"
      cookieName={COOKIE_NAME}
      flipButtons
      style={{
        backgroundColor: "#f4f4f4", color: "#000000", justifyContent: 'center', fontSize: "13px", padding: '10px',
        borderTop: 'solid rgba(130, 130, 130, .2) 1px'
      }}
      buttonStyle={{backgroundColor: Config.colors.purple, color: "#ffffff", fontSize: "13px"}}
      declineButtonStyle={{backgroundColor: Config.colors.orange, color: "#ffffff", fontSize: "13px"}}
      contentStyle={{flex: 'unset ! important'}}
      expires={COOKIE_DURATION}
    >
      <div className={classes.container}>
        <div className={classes.text}>
          Ce site utilise des cookies afin d'analyser le trafic des visites. Aucune information personnelle permettant de vous identifier
          n'est partagée
        </div>
      </div>
    </CookieConsent>
  );

}
