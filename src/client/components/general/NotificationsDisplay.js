import React, {useState, useEffect, useRef} from 'react';
import clsx from "clsx";
import {notificationService as appNotificationService} from '../../services/NotificationService';
import crossWhite from '../../assets/images/cross_white.svg';



import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "fixed",
    zIndex: 10,
    width: "100vw",
    height: "100vh",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  messageContainer: {
    position: "relative",
    width: "270px",
    color: Config.colors.white,
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.medium,
    paddingBottom: Config.spacings.medium,
    textAlign: "center",
    fontSize: Config.fontSizes.small,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      width: "400px",
    },

    "&.info": {
      backgroundColor: Config.colors.black,
    },
    "&.success": {
      backgroundColor: Config.colors.pink,
    },
    "&.error": {
      backgroundColor: Config.colors.orange,
    }
  },
  closeButton: {
    height: "10px",
    width: "10px",
    position: "absolute",
    right: Config.spacings.small,
    top: Config.spacings.small,
    background: "none",
    border: "none",
    cursor: "pointer",

    "&:focus": {
      outline: "none",
    },

    "&::-moz-focus-inner": {
      border: 0,
    },

    "& img": {
      width: "100%",
      height: "100%",
    }
  },
  confirmControls: {
    display: "flex",
    justifyContent: "center",
    marginTop: Config.spacings.small,
    textDecoration: "underline",
    cursor: "pointer",

    "& .control:first-child": {
      marginRight: Config.spacings.medium,
    }
  }

}));




let messageCounter = 0;

export function NotificationsDisplay(props) {
  const classes = useStyles();
  const notificationService = props.notificationService || appNotificationService;
  const [messages, updateMessages] = useState([]);
  const closeButtonRef = useRef();

  useEffect(() => {
    const subscription = notificationService.messages$.subscribe((newMessage) => {
      newMessage.id = messageCounter++;
      updateMessages(currentMessages => currentMessages.concat([newMessage]));
    });
    return () => subscription.unsubscribe();
  }, [notificationService.messages$]);

  useEffect(() => {
    if (closeButtonRef.current) {
      closeButtonRef.current.focus();
    }
  });

  return messages.map(renderMessage);

  function renderMessage(message) {

    const ariaLive = message?.type === "error" ? "polite" : "assertive";

    return (
      <div key={message.id} className={classes.container} onClick={() => closeMessage(message)}
        role="button" aria-label="Cliquer ici pour fermer la popup">
        <div className={clsx(classes.messageContainer, message.type)}
          role="button" aria-label={message.message} aria-live={ariaLive} aria-atomic="true"
          onClick={e => messageBoxClicked(e, message)}>
          <button className={classes.closeButton} onClick={() => closeMessage(message)} ref={closeButtonRef} aria-label="Fermer la notification">
            <img src={crossWhite} alt="Fermer" />
          </button>
          <div>{message.message}</div>
          <If condition={message.confirm}>
            <div className={classes.confirmControls}>
              <div className="control" onClick={() => closeMessage(message, {choice: 'yes'})} role="button" aria-label="Cliquer ici pour confirmer le changement">
                {message.yesText}
              </div>
              <div className="control" onClick={() => closeMessage(message, {choice: 'no'})} role="button" aria-label="Cliquer ici pour annuler le changement">
                {message.noText}
              </div>
            </div>
          </If>
        </div>
      </div>
    )
  }

  /**
   * Different strategies depending on the type of message : 
   *
   * - If this is a message without confirmation controls, any click in the message box closes the message (the same way cliking outside does)
   * - If this is a message with confirmation, a click in the message box does nothing
   */
  function messageBoxClicked(evt, message) {
    evt.stopPropagation();
    if (!message.confirm) {
      closeMessage(message);
    }
  }

  function closeMessage(message, {choice} = {}) {
    if (!choice) {
      /* 
       * In case of "close" action (without clicking the confirmation controls, but by clicking on the cross or outside the box,
       * if this is a confirmation message, consider it to be a "no" confirmation, if this is a message without confirmation controls,
       * consider it to be a "yes" confirmation.
       */
      choice = message.confirm ? 'no' : 'yes';
    }
    choice === 'no' ? message.confirmWithNo() : message.confirmWithYes();
    updateMessages((currentMessages) => {
      return currentMessages.filter(a => a.id != message.id);
    });
  }
}
