import React from 'react';
import gif from '../../assets/images/ddf.gif'
import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  screen: {
    background: Config.colors.white,
    height: "100vh",
    width: "100vw",
    paddingTop: Config.spacings.medium,
  },
  logo: {
    marginLeft: "auto",
    marginRight: "auto",
    width: "180px",
    "& img": {
      width: "100%",
    }
  }
}));


export function DdfLoadingSplashScreen({children}) {
  const classes = useStyles();
  return (
    <div className={classes.screen}>
      <div className={classes.logo}>
        <img src={gif} alt="Chargement..." />
      </div>
      {children}
    </div>
  );

}
