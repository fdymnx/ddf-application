import React from 'react';

import {useParams} from 'react-router-dom';
import {ResponsiveMainLayout} from '../../layouts/responsive/ResponsiveMainLayout';
import {FormMobileHeader} from '../../layouts/mobile/MobileMainLayout/FormMobileHeader';
import {MobileFooter} from '../../layouts/mobile/MobileMainLayout/MobileFooter';

/**
 * Usage :
 *
 * <FormLayout>
 *    <Content />
 * </FormLayout>
 *
 * showBackIcon
 *
 * Properties :
 * - desktopSideColumn: optional element to render a side column in the desktop layout
 *
 * The form is extracted from the current routes, the component  must used on a route
 * with `:formQuery` paratemer (see routes.js)
 */
export function FormLayout({showBackIcon = false, currentPlace, children}) {
  let params = useParams();
  const currentForm = params.formQuery;


  function renderMobileHeader() {
    return <FormMobileHeader search={true} geolocMarker={true} showBackIcon={showBackIcon} currentForm={currentForm} currentPlace={currentPlace} />;
  }

  return (
    <ResponsiveMainLayout
      mobileFooter={<MobileFooter currentForm={currentForm} />}
      mobileHeader={renderMobileHeader()}
      currentForm={currentForm}
    >
      {children}
    </ResponsiveMainLayout>
  );
}
