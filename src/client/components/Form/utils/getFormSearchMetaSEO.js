import React from "react";
import {MetaSEO} from "../../widgets/MetaSEO";
import {useTranslation} from 'react-i18next';
import {getLexicalSenseMetas} from "../../LexicalSense/utils/getLexicalSenseMetaSEO";

export function getFormSearchMetaSEO({url, formQuery, lexicalSenses, lexicalSensesCount}){
  const {t} = useTranslation();

  if (!lexicalSenses){
    return null;
  }

  let definition = "";
  let extraLabels = {};
  let keywords = [];

  if (lexicalSensesCount === 0 || lexicalSenses.edges.length === 0) {
    definition = t("FORM.NO_DEFINITION");
  } else{
    ({definition, extraLabels, keywords} = getLexicalSenseMetas({lexicalSense: lexicalSenses.edges[0].node, t}));

    if(lexicalSensesCount > 1){
      definition = t("FORM.DEFINITIONS_COUNT", {
        count: lexicalSensesCount,
        definition
      });
    }
  }

  return (
    <MetaSEO
      url={url}
      title={t('DOCUMENT_TITLES.FORM_SEARCH', {formQuery})}
      description={`${formQuery} - ${definition}`}
      ogDescription={definition}
      name={formQuery}
      addScript={true}
      extraLabels={extraLabels}
      keywords={keywords}
    />
  )
}