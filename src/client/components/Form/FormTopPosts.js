import {gql} from "@apollo/client";

export const FormTopPostsQuery = gql`
  query FormTopPosts_Query($formQs: String) {
    topPostAboutEtymology: topPostAboutEtymologyForAccurateWrittenForm(formQs: $formQs){
      id
      content
    }

    topPostAboutForm: topPostAboutFormForAccurateWrittenForm(formQs: $formQs){
      id
      content
    }
  }
`;
