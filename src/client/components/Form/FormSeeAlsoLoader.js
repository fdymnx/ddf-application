import React from 'react';
import {FormLexicalSenseListUseStyles} from "./FormLexicalSenseListStyles";
import globalStyles from '../../assets/stylesheets/globalStyles.js';
import clsx from 'clsx';

import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: Config.spacings.small,
    color: Config.colors.darkgray,
    backgroundColor: Config.colors.white,
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      "&::last-child": {
        marginBottom: 0
      }
    },
  },
  description: {
    marginLeft: Config.spacings.medium,
    marginRight: Config.spacings.medium,
    position: "relative",
    maxHeight: "100px",
    overflow: "hidden",
    fontFamily: "Raleway",
    fontSize: Config.fontSizes.medium,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      height: "100px"
    },
    "&.active": {
      position: "absolute",
      top: "0",
      height: "100%",
      width: "100%",
      backgroundImage: "linear-gradient(to bottom, rgba(0,0,0,0), white)",
      pointerEvents: "none"
    }
  },
  descriptionPlaceholder: {
    marginTop: Config.spacings.small,
    marginBottom: Config.spacings.small,
    backgroundColor: Config.colors.loading_placeholder,
    height: Config.fontSizes.medium,
  }

}));


export function FormSeeAlsoLoader() {
  const classesFormLexicalSenseList = FormLexicalSenseListUseStyles();
  const classes = useStyles();
  const gS = globalStyles();
  return (
    <div className={clsx(classes.container, gS.loadingOverlay, classesFormLexicalSenseList.lexicalSense)} >
      <div className={classes.description}>
        <div className={classes.descriptionPlaceholder} />
      </div>
    </div>
  );
}
