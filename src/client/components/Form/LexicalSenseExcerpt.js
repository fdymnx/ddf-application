import React, {useState, useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import uniq from 'lodash/uniq';
import {Link, useRouteMatch} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useTranslation} from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import {PurifyHtml} from '../../utilities/PurifyHtml';
import {encodeURIComponentIfNeeded} from "../../utilities/helpers/tools";
import DivButton from '../widgets/DivButton';
import {ROUTES} from '../../routes';
import {useStyles} from "./LexicalSenseExcerptStyle";
import globalStyles from '../../assets/stylesheets/globalStyles.js';
import Config from '../../Config';

LexicalSenseExcerpt.propTypes = {
  sense: PropTypes.object,
  countryData: PropTypes.shape({
    names: PropTypes.arrayOf(PropTypes.string),
    color: PropTypes.string,
  }),
  partOfSpeechList: PropTypes.array,
  source: PropTypes.string,
  className: PropTypes.string,
};

/**
 * 
 * @param {funcion} disableLinkByCallBack : disable Link around Excerpt and replace it with onClick to get the selected Sense in a list of LexicalSenseExerpt 
 * @returns 
 */
export function LexicalSenseExcerpt({sense, countryData, source, partOfSpeechList, className, disableLinkByCallBack = false, selectedDefId = null}) {
  const classes = useStyles();
  const gS = globalStyles();
  const {t} = useTranslation();
  const match = useRouteMatch();

  const [hasOverlay, sethasOverlay] = useState(false);
  const descriptionTextRef = useRef(null);
  const descriptionContainerRef = useRef(null);

  useEffect(() => {
    if (descriptionTextRef.clientHeight > descriptionContainerRef.clientHeight) {
      sethasOverlay(true);
    }
  }, [descriptionTextRef, descriptionContainerRef]);

  let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
    formQuery: encodeURIComponentIfNeeded(match.params.formQuery),
    lexicalSenseId: sense.id,
  });

  if (!sense.definition) {
    return null;
  }

  if (disableLinkByCallBack) {
    return (
      <DivButton
        aria-label="Choisir cette définition"
        onClick={() => disableLinkByCallBack(sense)}
        key={sense?.id}
        classesName={clsx(classes.container, classes.containerBorder, className)}
        selected={selectedDefId == sense?.id}
        data-testid={"lexical-sense-excerpt/" + sense.id}
      >
        {renderContent(countryData.color)}
      </DivButton>
    );
  } else {
    return (
      <div key={sense?.id}
        className={clsx(classes.container, classes.containerBorder, className)}
        data-testid={"lexical-sense-excerpt/" + sense.id}>
        {renderContent(countryData.color)}
      </div>
    );
  }


  function renderContent(color) {
    
    countryData.names = countryData.names.filter(name => !!name);

    return (<>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="stretch"

      >
        <Grid item xs={9} className={classes.countriesRow}>
          <div className={classes.countries} style={{backgroundColor: Config.colors[color]}}>
            <Choose>
              <When condition={countryData.names.length > 0}>{countryData.names.join(', ')}</When>
              <Otherwise>{t('GEOGRAPHICAL_DETAILS.NO_COUNTRY_LABEL')}</Otherwise>
            </Choose>
          </div>
        </Grid>
        <Grid item xs={3} className={classes.sources}>
          Source : {source}
        </Grid>
      </Grid>

      {disableLinkByCallBack ?
        renderInnerContent()
        :
        <Link role="link" aria-label="Définition détaillée" to={senseUrl} className={gS.outlineFocus} >
          {renderInnerContent()}
        </Link>
      }
      {!disableLinkByCallBack &&
        <Link role="link" aria-label="Définition détaillée" id="xxxx" to={senseUrl} className={gS.outlineFocus}>
          <div className={classes.rightEnd}>
            <b>[</b>...<b>]</b>
          </div>
        </Link>
      }
    </>)
  }

  function renderInnerContent() {
    return (
      <div className={classes.greyText} >
        <div className={classes.pos}>{uniq(partOfSpeechList).join(', ')}</div>
        <div ref={descriptionTextRef} className={classes.description}>
          <PurifyHtml className={classes.text} removeAllowedTags={['a']} html={sense.definition} />
        </div>
        <div className={clsx(hasOverlay && classes.descriptionOverlay)}></div>
      </div >
    )
  }
}
