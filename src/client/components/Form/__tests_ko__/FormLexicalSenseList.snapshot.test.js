import React from 'react';
import {render, cleanup, getNodeText} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import waait from 'waait';

import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {FormLexicalSenseList} from '../FormLexicalSenseList';
import {ROUTES} from '../../../routes';
import {
  formGqlMocks,
} from './__gql_mocks__/FormLexicalSenseList.gqlMocks.js';
import {
  formSeeAlsoGqlMocks
} from './__gql_mocks__/FormSeeAlso.gqlMocks.js';

afterEach(() => {
  cleanup();
});

it.only('renders snapshot', async () => {
 // console.log({gqlMocks: [].concat(formSeeAlsoGqlMocks, formGqlMocks)})

  const {container} = renderWithMocks({
    locationPath: "/form/affairé",
    gqlMocks: [].concat(formSeeAlsoGqlMocks, formGqlMocks),
    element: <FormLexicalSenseList formQuery="affairé" currentPlace={{id: "geonames:6614192"}} />,
    routePath: ROUTES.FORM_SEARCH
  });
  await act(waait);
  await act(waait);
  expect(container).toMatchSnapshot();
});


