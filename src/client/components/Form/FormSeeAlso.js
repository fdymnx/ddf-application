import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useQuery, gql} from '@apollo/client';
import flatten from 'lodash/flatten';
import uniq from 'lodash/uniq';
import {ROUTES} from '../../routes';
import {gqlSemanticRelationFormFragment, getSemanticRelationFormList} from '../../utilities/helpers/semanticRelationFormList';

import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {FormSeeAlsoLoader} from "./FormSeeAlsoLoader";
import globalStyles from '../../assets/stylesheets/globalStyles.js';

export let gqlFormSeeAlsoQuery = gql`
  query FormSeeAlso_Query($formQs: String) {
    associativeRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs, filterOnType: associativeRelation) {
      edges {
        node {
          id
          ...SemanticRelationFormFragment
        }
      }
    }

    graphicRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs, filterOnType: graphicRelation) {
      edges {
        node {
          id
          ...SemanticRelationFormFragment
        }
      }
    }

    morphologicalRelationSemanticRelations: semanticRelationsForAccurateWrittenForm(formQs: $formQs, filterOnType: morphologicalRelation) {
      edges {
        node {
          id
          ...SemanticRelationFormFragment
        }
      }
    }
  }

  ${gqlSemanticRelationFormFragment}
`;

import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: Config.colors.lightgray,
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    "& h4": {
      marginBottom: Config.spacings.small,
      fontSize: Config.fontSizes.medium,
      color: Config.colors.purple
    },
    "& p": {
      fontSize: Config.fontSizes.small,
      color: Config.colors.darkgray,
      "& a": {
        color: "inherit",
        fontStyle: "italic"
      }
    },
    "& p:not(:last-child)": {
      marginBottom: "2px"
    },
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      backgroundColor: Config.colors.white,
      "& .title": {
        display: "none"
      },
      "& p": {        
        fontSize: Config.fontSizes.medium,
        color: Config.colors.black,
        "& a": {          
          fontStyle: "normal !important",
          color: `${Config.colors.purple} !important`         
        }
      }
    }
  },
  inlineTitle: {
    fontWeight: Config.fontWeights.bold,
    [theme.breakpoints.up(Config.mediaQueries.mobileMaxWidth)]: {
      fontWeight: Config.fontWeights.normal
    },
  }
}));



FormSeeAlso.propTypes = {
  formQuery: PropTypes.string.isRequired,
};

export function FormSeeAlso(props) {
  const classes = useStyles();
  const {t} = useTranslation();
  const {formQuery} = props;
  const {data, loading, error} = useQuery(gqlFormSeeAlsoQuery, {
    variables: {
      formQs: formQuery,
    },
    fetchPolicy: 'cache-and-network',
  });
  apolloErrorHandler(error, 'log');

  if (loading && !data) {
    return <SeeAlsoContainer>
      <FormSeeAlsoLoader />
    </SeeAlsoContainer>
  }

  if (error) return null;

  const {associativeRelationSemanticRelations, graphicRelationSemanticRelations, morphologicalRelationSemanticRelations} = data;

  /* Don't render the FormSeeAlso component if no results for any of the categories */
  const associativeRel = getSemRelations(associativeRelationSemanticRelations, formQuery);
  const graphicRel = getSemRelations(graphicRelationSemanticRelations, formQuery);
  const morphologicalRel = getSemRelations(morphologicalRelationSemanticRelations, formQuery);

  if (!associativeRel && !graphicRel && !morphologicalRel) {
    return <SeeAlsoContainer>
      {t('FORM.SEE_ALSO.NO_RELATED')}
    </SeeAlsoContainer>
  }

  return (
    <SeeAlsoContainer>
      <If condition={associativeRel}>
        <p>
          <span className={classes.inlineTitle}>{t('FORM.SEE_ALSO.RELATED')}&nbsp;:&nbsp;</span>
          {renderSemanticRelations(associativeRel)}
        </p>
      </If>

      <If condition={graphicRel}>
        <p>
          <span className={classes.inlineTitle}>{t('FORM.SEE_ALSO.FORM_VARIATION')}&nbsp;:&nbsp;</span>
          {renderSemanticRelations(graphicRel)}
        </p>
      </If>
      <If condition={morphologicalRel}>
        <p>
          <span className={classes.inlineTitle}>{t('FORM.SEE_ALSO.DERIVED')}&nbsp;:&nbsp;</span>
          {renderSemanticRelations(morphologicalRel)}
        </p>
      </If>
    </SeeAlsoContainer>
  );

  // get semantic relations, ordered, deduplicated and filtered from formQuery
  function getSemRelations(semanticRelations, ignoreValue) {
    if (semanticRelations?.edges?.length > 0) {
      return uniq(flatten(semanticRelations.edges.map(({node: semanticRelation}) => getSemanticRelationFormList(semanticRelation))))
        .sort()
        .filter((value) => value !== ignoreValue);
    } else {
      return false;
    }
  }

  function renderSemanticRelations(forms) {
    if (!Array.isArray(forms)) {
      return null;
    }
    return forms.map((form, index) => {
      return (
        <React.Fragment key={index}>
          <Link role="link" aria-label={"Lancer une recherche sur " + form} to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: form})}>{form}</Link>
          <If condition={index < forms.length - 1}>
            <span>, </span>
          </If>
        </React.Fragment>
      );
    });
  }
}


export function SeeAlsoContainer({children}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const gS = globalStyles();
  return <React.Fragment>
    <div className={gS.secondaryTitle}>{t('FORM.SEE_ALSO.TITLE')}</div>
    <div className={classes.container}>
      <h4 className={"title"}>{t('FORM.SEE_ALSO.TITLE')}</h4>
      {children}
    </div>
  </React.Fragment>
}