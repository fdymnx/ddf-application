import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

const border_width = "6px";
const border_width_desktop = "3px";

export const useStyles = makeStyles((theme) => ({
  container: {
    padding: Config.spacings.medium - Config.spacings.tiny,
    margin: `${Config.spacings.verytiny}px ${Config.spacings.tiny}px ${Config.spacings.medium}px ${Config.spacings.tiny}px`,
    color: Config.colors.darkgray,
    backgroundColor: Config.colors.white,
    maxWidth: "98vw"
  },
  containerBorder: {
    borderRight: `solid ${border_width}`,
    borderBottom: `solid ${border_width}`,
    borderColor: `${Config.colors.weirdpink} ! important`,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      borderWidth: border_width_desktop,
    }
  },
  placeholder: {
    backgroundColor: Config.colors.loading_placeholder
  },

  countrySourcePlaceholder: {
    width: "72px",
    backgroundColor: Config.colors.lightgray
  },
  greyText: {
    color: Config.colors.darkgray
  },
  patchHeight: {
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      height: "3px"
    },
  },
  sources: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    textAlign: "end",
    maxWidth: "40%",
    fontStyle: "italic",
    fontSize: Config.fontSizes.small,
    fontWeight: Config.fontWeights.bold,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny,
    color: Config.colors.darkgray
  },

  rightEnd: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: Config.spacings.small,
    color: Config.colors.purple
  },

  spaceBetween: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },

  countriesRow: {
    display: "flex"
  },
  countries: {
    maxWidth: "95%",
    overflow: "hidden",
    overflowWrap: "break-word",
    textOverflow: "ellipsis",
    color: Config.colors.white,
    whiteSpace: "nowrap",
    paddingTop: "2px",
    paddingBottom: "2px",
    paddingLeft: Config.spacings.small,
    paddingRight: Config.spacings.small,
    fontSize: Config.fontSizes.medium,
    fontWeight: Config.fontWeights.bold
  },
  pos: {
    marginTop: Config.spacings.small,
    marginBottom: Config.spacings.tiny,
    fontSize: Config.fontSizes.small,
    fontStyle: "italic"
  },
  posPlaceholder: {
    marginTop: Config.spacings.small,
    width: "60px",
    height: "12px",
    backgroundColor: Config.colors.lightgray
  },
  descriptionPlaceholder: {
    marginTop: Config.spacings.small,
    marginBottom: Config.spacings.small,
    height: "12px",
    backgroundColor: Config.colors.lightgray
  },
  description: {
    position: "relative",
    maxHeight: "100px",
    overflow: "hidden",
    fontFamily: "Raleway",
    fontSize: Config.fontSizes.medium,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      height: "100px"
    },
    "& a.description": {
      display: "block",
      textDecoration: "none",
      color: "inherit"
    }
  },
  descriptionOverlay: {
    position: "absolute",
    top: "0",
    height: "100%",
    width: "100%",
    backgroundImage: "linear-gradient(to bottom, rgba(0,0,0,0), white)",
    pointerEvents: "none"
  },




}));