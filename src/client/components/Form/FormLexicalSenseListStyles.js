import Config from '../../Config';
import {makeStyles} from "@material-ui/core/styles";

export const FormLexicalSenseListUseStyles = makeStyles((theme) => ({
  lexicalSenses: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    flex: "1",
    width: "100%",
  },
  lexicalSense: {
    width: "100%",
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      width: `calc(50% - ${Config.spacings.medium / 2}px)`,
      marginBottom: Config.spacings.medium
    },
  },
  noResultsText: {
    fontStyle: "italic",
  },
  width100: {
    width: "100%",
  },
}));