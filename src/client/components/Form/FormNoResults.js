import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useTranslation} from 'react-i18next';
import {gql, useQuery} from '@apollo/client';

import {otherCaseFirstLetter} from "../../utilities/helpers/tools";

import {apolloErrorHandler} from '../../utilities/apolloErrorHandler';
import {ROUTES} from '../../routes';
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../Config';
import addWhiteIcon from '../../assets/images/add_white.svg';

const formSuggestionsQuery = gql`
  query Search_Query($first: Int, $qs: String, $qsOtherCase:String) {
    lexicalSensesCount: lexicalSensesCountForAccurateWrittenForm(formQs: $qsOtherCase)
    forms(qs: $qs, first: $first) {
      edges {
        node {
          id
          writtenRep
        }
      }
    }
  }
`;


const useStyles = makeStyles((theme) => ({
  container: {
    padding: Config.spacings.medium,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      padding: 0
    },
    marginBottom: Config.spacings.medium,
    backgroundColor: Config.colors.lightgray,
    color: Config.colors.darkgray,
    '& ul': {
      listStyleType: 'none'
    },
    '& li': {
      marginTop: Config.spacings.tiny
    },
    '& a': {
      color: Config.colors.purple,
      textDecoration: 'inherit',
      fontFamily: 'Raleway',
      fontWeight: Config.fontWeights.semibold
    },
    '& hr': {
      marginTop: Config.spacings.medium,
      marginBottom: Config.spacings.medium
    }
  },
  noResultsText: {
    fontStyle: 'italic'
  },
  suggestionsText: {
    fontWeight: Config.fontWeights.light
  },
  icon: {
    height: "25px",
    marginRight: Config.spacings.small
  },
  button: {
    minWidth: '50px',
    padding: "10px 20px",
    color: `${Config.colors.white} ! important`,
    backgroundColor: Config.colors.purple,
    fontStyle: 'normal',
    margin: '10px',
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
  }

}));

FormNoResults.propTypes = {
  formQuery: PropTypes.string.isRequired,
};

export function FormNoResults(props) {
  const {t} = useTranslation();
  const classes = useStyles();
  const {formQuery} = props;
  const formQueryOtherCase = otherCaseFirstLetter(formQuery)

  const {data, loading, error} = useQuery(formSuggestionsQuery, {
    variables: {
      first: 10,
      qs: formQuery,
      qsOtherCase: formQueryOtherCase
    },
    fetchPolicy: 'cache-and-network',
  });
  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;

  const {forms, lexicalSensesCount} = data;
  let filteredForms = forms.edges.filter(({node: {writtenRep}}) => writtenRep !== formQuery).map(({node}) => node);

  return (
    <div className={classes.container}>
      <div className={classes.noResultsText}>
        {t('FORM.NO_RESULTS')}
        <div className={classes.center} >
          <Link role="link" aria-label={"Ajouter une entrée"} className={classes.button}
            to={formatRoute(ROUTES.CREATE_LEXICAL_SENSE) + "?form=" + formQuery}
          >
            <img className={classes.icon} src={addWhiteIcon} alt={"Ajouter une entrée"} /> Ajouter une entrée
          </Link>
        </div>

      </div>

      <If condition={lexicalSensesCount > 0}>
        <br />
        <div className={classes.suggestionsText}>
          {"Vouliez-vous rechercher "}
          <Link role="link" aria-label={"Lancer une recherche sur " + formQueryOtherCase} to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: formQueryOtherCase})}>{formQueryOtherCase}</Link>
          {lexicalSensesCount === 1 ? " qui a une définition ?" : ` qui a ${lexicalSensesCount} définitions ?`}
        </div>
      </If>

      <If condition={filteredForms.length > 0}>
        <hr />
        <div className={classes.suggestionsText}>{t('FORM.CLOSE_FORMS_SUGGESTIONS')}</div>
        <div>
          <ul role="navigation" aria-label="Liste de suggestions proches de votre recherche">
            {filteredForms.map((form) => (
              <li key={form.id}>
                <Link role="link" aria-label={"Lancer une recherche sur " + form.writtenRep} to={formatRoute(ROUTES.FORM_SEARCH, {formQuery: form.writtenRep})}>{form.writtenRep}</Link>
              </li>
            ))}
          </ul>
        </div>
      </If>
    </div>
  );


}
