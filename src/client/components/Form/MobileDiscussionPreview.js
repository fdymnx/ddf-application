import React from 'react';
import PropTypes from 'prop-types';
import {PurifyHtml} from '../../utilities/PurifyHtml';
import {makeStyles} from "@material-ui/core/styles"; 
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  discussion: {
    backgroundColor: Config.colors.lightgray,
    marginTop: Config.spacings.tiny,
    fontSize: Config.fontSizes.small
  },
  header: {
    color: Config.colors.orange,
    fontWeight: Config.fontWeights.bold,
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny
  },
  content: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingBottom: Config.spacings.small
  }
}));


MobileDiscussionPreview.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export function MobileDiscussionPreview({title, content}) {
  const classes = useStyles();

  return (
    <div className={classes.discussion}>
      <div className={classes.header}>{title}</div>
      <PurifyHtml className={classes.content} html={content} />
    </div>
  );
}
