/*
* Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
* and other contributors as indicated by the @author tags.
*
* Licensed under the Apache License, Version 2.0 (the 'License');
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an 'AS IS' BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/
import React from 'react';

import {useQuery, gql} from "@apollo/client";
import {unique} from "../../utilities/helpers/tools";
import {ChipsList} from "../widgets/ChipsList";

const LexicalSenseSemanticPropertiesQuery = gql`
 query LexicalSenseSemanticProperties_Query($lexicalSenseId: ID!) {
   lexicalSense(id: $lexicalSenseId){
     id
     semanticProperties {
        edges {
          node {
            id
            prefLabel
            schemes {
              edges {
                node {
                  id
                }
              }
            }
          }
        }
      }
   }
 } 
`;




/**
* This component render all semantic properties of a lexicalSense 
*
* @param {string} lexicalSenseId 
* @param {boolean} showDeleteIcon
* @param {function} [setConfirmToDelete]  : optional, work with isMine, if not null :enable delete icon next to each item (only if user is allowed to remove it)
* @param {function} [isMine] :optional, work with setConfirmToDelete
*/
export function LexicalSenseSemanticProperties({lexicalSenseId, showDeleteIcon, isMine, setConfirmToDelete}) {
  const {data: {lexicalSense} = {}, loading} = useQuery(LexicalSenseSemanticPropertiesQuery, {
    variables: {
      lexicalSenseId,
    },
    fetchPolicy: 'cache-and-network'
  });

  if (loading) {
    return null;
  } else {

    // we use array of object [{label},{label},{label} ] to display chips and optional delete button when setConfirmToDelete is set
    const semanticProperties = lexicalSense?.semanticProperties?.edges?.map(({node}) => node);
    const semanticPropertyList = semanticProperties?.length > 0 ? unique(semanticProperties, 'prefLabel') : [];

    return (
      <ChipsList list={semanticPropertyList}
        isMine={isMine}
        setConfirmToDelete={setConfirmToDelete}
        showDeleteIcon={showDeleteIcon}
      />
    )
  }
}
