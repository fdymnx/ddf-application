/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React, {useEffect} from 'react';

import clsx from 'clsx';
import upperFirst from 'lodash/upperFirst';
import uniq from 'lodash/uniq';
import {useTranslation} from 'react-i18next';
import {useLazyQuery} from "@apollo/client";
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {makeStyles} from '@material-ui/core/styles';
import {UsageExamples} from './UsageExamples';
import {ROUTES} from '../../routes';
import Config from "../../Config";
import {ShowWhereIAM} from '../widgets/ShowWhereIAM';
import LoadingGif from "../widgets/LoadingGif";
import {LexicalSenseSameSemanticRelationList} from "./LexicalSenseSameSemanticRelationList";
import {LexicalSenseSemanticProperties} from "./LexicalSenseSemanticProperties";
import {getLocalizedEntityData} from '../../utilities/helpers/countries';
import {PurifyHtml} from '../../utilities/PurifyHtml';
import {unique} from "../../utilities/helpers/tools";
import {gqlLexicalSenseQuery} from "./LexicalSenseDefinition.gql";
import {useMediaQueries} from '../../layouts/MediaQueries';

const useStyles = makeStyles((theme) => ({
  section: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    marginBottom: Config.spacings.medium,
    backgroundColor: Config.colors.white
  },
  definitionSection: {
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingTop: Config.spacings.medium
    }
  },
  countriesRow: {
    display: "flex",

    "& a": {
      textDecoration: 'inherit'
    }
  },
  countries: {
    color: "white",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    fontSize: Config.fontSizes.medium,
    fontWeight: Config.fontWeights.medium,
    paddingTop: "2px",
    paddingBottom: "2px",
    paddingLeft: Config.spacings.small,
    paddingRight: Config.spacings.small,
    backgroundColor: Config.colors.darkgray,
  },
  pos: {
    marginTop: Config.spacings.small,
    fontSize: Config.fontSizes.medium,
    fontStyle: 'italic'
  },

  tags: {
    marginTop: Config.spacings.small,
  },
  description: {
    marginTop: Config.spacings.small,
    fontFamily: 'Raleway',
    fontSize: Config.fontSizes.large,
  }
}));

/**
 * Display a Lexical sense definition, with country, example, all relation with concept (verbe,person,mood...)
 * @param {function} setConfirmToDelete: if not false, display delete icon on right on each chips
 * @param {boolean} showDeleteIcon: show or not the delete icon on each chips 
 * @param {boolean} hideUsageExamples: show or not the UsageExamples
 * @param {function} setSemanticRelationsInParent : optional, if you need this semanticRelations DATA in the parent. 
 */
export function LexicalSenseDefinition({formQuery, lexicalSenseId,
  showDeleteIcon = false, setConfirmToDelete = false,
  setSemanticRelationsInParent = false, hideUsageExamples = false
}) {
  const {isDesktop} = useMediaQueries();
  const classes = useStyles();
  let {t} = useTranslation();

  const [getData, {data: {lexicalSense} = {}, loading}] = useLazyQuery(gqlLexicalSenseQuery, {
    variables: {lexicalSenseId},
    fetchPolicy: 'cache-and-network'
  });

  useEffect(() => {
    getData();
  }, []);


  if (loading || !lexicalSense) {
    return <div className={clsx(classes.section, classes.definitionSection)}>
      <LoadingGif />
    </div>
  }

  const {definition, usageExamples, lexicalEntryGrammaticalPropertyLabels, places} = lexicalSense;
  const regionData = places?.edges?.length > 0 ? getLocalizedEntityData(places, isDesktop ? 7 : 4) : {};
  const color = Config.colors[regionData.color];
  const grammaticalPropertyList = lexicalEntryGrammaticalPropertyLabels?.length > 0 ? uniq(lexicalEntryGrammaticalPropertyLabels) : [];

  return (
    <ShowWhereIAM path={"src/client/components/LexicalSense/LexicalSenseDefinition.js"}>
      <div className={clsx(classes.section, classes.definitionSection)}>
        <div className={classes.countriesRow} >
          <Link role="link" aria-label="Localisations"
            className={classes.countries} style={{backgroundColor: color}}
            to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_COUNTRIES, {
              formQuery,
              lexicalSenseId,
            })}
          >
            <Choose>
              <When condition={regionData?.names?.length > 0}>{regionData.names.join(', ')}</When>
              <Otherwise>{t('GEOGRAPHICAL_DETAILS.NO_COUNTRY_LABEL')}</Otherwise>
            </Choose>
          </Link>
        </div>

        {renderConditionnalCommaList(grammaticalPropertyList)}

        <div className={classes.tags}>
          <LexicalSenseSemanticProperties
            lexicalSenseId={lexicalSenseId}
            setSemanticRelationsInParent={setSemanticRelationsInParent}
            setConfirmToDelete={setConfirmToDelete}
            showDeleteIcon={showDeleteIcon}
          />

          <LexicalSenseSameSemanticRelationList
            lexicalSenseId={lexicalSenseId}
            setSemanticRelationsInParent={setSemanticRelationsInParent}
            setConfirmToDelete={setConfirmToDelete}
            showDeleteIcon={showDeleteIcon}
          />
        </div>

        <PurifyHtml className={classes.description} html={definition} />
        {!hideUsageExamples &&
          <UsageExamples usageExamples={usageExamples?.edges?.map((e) => e.node)} />
        }
      </div>
    </ShowWhereIAM>
  )


  function renderConditionnalCommaList(arr) {
    if (arr.length === 0) return null;

    return (
      <div className={classes.pos}>
        {arr.map((item, index) => {
          if (index === 0) {
            item = upperFirst(item);
          }
          return (
            <React.Fragment key={index}>
              <span>{item}</span>
              <If condition={index < arr.length - 1}>, </If>
            </React.Fragment>
          );
        })}
      </div>
    );
  }




}



