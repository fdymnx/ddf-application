import Config from "../../Config"
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  placeholder: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    marginBottom: Config.spacings.medium,
    backgroundColor: Config.colors.white,
  },
  countries: {
    width: "100px",
    backgroundColor: Config.colors.loading_placeholder,
    paddingTop: "2px",
    paddingBottom: "2px",
    paddingLeft: Config.spacings.small,
    paddingRight: Config.spacings.small,
    marginBottom: Config.spacings.small,
  },
  fakeTextLine: {
    position: "relative"
  },
  textPlaceholder: {
    position: "absolute",
    width: "100%",
    height: "75%",
    top: 0,
    backgroundColor: Config.colors.loading_placeholder,
  }
}));

export default useStyles