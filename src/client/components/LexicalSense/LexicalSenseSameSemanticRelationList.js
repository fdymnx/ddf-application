/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import React from 'react';

import {useQuery, gql} from "@apollo/client";
import {unique} from "../../utilities/helpers/tools";
import {ChipsList} from "../widgets/ChipsList";
import {gqlLexicalSenseSemanticRelationTypeFragment} from '../../utilities/helpers/lexicalSenseSemanticRelationList';

const LexicalSenseSameSemanticRelationListQuery = gql`
  query LexicalSenseSameSemanticRelationList_Query($lexicalSenseId: ID!, $includeCreator: Boolean = false) {
    lexicalSense(id: $lexicalSenseId){
      id
      ...LexicalSenseSemanticRelationTypeFragment 
    }
  }
  ${gqlLexicalSenseSemanticRelationTypeFragment}
`;




/**
 * This component render all sameWrittenRep semantic relation of a lexicalSense
 * so between a definition and a definition of the same form
 *
 * @param {string} lexicalSenseId 
 * @param {boolean} showDeleteIcon
 * @param {function} [setConfirmToDelete]  : optional, work with canUpdate, if not null :enable delete icon next to each item (only if user is allowed to remove it)
 * @param {function} setSemanticRelationsInParent : optional, if you need this semanticRelations DATA in the parent. 
 */
export function LexicalSenseSameSemanticRelationList({lexicalSenseId, showDeleteIcon, setConfirmToDelete, setSemanticRelationsInParent}) {
  const {data: {lexicalSense} = {}, loading} = useQuery(LexicalSenseSameSemanticRelationListQuery, {
    variables: {
      lexicalSenseId,
      includeCreator: !!setConfirmToDelete,
    },
    fetchPolicy: 'cache-and-network',
    onCompleted: data => {
      if (data?.lexicalSense?.semanticRelations && setSemanticRelationsInParent) {
        setSemanticRelationsInParent(data?.lexicalSense?.semanticRelations);
      }
    }
  });

  if (loading) {
    return null;
  } else {

    const semanticRelations = formatSemanticRelations(lexicalSense?.semanticRelations, lexicalSenseId);
    return (
      <ChipsList
        list={semanticRelations}
        setConfirmToDelete={setConfirmToDelete}
        showDeleteIcon={showDeleteIcon}
      />
    )
  }
}


// change the shape of the data and remove unwanted data 
export function formatSemanticRelations(semanticRelations, lexicalSenseId) {

  let tmp = semanticRelations?.edges?.map(({node}) => node);
  if (tmp?.length > 0) {
    let tmp2 = tmp.map((node) => {
      // to display link to definition we need to parse lexicalSenses and remove node where id === lexicalSenseId
      let def = node?.lexicalSenses?.edges.map(
        ({node}) => {return {defId: node?.id, writtenRep: node?.canonicalFormWrittenRep}}
      ).filter(({defId, writtenRep}) =>
        writtenRep && (!defId.includes(lexicalSenseId))
      );
      let {writtenRep, defId} = def?.[0] || {writtenRep: null, defId: null};
      if (def?.length > 1) {console.log("this have multiple lexicalSenses ", def)}

      return {
        id: node?.id,
        canUpdate: node?.canUpdate,
        prefLabel: node?.semanticRelationTypeLabel || node?.semanticRelationType?.prefLabel,
        writtenRep,
        defId,
        enableDelete: true
      }
    });
    return unique(tmp2, 'prefLabel');
  } else {
    return []
  }
}