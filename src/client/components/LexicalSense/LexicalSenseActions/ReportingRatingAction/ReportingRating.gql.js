import { gql } from '@apollo/client';

export const gqlReportingRatingFragment = gql`
  fragment LexicalSenseReportingRatingFragment on LexicalSense {
    reportingRatingsCount
    reportingRating {
      id
    }
  }
`;

export const gqlAddReportingRatingMutation = gql`
  mutation CreateReportingRating($lexicalSenseId: ID!, $message: String, $reportingRatingType: ReportingRatingType!) {
    createReportingRating(input: { lexicalSenseId: $lexicalSenseId, message: $message, reportingRatingType: $reportingRatingType }) {
      createdObject {
        id
      }
    }
  }
`;

export const gqlRemoveReportingRatingMutation = gql`
  mutation RemoveEntity($reportingRatingId: ID!) {
    removeEntity(input: { objectId: $reportingRatingId }) {
      deletedId
    }
  }
`;
