/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import {useSnackbar} from 'notistack';
import {useTranslation} from 'react-i18next';
import {gql, useMutation} from "@apollo/client";
import {ActionButton} from '../../widgets/ActionButton';


let gqlUpdateLexicalSenseProcessed = gql`
  mutation UpdateLexicalSenseProcessed($lexicalSenseId: ID!, $writtenRep: String!, $processed: Boolean!) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,
      writtenRep: $writtenRep,
      objectInput:{ processed: $processed }  
    }) {
      updatedObject {
        id
        processed
        reviewed
      }
    }
  }
`;


export function ProcessAction({lexicalSense, onActionError} = {}) {
  let {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();

  let [updateProcessedMutation, {loading}] = useMutation(gqlUpdateLexicalSenseProcessed, {
    onError: onActionError,
    onCompleted: () => {     
      enqueueSnackbar(t("LEXICAL_SENSE_DETAIL.ACTIONS.LEXICAL_SENSE_PROCESSED"), {variant: "success"});
    }
  });

  return (!lexicalSense.processed ?
    <ActionButton
      text={t('LEXICAL_SENSE_DETAIL.ACTIONS.PROCESS')}
      iconName={"checkmark"}
      color={"green"}
      onClick={() => updateProcessed(true)}
      loading={loading}
      unactive={true}
    />
    :
    <ActionButton
      text={t('LEXICAL_SENSE_DETAIL.ACTIONS.UNPROCESS')}
      iconName={"checkmark"}
      color={"green"}
      onClick={() => updateProcessed(false)}
      loading={loading}
      unactive={false}
    />
  );

  async function updateProcessed(processedValue) {
    if (loading) return;

    await updateProcessedMutation({
      variables: {
        lexicalSenseId: lexicalSense.id,
        writtenRep: lexicalSense.canonicalFormWrittenRep,
        processed: processedValue
      },
      optimisticResponse: {
        __typename: 'Mutation',
        updateLexicalSense: {
          __typename: 'UpdateLexicalSensePayload',
          updatedObject: {
            __typename: 'LexicalSense',
            id: lexicalSense.id,
            reviewed: lexicalSense.reviewed,
            processed: processedValue
          }
        }
      }
    });
  }
}
