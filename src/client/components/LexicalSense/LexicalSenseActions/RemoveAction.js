/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useMutation, gql} from "@apollo/client";
import {makeStyles} from '@material-ui/core/styles';
import {useSnackbar} from 'notistack';
import {ActionButton} from '../../widgets/ActionButton';
import Config from '../../../Config';
import ConfirmDialog from '../../widgets/ConfirmDialog';



export const gqlRemoveLexicalSenseMutation = gql`
  mutation RemoveLexicalSense($lexicalSenseId: ID!) {
    removeEntity( input: { objectId: $lexicalSenseId }) {
      deletedId
    }
  }
`;



const useStyles = makeStyles((theme) => ({
  colorPurple: {
    color: Config.colors.darkpurple
  }
}));



export function RemoveAction({lexicalSense, actionButtonShowText, onActionError, onRemoveSuccess, reloadRef} = {}) {
  let {t} = useTranslation();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const {enqueueSnackbar} = useSnackbar();

  const handleClose = () => {
    setOpen(false);
  };


  let [removeLexicalSense, {loading}] = useMutation(gqlRemoveLexicalSenseMutation, {
    onError: onActionError,
    onCompleted: () => {
      if (onRemoveSuccess) {
        onRemoveSuccess();
      }
      enqueueSnackbar(t("LEXICAL_SENSE_DETAIL.ACTIONS.LEXICAL_SENSE_REMOVE_SUCCESS"), {variant: "success"});
    }
  });

  return (
    <>
      <ActionButton
        text={t('LEXICAL_SENSE_DETAIL.ACTIONS.REMOVE')}
        iconName={"remove"}
        color={"black"}
        onClick={() => {setOpen(true)}}
        showText={actionButtonShowText}
        loading={loading}
        unactive={false}
      />
      <ConfirmDialog isOpen={open}
        onClose={handleClose}
        confirmText={"SUPPRIMER"}
        onConfirm={handleRemove}
      >
        Vous êtes sur le point de supprimer l'entrée <span className={classes.colorPurple}>{lexicalSense?.canonicalForm?.writtenRep}</span><br />
        Êtes-vous sûr ?
      </ConfirmDialog>

    </>
  );

  function handleRemove() {

    handleClose();
    if (!loading) {
      removeLexicalSense({
        variables: {lexicalSenseId: lexicalSense.id},
        update: (cache) => {
          try {
            if (reloadRef?.current?.reload) {
              reloadRef?.current?.reload();
            }
          } catch (error) {
            console.log(error)
          }
        }
      });
    }
  }

}
