import React from 'react';
import clsx from "clsx";
import lexicalSenseLoaderStyle from './LexicalSenseLoaderStyle.js';
import globalStyles from '../../assets/stylesheets/globalStyles.js';

export function LexicalSenseLoader(props) {
  const classes = lexicalSenseLoaderStyle();
  const gS = globalStyles();
  return (
    <div className={clsx(classes.placeholder, gS.loadingOverlay)}>
      <div className={classes.countriesRow}>
        <div className={classes.countries}>
          &nbsp;
        </div>
      </div>

      <div className={classes.fakeTextLine}>
        &nbsp;
        <div className={classes.textPlaceholder} />
      </div>

      <div className={classes.fakeTextLine}>
        &nbsp;
        <div className={classes.textPlaceholder} />
      </div>

    </div>
  );
}
