import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import {gql} from '@apollo/client';
import {TableView} from '../widgets/Table';

import {useTranslation} from 'react-i18next';
import dayjs from 'dayjs';


LexicalSenseHistoryTable.propTypes = {
  lexicalSenseId: PropTypes.string.isRequired,
};


/**
 * display a table with informations about LexicalSense history
 * like when/who etc add it and status
 * @param {*} props
 */
export function LexicalSenseHistoryTable({lexicalSenseId}) {
  const {t} = useTranslation();
  const reloadRef = useRef();

  let LexicalSenseContribQuery = gql`
    query LexicalSenseContribQuery($first: Int, $after: String, $lexicalSenseId: ID!, $filters: [String],  $sortings: [SortingInput]) {
    lexicalSense(id: $lexicalSenseId ) {
      actionsCount(filters: $filters)
      actions(
        filters: $filters  
        first: $first
        after: $after
        sortings: $sortings
      ) {
        edges {
          node {
            id
            startedAtTime
            person {
              fullName
            }
            entitySnapshot(entityId: $lexicalSenseId) {
              ... on LexicalSense {
                definition
              }
            }
          }
        }
      }
    }
  }
  `;


  return <TableView
    pageSize={10}
    showCheckbox={false}
    columns={createColumns()}
    gqlQuery={LexicalSenseContribQuery}
    gqlCountPath="lexicalSense.actionsCount"
    gqlConnectionPath="lexicalSense.actions"
    ariaDescribedby="Tableau avec l'historique des modifications sur un sens"
    filters={{filters: [`hasEntity:${lexicalSenseId}`], lexicalSenseId}}
    ref={reloadRef}
    colIndexForLoader={1}
  />

  function createColumns() {
    return [{
      ariaLabel: 'Trier le tableau par date',
      label: 'Date',
      sortFor: 'startedAtTime',
      customRender: (node) => {
        return dayjs(node.startedAtTime).format('L')
      }
    }, {
      label: "Personne",
      customRender: (node) => {
        return node?.person?.fullName
      }
    }, {
      label: "Nature de l'ajout",
      customRender: (node) => {
        return t(`LEXICAL_SENSE_HISTORY.TYPENAME.${node?.__typename.toUpperCase()}`) || node?.__typename
      }
    }, {
      label: "Valeur ou contenu du champ",
      customRender: (node) => {
        return node?.entitySnapshot?.definition
      }
    }
    ]
  }
}
