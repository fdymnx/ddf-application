import React from 'react';

import { useTranslation } from 'react-i18next';
import { useQuery } from '@apollo/client';

import { FormTopPostsQuery } from '../Form/FormTopPosts';
import { apolloErrorHandler } from '../../utilities/apolloErrorHandler';
import { MobileDiscussionPreview } from '../Form/MobileDiscussionPreview';

/**
 * Usage :
 *
 * <LexicalSenseMobileEthymology formQuery="currentFormQuery" />
 *
 */
export function LexicalSenseMobileEthymology(props) {
  const { formQuery } = props;
  const { t } = useTranslation();
  const { data, loading, error } = useQuery(FormTopPostsQuery, {
    variables: {
      formQs: formQuery,
    },
  });
  apolloErrorHandler(error, 'log');
  if ((loading && !data) || error) return null;

  const { topPostAboutEtymology } = data;

  if (!data?.topPostAboutEtymology) {
    return null;
  }

  return (
    <MobileDiscussionPreview
      title={t('FORM.TOP_POSTS.DESKTOP.ETYMOLOGY_HEADER')+" :"}
      content={topPostAboutEtymology.content}
      commentCount={12}
    />
  );
}
