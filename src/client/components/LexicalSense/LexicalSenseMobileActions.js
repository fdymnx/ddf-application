import React from 'react';

import {makeStyles} from '@material-ui/core/styles';
import {useQuery, gql} from '@apollo/client';
import globalStyles from '../../assets/stylesheets/globalStyles.js';
import {gqlLexicalSenseActionsFragment, LexicalSenseActions} from '../LexicalSense/LexicalSenseActions';
import {ValidOrNotCount} from "../LexicalSense/LexicalSenseDesktopSideColumn";

export const LexicalSenseQueryActions = gql`
  query LexicalSenseActions_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      validationRatingsCount
      ...LexicalSenseActionsFragment
    }
  }
  ${gqlLexicalSenseActionsFragment}

`;


export function LexicalSenseMobileActions({formQuery, lexicalSenseId, onRemoveSuccess, notistackService}) {
  const classes = useStyles();

  const gS = globalStyles();
  const {data, loading} = useQuery(LexicalSenseQueryActions, {
    variables: {
      lexicalSenseId
    },
  });

  if (loading || !data?.lexicalSense) {
    return null;
  }

  const likeCount = data?.lexicalSense?.validationRatingsCount;

  return (
    <div>
      <ValidOrNotCount likeCount={likeCount} />
      <div className={gS.marginBottom} />
      <div className={classes.minWidth}>
        <LexicalSenseActions
          lexicalSense={data?.lexicalSense}
          formQuery={formQuery}
          onRemoveSuccess={onRemoveSuccess}
          notistackService={notistackService}
        />
      </div>
    </div>
  )
}


const useStyles = makeStyles((theme) => ({
  minWidth: {
    minWidth: "75vw"
  }
}));
