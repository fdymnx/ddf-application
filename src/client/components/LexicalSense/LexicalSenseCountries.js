import React from 'react';
import PropTypes from 'prop-types';

import {useHistory, Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {useTranslation} from 'react-i18next';
import {makeStyles} from "@material-ui/core/styles";
import Config from '../../Config';
import {ROUTES} from '../../routes';
import {SimpleModalPage} from '../../layouts/mobile/SimpleModalPage';
import {colorToCssClassName, getLocalizedTreePlaces} from '../../utilities/helpers/countries';
import {ShowWhereIAM} from '../../components/widgets/ShowWhereIAM';


const useStyles = makeStyles((theme) => ({ 
  container: {
    padding: "10px 20px",
    marginBottom: "20px",
    backgroundColor: Config.colors.white,
  },
  country: {
    display: "inline-block",
    minWidth: "125px",
    padding: Config.spacings.tiny,
    margin: Config.spacings.tiny,
    marginTop: "15px",
    color: Config.colors.white,
    "&:first-child": {
      backgroundColor: Config.colors.black, 
      marginLeft: 0
    }
  },
  state: {
    padding: Config.spacings.tiny,
    margin: Config.spacings.tiny,
    "&:first-child": {
        marginLeft: "5px",
    }  
  },
  cities: {
    display: "flex",
    flexWrap: "wrap",
    color: Config.colors.black,
    marginLeft: "15px"
  },
  city: {
    padding: Config.spacings.tiny,
    color: Config.colors.black,
    "&:not(:last-child)": {
      "&::after": {
        content: "', '"
      }
    },
    "&:first-child": {
      marginLeft: "3px"
    }
  },
  desktopCaption: {
    display: "none",
    marginBottom: Config.spacings.small,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      display: "block"
    }
  }
}));



LexicalSenseCountries.propTypes = {
  places: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      color: PropTypes.string,
    })
  ),
};

export function LexicalSenseCountries({places}) {
  const classes = useStyles();
  const {t} = useTranslation();
  let tPlaces = getLocalizedTreePlaces(places);

  if (!places?.length) {
    return <div>{t('GEOGRAPHICAL_DETAILS.NO_RESULTS')}</div>;
  } else {
    return (
      <ShowWhereIAM path="client/components/LexicalSense/LexicalSenseCountries.js"  >
        <div className={classes.container}>
          <div className={classes.desktopCaption}>{t('GEOGRAPHICAL_DETAILS.PAGE_CAPTION')}</div>
          <div>
            {Object.keys(tPlaces).sort().map(function (country, pindex) {
              // iterate over countries and display each one
              let {id, countryId, color, states} = tPlaces[country];

              return (
                <div key={'country' + pindex}>
                  <If condition={country != 'null'}>
                    <Link to={formatRoute(ROUTES.EXPLORE_LOCALISATION_FORM, {id: countryId || id})}
                      role="link" aria-label={country}
                      className={classes.country}
                      style={{
                        backgroundColor: Config.colors?.[colorToCssClassName(color)] || Config.colors.gray
                      }}>
                      {country}
                    </Link>
                  </If>


                  {Object.keys(states).sort().map(function (state, sindex) {
                    // iterate over states and display each one
                    let {cities, color, id, __typename} = states[state];
                    const style = {color: Config.colors?.[colorToCssClassName(color)] || Config.colors.gray};
                    return (
                      <div key={'state' + sindex}>
                        <If condition={state != 'null'}>

                          {__typename !== "State" ?
                            <div className={classes.state} style={style}>{state}</div>
                            :
                            <Link to={formatRoute(ROUTES.EXPLORE_LOCALISATION_FORM, {id})} role="link" aria-label={state} >                              
                              <div className={classes.state} style={style}>{state}</div>
                            </Link>
                          }
                        </If>
                        <div className={classes.cities}>
                          {Object.keys(cities).sort().map(function (city, cindex) {
                            // iterate over cities and display each one
                            let {name, id, __typename} = cities[city];

                            if (__typename === "City") {
                              return (
                                <Link to={formatRoute(ROUTES.EXPLORE_LOCALISATION_FORM, {id})}
                                  role="link" aria-label={city}
                                  key={'city' + cindex} className={classes.city}>
                                  {name}
                                </Link>
                              );
                            } else {
                              return <div key={'city' + cindex} className={classes.city}>
                                {name}
                              </div>
                            }
                          })}
                        </div>
                      </div>
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
      </ShowWhereIAM>
    );
  }
}

LexicalSenseCountriesMobilePage.propTypes = {
  lexicalSenseId: PropTypes.string.isRequired,
  formQuery: PropTypes.string.isRequired,
};

export function LexicalSenseCountriesMobilePage({lexicalSenseId, formQuery, places}) {
  const {t} = useTranslation();
  let history = useHistory();

  function closeButtonHandler() {
    let backPath = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
      lexicalSenseId,
      formQuery,
    });
    history.replace({
      pathname: backPath,
    });
  }

  return (
    <SimpleModalPage
      title={t('GEOGRAPHICAL_DETAILS.TITLE')}
      content={<LexicalSenseCountries places={places} />}
      controls={['close']}
      onClose={closeButtonHandler}

    />
  );
}
