import React from 'react';
import PropTypes from 'prop-types';
import {PurifyHtml} from '../../utilities/PurifyHtml';
import {useTranslation} from 'react-i18next';


import Config from '../../Config';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  usageExample: {
    fontSize: Config.fontSizes.small,
    color: Config.colors.darkgray,
    marginTop: Config.spacings.small
  },
  label: {
    fontWeight: Config.fontWeights.bold
  },
  text: {
    fontStyle: "italic"
  }
}));


UsageExamples.propTypes = {
  usageExamples: PropTypes.arrayOf(
    PropTypes.shape({
      bibliographicalCitation: PropTypes.string,
      value: PropTypes.string,
    })
  ),
};
export function UsageExamples({usageExamples}) {
  const {t} = useTranslation();
  const classes = useStyles();

  if (!usageExamples) return null;
  let filteredExamples = usageExamples.filter((ex) => ex.value);

  return (
    <React.Fragment>
      {filteredExamples.map((usageExample, index) => (
        <div key={index} className={classes.usageExample} data-testid="example">
          <span className={classes.label}>{t('LEXICAL_SENSE_DETAIL.EXAMPLE_LABEL')}&nbsp;:&nbsp;</span>
          <span className={classes.text}>
            <PurifyHtml as="span" html={usageExample.value} />
            <If condition={usageExample.bibliographicalCitation}>
              {" - "}
              <PurifyHtml as="span" html={usageExample.bibliographicalCitation} />
            </If>
          </span>
        </div>
      ))}
    </React.Fragment>
  );
}