import React from "react";
import {MetaSEO} from "../../widgets/MetaSEO";
import {useTranslation} from 'react-i18next';
import unique from "lodash/uniq";

export function getLexicalSenseMetas({lexicalSense, t}){
  let definition = "";
  let extraLabels = {};
  let keywords = [];

  const grammaticalPropertyLabels = [...(lexicalSense.lexicalEntryGrammaticalPropertyLabels || []), ...(lexicalSense.semanticPropertyLabels || [])];

  if (grammaticalPropertyLabels?.length > 0){
    keywords = keywords.concat(grammaticalPropertyLabels);
    definition += `${unique(grammaticalPropertyLabels).join(", ")} - `
  }

  definition += lexicalSense.definition;

  if (lexicalSense.lexicographicResourceName){
    extraLabels[t("FORM.LEXICOGRAPHIC_RESOURCE")] = lexicalSense.lexicographicResourceName;
  }

  if(lexicalSense.places?.edges?.length > 0){
    const placeNames = lexicalSense.places?.edges.map(({node}) => node.name);
    extraLabels[t("FORM.PLACES", {count : lexicalSense.places.edges.length})] = placeNames.join(", ");
    keywords = keywords.concat(placeNames);
    definition = `(${placeNames.join(", ")}) ${definition}`
  } else {
    extraLabels[t("FORM.PLACES", {count : 0})] = t("GEOGRAPHICAL_DETAILS.NO_COUNTRY_LABEL");
  }

  keywords = unique(keywords);

  return {extraLabels, definition, keywords};
}

export function getLexicalSenseMetaSEO({url, formQuery, lexicalSense}){
  const {t} = useTranslation();

  if (!lexicalSense){
    return null;
  }

  const {definition, extraLabels, keywords} = getLexicalSenseMetas({lexicalSense, t});

  return (
    <MetaSEO
      url={url}
      title={t('DOCUMENT_TITLES.LEXICAL_SENSE', {formQuery})}
      description={`${formQuery} - ${definition}`}
      ogDescription={definition}
      name={formQuery}
      addScript={true}
      extraLabels={extraLabels}
      keywords={keywords}
    />
  )
}