import React, { useEffect } from 'react';
import Yasgui from '@triply/yasgui';
import { DesktopMainLayout } from '../../layouts/desktop/DesktopMainLayout';
import globalStyles from '../../assets/stylesheets/globalStyles.js';
import {MetaSEO} from "../widgets/MetaSEO";

export function Yasguisparql() {
  const gS = globalStyles();

  const _url = window.location.origin + '/api/sparql';
  useEffect(() => {
    const yasgui = new Yasgui(document.getElementById('yasgui'), {
      copyEndpointOnNewTab: true,
      method: 'POST',
      requestConfig: { endpoint: _url },
    });
    return () => {};
  }, []);

  return (
    <React.Fragment>
      <MetaSEO title="SPARQL" />
      <DesktopMainLayout useDefaultGrid={false}>
        <div className={gS.SMPcontent}>
          <div>Requête SPARQL</div>
          <br />
          <div id="yasgui" />
        </div>
      </DesktopMainLayout>
    </React.Fragment>
  );
}
