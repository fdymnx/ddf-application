import React from 'react';
import {useTranslation} from 'react-i18next';
import {useQuery, useMutation, gql} from "@apollo/client";
import {formatRoute} from 'react-router-named-routes';
import {useHistory, useRouteMatch, useParams} from 'react-router-dom';
import {ROUTES} from '../../routes';
import {uniq, merge} from 'lodash';
import queryString from 'query-string';
import {_FORM, _DEFINITION} from "./form/LexicalSenseEditorConst";
import {LexicalSenseEditor} from './form/LexicalSenseEditor';
import {mergeVariablesWithFormikValues} from "./CreateLexicalSense";
import {Template} from "./LinkEdit/Template";
import {getGrammaticalCategoryFromLexicalEntry} from '../../utilities/helpers/lexicalEntryPartOfSpeechList';
import {gqlLexicalSenseSemanticRelationTypeFragment} from '../../utilities/helpers/lexicalSenseSemanticRelationList';
import {decodeURIComponentIfNeeded} from '../../utilities/helpers/tools';
import LoadingGif from "../widgets/LoadingGif";
import {ShowWhereIAM} from '../widgets/ShowWhereIAM';

export function EditLexicalSense(props) {
  const {t} = useTranslation();
  const history = useHistory();
  const match = useRouteMatch();

  let params = useParams();
  /** Initializing state from query string params */
  // in some route match is a empty object, fallback with params then props
  let formWrittenRep = decodeURIComponentIfNeeded(match.params.formQuery || params.formQuery || props?.computedMatch?.params?.formQuery);
  let lexicalSenseId = decodeURIComponentIfNeeded(match.params.lexicalSenseId || params.lexicalSenseId || props?.computedMatch?.params?.lexicalSenseId);

  const {step} = queryString.parse(location.search);
  /* GraphQL query */
  const [updateLexicalSenseMutation, {error, loading: isSubmitting}] = useMutation(gqlUpdateLexicalSense);

  const {data: {lexicalSense} = {}, loading} = useQuery(gqlLexicalSenseQuery, {
    variables: {lexicalSenseId},
    fetchPolicy: 'cache-and-network'
  });



  if (loading) {
    return (
      <Template error={error} {...props} formWrittenRep={formWrittenRep}>
        <LoadingGif />
      </Template>
    )
  }
  const grammaticalCategoryInitial = getGrammaticalCategoryFromLexicalEntry(lexicalSense?.lexicalEntry);


  return (
    <ShowWhereIAM path="src/client/components/contribution/EditLexicalSense.js">
      <Template error={error} {...props} formWrittenRep={formWrittenRep}
        title={lexicalSense?.canUpdate ? 'CONTRIBUTION.EDIT_LEXICAL_SENSE_TITLE' : 'CONTRIBUTION.ENRICH_LEXICAL_SENSE_TITLE'}>
        <LexicalSenseEditor
          formWrittenRep={formWrittenRep}
          lexicalSenseId={lexicalSenseId}
          grammaticalCategory={grammaticalCategoryInitial}
          definition={lexicalSense?.definition}
          lexicalEntry={lexicalSense?.lexicalEntry}
          canUpdate={lexicalSense?.canUpdate}
          showStepPlace={false}
          canEditFormWrittenRep={false}
          onSubmit={handleSubmit}
          onCancel={navigateBack}
          isSubmitting={isSubmitting}
        />
      </Template>
    </ShowWhereIAM>
  )

  async function handleSubmit(formikValues, formikOptions) {
    const {definition, usageExample} = formikValues;

    let variables = {writtenRep: formWrittenRep, lexicalSenseId};

    if (!lexicalSense || definition !== lexicalSense.definition) {
      variables.definition = definition;
    }
    if (usageExample?.bibliographicalCitation || usageExample?.value) {
      variables.usageExample = usageExample;
    }

    let tmpObj = {
      formikValues,
      initialVariables: {...lexicalEntry, grammaticalCategory: grammaticalCategoryInitial},
    };

    if (step && step === _FORM && formikValues?.formWrittenRep !== formWrittenRep) {
      variables.writtenRep = formikValues?.formWrittenRep;
      tmpObj = {
        formikValues,
        initialVariables: {},
      }
    }

    const lexicalEntry = lexicalSense?.lexicalEntry || {};
    variables = merge(variables, mergeVariablesWithFormikValues(tmpObj));

    try {
      // Apply mutation only if a diff appears      
      if (Object.keys(variables).length > 1) {
        await updateLexicalSenseMutation({variables});

        if (step && step === _FORM && formikValues?.formWrittenRep !== formWrittenRep) {
          formWrittenRep = formikValues?.formWrittenRep;
        }
      }

      navigateBack();
    } catch (error) {
      console.log(error);
    } finally {
      formikOptions.setSubmitting(false);
    }
  }

  function navigateBack() {
    let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE, {
      formQuery: formWrittenRep,
      lexicalSenseId
    });
    history.push(senseUrl);
  }
}


export const gqlLexicalSenseFragment = gql`
  fragment LexicalSenseFragment on LexicalSense {
    definition
    canonicalFormWrittenRep
      
    lexicalEntry {
      id
      __typename
      ... on WordInterface {
        partOfSpeech {
          id
          prefLabel
        }
      }
      ... on InflectablePOS {
        number {
          id
          prefLabel
        }
        gender {
          id
          prefLabel
        }
      }
      ... on Verb {
        partOfSpeech {
          id
          prefLabel
        }
        transitivity {
          id
          prefLabel
        }
      }
      ... on VerbalInflection {
        partOfSpeech {
          id
          prefLabel
        }
        transitivity {
          id
          prefLabel
        }
        mood {
          id
          prefLabel
        }
        tense {
          id
          prefLabel
        }
        person {
          id
          prefLabel
        }
      }
      ... on MultiWordExpression {
        multiWordType {
          id
          prefLabel
        }
      }
      ... on Affix {
        termElement {
          id
          prefLabel
        }
      }
    }
  } 
`

export const gqlLexicalSenseQuery = gql`
  query LexicalSenseEdit_Query($lexicalSenseId: ID! , $includeCreator: Boolean = true ) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      semanticProperties {
        edges {
          node {
            id
            prefLabel           
          }
        }
      }
      lexicalEntryGrammaticalPropertyLabels
      ...LexicalSenseFragment
      ...LexicalSenseSemanticRelationTypeFragment
    }
  }
  ${gqlLexicalSenseFragment}
  ${gqlLexicalSenseSemanticRelationTypeFragment}
`;

const gqlUpdateLexicalSense = gql`
  mutation UpdateLexicalSense(
    $lexicalSenseId: ID!,  
    $grammaticalCategoryId: String,
    $definition: String,  
    $writtenRep: String,  
    $usageExample: UsageExampleInput, 
    $transitivityId: String,
    $moodId: String,
    $tenseId: String,
    $personId: String,
    $genderId: String ,
    $numberId: String,
    $placeId: String,
    $registerId:String,
    $connotationId:String
  ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId, 
        grammaticalCategoryId: $grammaticalCategoryId,
        definition: $definition,
        writtenRep: $writtenRep,
        usageExample: $usageExample,
        transitivityId: $transitivityId,
        moodId: $moodId,
        tenseId: $tenseId,
        personId: $personId,
        genderId: $genderId,
        numberId: $numberId,
        placeId: $placeId,
        registerId: $registerId,
        connotationId: $connotationId
      }
    ) {
      updatedObject {
        id
        ...LexicalSenseFragment
        definition
      }
    }
  }
  ${gqlLexicalSenseFragment}
`;
