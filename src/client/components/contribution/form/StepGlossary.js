



import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "glossary";

const help = ``

export default function StepGlossary({persistedData}) {
  return (
    <StepTemplate title='Je rajoute un lexique' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={false}
        name={fieldName}
        maxSuggestionsLength={150}
        placeHolder='Lexique'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepGlossary.propTypes = {
  values: PropTypes.shape({
    glossary: PropTypes.object
  })
};