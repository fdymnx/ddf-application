import React from 'react';
import PropTypes from 'prop-types';
import FormikInput from '../../widgets/forms/formik/FormikInput';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';

import StepTemplate from './StepTemplate';


const fieldName = "grammaticalCategory";


// schemeId will contain the grammaticalCategorySchemeId: "http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type"
export default function StepCategory({values, schemeId}) {

  function overloadData(data) {
    const addMe = {
      "id": schemeId.toLowerCase().includes("multiword") ? "ddfa:multiword-type/unspecifiedPhrase" : "ddfa:part-of-speech-type/unspecifiedPOS",
      "prefLabel": "Je ne sais pas",
      "definition": "Si vous n'êtes pas sûr",
      "scopeNote": null,
      "__typename": "GrammaticalProperty"
    }

    data = data.filter((item) => item.prefLabel !== addMe.prefLabel);
    data.push(addMe);
    return data;
  }

  return (
    <StepTemplate title='Je rajoute une catégorie grammaticale'>
      <>
        <FormikInput type="hidden" name="formWrittenRep" aria-label="Ne pas toucher ce champ, mesure anti robot" />
        <ConceptChooser
          name={fieldName}
          placeHolder='Catégorie grammaticale'
          schemeId={schemeId}
          overloadData={(data) => overloadData(data)}
        />
      </>
    </StepTemplate>
  );
}


StepCategory.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired,
    grammaticalCategory: PropTypes.object
  }),
  schemeId: PropTypes.string,
};