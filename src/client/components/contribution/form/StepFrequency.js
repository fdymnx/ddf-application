


import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "frequency";

const help = ``;

export default function StepFrequency({persistedData}) {
  return (
    <StepTemplate title='Je rajoute une fréquence' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={true}
        name={fieldName}
        placeHolder='Fréquence'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepFrequency.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired
  })
};