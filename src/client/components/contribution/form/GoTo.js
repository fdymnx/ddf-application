

import React, {useState} from 'react';

import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import {formatRoute} from 'react-router-named-routes';
import {ROUTES} from '../../../routes';
import Config from '../../../Config';
import {encodeURIComponentIfNeeded} from "../../../utilities/helpers/tools";
import {useMediaQueries} from '../../../layouts/MediaQueries';
import addYellowIcon from '../../../assets/images/add_yellow.svg';
import editYellowIcon from '../../../assets/images/contributions_yellow.svg';
import xIconGrey from '../../../assets/images/cross_grey.svg';
import DivButton from "../../widgets/DivButton";

import {
  _FORM, _DEFINITION, _PLACE, _EDIT_PLACES, _CATEGORY, _GENDERNUMBER, _TRANSITIVITY, _TENSEMOODPERSON,
  _EDIT_DOMAINS, _EDIT_TEMPORALITIES, _EDIT_REGISTERS, _EDIT_USAGEEXAMPLES, _USAGEEXAMPLE, _EDIT_FREQUENCIES, _EDIT_CONNOTATIONS,
  _EDIT_GRAMMATICAL_CONSTRAINTS, _EDIT_TEXTUALGENRES, _EDIT_SOCIOLECTS, _EDIT_GLOSSARIES,
  _EDIT_SEMANTIC_RELATIONS, _ADDNEWDEFINITION
} from "./LexicalSenseEditorConst"

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    padding: Config.spacings.medium,
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: Config.colors.yellow,
    minHeight: '225px'
  },
  title: {
    color: Config.colors.white,
    paddingBottom: Config.spacings.medium,
    textAlign: "center"
  },
  icon: {
    height: "25px",
    marginRight: Config.spacings.small
  },
  iconSmall: {
    height: "15px"
  },
  buttonMP: {
    padding: "8px 16px",
    margin: '8px',
  },
  button: {
    minWidth: '50px',
    backgroundColor: Config.colors.white,
    display: "flex",
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: `${Config.colors.purple} !important`
  }
}));

/**
 *  Show the differents step/parts that can be added to a form
 * 
 * @param {string} formQuery 
 * @param {string} lexicalSenseId
 * @param {bool} showAddOnly show only button to add data, not to edit 

 */
export function GoTo({formQuery, lexicalSenseId, showAddOnly = false}) {
  const classes = useStyles();
  const {isMobile, isSmallMobile} = useMediaQueries();
  const [showMarqueUsage, setShowMarqueUsage] = useState(false);


  const buttons = [
    ...(!showAddOnly ? [{label: 'la forme écrite', step: _FORM}] : []),
    ...(!showAddOnly ? [{label: 'la définition', step: _DEFINITION}] : []),
    //
    {label: 'les exemples', step: _EDIT_USAGEEXAMPLES},
    {label: "les lieux d’usage", step: _EDIT_PLACES},
    {label: "une marque d’usage", onClick: () => {setShowMarqueUsage(true)}},
    {label: "un lien avec d’autres définitions", step: _EDIT_SEMANTIC_RELATIONS},
    {label: 'ce mot à un lexique', step: _EDIT_GLOSSARIES},
    {label: 'une autre définition', step: _ADDNEWDEFINITION, icon: addYellowIcon},
  ]

  const marqueUsageButtons = [
    {label: "domaine", step: _EDIT_DOMAINS},
    {label: "temporalité", step: _EDIT_TEMPORALITIES},
    {label: "registre", step: _EDIT_REGISTERS},
    {label: "fréquence", step: _EDIT_FREQUENCIES},
    {label: "connotation", step: _EDIT_CONNOTATIONS},
    {label: "genre textuel", step: _EDIT_TEXTUALGENRES},
    {label: "argot", step: _EDIT_SOCIOLECTS},
    {label: "contrainte grammmaticale", step: _EDIT_GRAMMATICAL_CONSTRAINTS},
  ]


  return (
    <div className={classes.container}>
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={1}
      >
        <Grid item xs={1}></Grid>
        <Grid item xs={10}>
          <div className={classes.title}>
            Je souhaite ajouter ou modifier :
          </div>
        </Grid>
        <Grid item xs={1}>
          {showMarqueUsage &&
            <DivButton aria-label={"Retour"} onClick={() => setShowMarqueUsage(false)} >
              <img className={classes.iconSmall} src={xIconGrey} alt="Retour" />
            </DivButton>
          }
        </Grid>
      </Grid>

      <Grid
        container
        direction="row"
        justifyContent={isMobile ? "flex-start" : "center"}
        alignItems="center"
        spacing={1}
      >
        {renderButtons()}
      </Grid>

    </div>
  );

  function renderButtons() {
    let useBtns = showMarqueUsage ? marqueUsageButtons : buttons;
    return useBtns.map((btn, index) =>
      <Grid item key={index + btn?.label} xs={isSmallMobile ? 12 : null}>
        <IconButton {...btn} to={createLink(btn.step)} />
      </Grid>
    )
  }

  function createLink(step) {
    
    formQuery = encodeURIComponentIfNeeded(formQuery);
    lexicalSenseId = encodeURIComponentIfNeeded(lexicalSenseId);

    switch (step) {
      case _ADDNEWDEFINITION:
        return formatRoute(ROUTES.CREATE_LEXICAL_SENSE) + "?form=" + formQuery;
      case _USAGEEXAMPLE:
      case _PLACE:
      case _DEFINITION:
      case _CATEGORY:
      case _FORM:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {formQuery, lexicalSenseId}) + "?step=" + step;
      case _EDIT_USAGEEXAMPLES:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_USAGEEXAMPLES, {formQuery, lexicalSenseId});
      case _EDIT_PLACES:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_PLACES, {formQuery, lexicalSenseId});
      case _EDIT_DOMAINS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_DOMAINS, {formQuery, lexicalSenseId});
      case _EDIT_TEMPORALITIES:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_TEMPORALITIES, {formQuery, lexicalSenseId});
      case _EDIT_REGISTERS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_REGISTERS, {formQuery, lexicalSenseId});
      case _EDIT_FREQUENCIES:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_FREQUENCIES, {formQuery, lexicalSenseId});
      case _EDIT_GRAMMATICAL_CONSTRAINTS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_GRAMMATICAL_CONSTRAINTS, {formQuery, lexicalSenseId});
      case _EDIT_TEXTUALGENRES:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_TEXTUALGENRES, {formQuery, lexicalSenseId});
      case _EDIT_SOCIOLECTS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_SOCIOLECTS, {formQuery, lexicalSenseId});
      case _EDIT_CONNOTATIONS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_CONNOTATIONS, {formQuery, lexicalSenseId});
      case _EDIT_GLOSSARIES:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_GLOSSARIES, {formQuery, lexicalSenseId});
      case _EDIT_SEMANTIC_RELATIONS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_SEMANTIC_RELATIONS, {formQuery, lexicalSenseId});
      default:
        return false
    }
  }
};

export function IconButton({icon = editYellowIcon, disable = false, label, to = false, removeMarginPadding = false, onClick = false}) {
  const classes = useStyles();

  if (onClick) {
    return (
      <DivButton onClick={onClick} aria-label={label} classesName={clsx(classes.button, !removeMarginPadding && classes.buttonMP)} >
        <img className={classes.icon} src={icon} alt={`Ajouter ${label}`} /> {label}
      </DivButton>
    );
  } else {
    return (
      <Link to={to} disabled={disable} role="link" aria-label={label} className={clsx(classes.button, !removeMarginPadding && classes.buttonMP)}>
        <img className={classes.icon} src={icon} alt={`Ajouter ${label}`} /> {label}
      </Link>
    );
  }
}


