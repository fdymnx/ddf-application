import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';

import CloseIcon from '@material-ui/icons/Close';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';

import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import Config from '../../../Config.js';

const useStyles = makeStyles({
  flexspace: {
    display: "flex",
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  content: {
    backgroundColor: 'white',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    padding: Config.spacings.medium,
    boxShadow: 24,

    width: 'min(65vw,1100px)',
    maxHeight: '85vh',
    overflowY: 'auto'
  },
  closeButton: {
    display: "flex",
    justifyContent: "flex-end",
    paddingBottom: Config.spacings.small,
  }

});


/*
little template
*/
export default function StepTemplate({title, help = null, children}) {
  const classes = useStyles();
  const gS = globalStyles();

  const [helpModalOpen, setHelpModalOpen] = useState(false);


  return (
    <>
      {help &&
        <Modal
          open={helpModalOpen}
          onClose={() => setHelpModalOpen(false)}
          role="alertdialog"
          aria-modal="true"
          aria-labelledby="Rubrique d'aide pour ce champ"
        >

          <div className={classes.content}>
            <div className={classes.closeButton} >
              <IconButton aria-label="close" onClick={() => setHelpModalOpen(false)}>
                <CloseIcon />
              </IconButton>
            </div>
            {help}
          </div>
        </Modal>
      }
      <div className={classes.flexspace}>
        {title &&
          <div className={gS.stepTemplateTitle}>
            {title}
          </div>
        }
        {help &&
          <IconButton onClick={() => setHelpModalOpen(true)} aria-label={"ouvrir la popup d'aide pour ce champ à remplir"}>
            <HelpOutlineIcon />
          </IconButton>
        }
      </div>
      <div className={gS.stepTemplateSpacer} />
      {children}
      <div className={gS.stepTemplateSpacer} />
    </>
  );
}

StepTemplate.propTypes = {
  title: PropTypes.string
};
