


import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "sociolect";

const help = ``

export default function StepSociolect({persistedData}) {
  return (
    <StepTemplate title='Je rajoute un argot' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={false}
        name={fieldName}
        placeHolder='Argot'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepSociolect.propTypes = {
  values: PropTypes.shape({
    sociolect: PropTypes.object
  })
};