import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

 

const help = ``

export default function StepTextualGenre({persistedData}) {
  return (
    <StepTemplate title='Je rajoute un genre textuel' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={true}
        name={"textualGenre"}
        placeHolder='Genre textuel'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + "textual-genre"}
      />
    </StepTemplate>
  );
}


StepTextualGenre.propTypes = {
  values: PropTypes.shape({    
    textualGenre: PropTypes.object
  })
};