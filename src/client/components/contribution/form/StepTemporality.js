


import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "temporality";

const help = ``

export default function StepTemporality({persistedData}) {
  return (
    <StepTemplate title='Je rajoute une temporalité' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={false}
        name={fieldName}
        placeHolder='Temporalité'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepTemporality.propTypes = {
  values: PropTypes.shape({
    temporality: PropTypes.object
  })
};