import React from 'react';
import PropTypes from 'prop-types';
import {useFormikContext} from "formik";
import {GeolocPicker} from '../../widgets/GeolocPicker';
import StepTemplate from './StepTemplate';


const fieldName = "place"
const help = `L'espace francophone est vaste et divers. Alors que certains pays entiers parlent le français, certaines zones de la francophonie sont limitées à une communauté isolée au sein d'une communauté multiculturelle. Le français n'est pas utilisé partout dans les mêmes contextes et pour désigner les mêmes choses et les communautés linguistiques ont pu faire des choix lexicaux variés. Certains mots ne sont utilisés que dans certaines zones géographiques (« cégépien » par exemple est utilisé au Québec, mais pas en Belgique) ou ont une définition propre à cette zone. Le mot « sucre », par exemple, est le nom courant du saccharose un peu partout dans l'espace francophone, mais aussi le nom courant de la sève d'érable surtout au Québec ainsi qu'un terme technique dans le domaine de l'électricité, synonyme de « domino » ou « serre-fils ».

Ces indications géographiques peuvent être de différentes natures et le but est de refléter la réalité de la langue de la manière la plus précise possible. Trois types d'indications principales seront proposées : le pays, la région et la commune. Par région, on entend également province, état, cantons et territoires. Ces trois niveaux géographiques présentent l'avantage de renvoyer à des informations accessibles à tous et de se situer à un niveau de généralité suffisamment précis et univoque.

Dans la description de la langue, un niveau de précision supplémentaire existe, celui de la communauté ou du groupe, mais cet échelon est délicat à décrire dans un dictionnaire et ne se base pas sur des éléments uniquement géographiques. Ces indications seront donc précisées avec d'autres étiquettes. `


/**
 * in this step we add the place
 *  
 */
export default function StepPlace({values}) {
  const formikContext = useFormikContext();

  return (
    <StepTemplate title="J'ajoute le lieu d'usage" help={help}>
      <GeolocPicker
        displayInColumn={true}
        onChange={handleOnChange}
        initialPlace={values?.[fieldName]}
      />
    </StepTemplate>
  );

  function handleOnChange(params) {
    formikContext.setFieldValue(fieldName, params);
  }
}


StepPlace.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired,
    place: PropTypes.object
  })
};