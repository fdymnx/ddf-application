


import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const fieldName = "domain";

const help = ``

export default function StepDomain({persistedData}) {
  return (
    <StepTemplate title='Je rajoute un domaine' help={help}>
      <ConceptChooser
        persistedData={persistedData}
        hideInput={false}
        name={fieldName}
        placeHolder='Domaine'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />
    </StepTemplate>
  );
}


StepDomain.propTypes = {
  values: PropTypes.shape({
    domain: PropTypes.object
  })
};