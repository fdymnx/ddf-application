


import React from 'react';
import PropTypes from 'prop-types';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';
import clsx from "clsx";


const tenseFieldName = "tense";
const moodFieldName = "mood";
const personFieldName = "person";

const help =
  `Le français est une langue très riche en matière de temps verbaux. Chaque temps verbal peut prendre plusieurs valeurs, selon le contexte d'usage ; nous en citons uniquement les principales :

    Le présent, qui a pour fonction première de placer l'action/le fait dans le moment de la parole. Il permet aussi d'énoncer une habitude, une vérité générale, une action passée depuis peu ou qui va bientôt avoir lieu. Ex. : Je mange des céréales tous les matins.

    Le passé simple, qui a pour fonction d'indiquer que l'action est passée et terminée. Il permet également d'énoncer des actions brèves ou soudaines dans le passé. En littérature, le passé simple est le temps du récit, il sert à dérouler l'histoire. Ex. : En entrant dans la maison, il enleva son manteau.

    L'imparfait a pour fonction première d'indiquer que l'action a commencé dans le passé, mais contrairement au passé simple, il met en avant le déroulement. Il permet également d'énoncer une habitude dans le passé ou une action en cours dans le passé. En littérature, l'imparfait est le temps de la description, il sert à dessiner le contexte de déroulement de l'histoire. Ex. : Il était marin avant de devenir pompier.

    Le futur, qui a pour fonction première de placer l'action dans l'avenir. Il permet donc de parler de projets lointains ou de donner des ordres de manière plus polie. Ex. : Un jour je serai le meilleur dresseur.

    Le passé composé, le plus-que-parfait et le futur antérieur sont des temps composés. Ils ne sont pas décrits avec des entrées dédiées dans le Dictionnaire des francophones. On pourra cependant trouver des entrées pour la forme utilisée pour le participe passé, qui est une forme adjectivale du verbe. Ex. : Quand tu arriveras, je serai déjà parti.

Ces temps sont toujours associés à un mode et il y en a cinq aussi :

    L'indicatif permet d'inscrire l'action dans le monde réel de la personne qui parle. Ex. : Je sortis de table à la fin du repas.
    Le subjonctif permet d'exprimer l'éventualité, l'incertitude, un fait pensé ou imaginé, donc irréel au moment de l'énonciation. Le subjonctif est souvent utilisé avec la conjonction de subordination que ou qu'. Ex. : Il ira à l'hôpital jusqu'à ce qu'il aille mieux.
    Le conditionnel permet d'exprimer un souhait, une hypothèse ou une condition. Ex. : J'irais bien à la piscine ce week-end.
    L'impératif permet de conseiller ou de donner un ordre. Ex. : N'oublie pas de ranger ta chambre !
    Le gérondif exprime la simultanéité d'un fait qui a lieu dans le cadre d'un autre fait et se construit avec la préposition en. Ex. : Prends ton sac en partant !`

export default function StepTenseMoodPerson() {

  const gS = globalStyles();

  return (
    <StepTemplate title='Je rajoute le temps ...' help={help}>

      <ConceptChooser
        hideInput={true}
        name={tenseFieldName}
        placeHolder='Genre'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + tenseFieldName}
        addIDontKnowConcept={true}
      />

      <div className={clsx(gS.stepTemplateSpacer, gS.stepTemplateTitle)}>... et le mode</div>

      <ConceptChooser
        hideInput={true}
        // initialValue={values?.[moodFieldName]}
        name={moodFieldName}
        placeHolder="Nombre"
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + moodFieldName}
        addIDontKnowConcept={true}
      />

      <div className={clsx(gS.stepTemplateSpacer, gS.stepTemplateTitle)}>... et la personne</div>

      <ConceptChooser
        hideInput={true}
        // initialValue={values?.[personFieldName]}
        name={personFieldName}
        placeHolder="Personne"
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + personFieldName}
        addIDontKnowConcept={true}
      />
    </StepTemplate>
  );
}


StepTenseMoodPerson.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired,
    definition: PropTypes.string,
    place: PropTypes.object,
    grammaticalCategory: PropTypes.object.isRequired,
    register: PropTypes.object,
    tense: PropTypes.object,
    mood: PropTypes.object,
    person: PropTypes.object,
  })
};