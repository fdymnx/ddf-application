


import React from 'react';
import PropTypes from 'prop-types';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';
import clsx from "clsx";


const genderFieldName = "gender";
const numberFieldName = "number";

const help = `Le genre grammatical est une caractéristique de chaque nom en français, qui peut être masculin, féminin ou épicène. Pour les êtres animés, la plupart du temps le genre grammatical reflète le genre ou le sexe. Par exemple, louve est féminin et désigne un animal de sexe féminin, et loup est masculin et désigne un animal de sexe masculin. Il existe quelques rares exceptions comme une sentinelle, mot féminin qui désigne aussi bien un homme qu'une femme ou un mannequin, nom masculin qui peut désigner une femme. Pour les noms d'objets, cette classification en masculin et féminin est bien plus arbitraire puisque les objets ne sont pas sexués.

Le genre grammatical a une influence sur la forme des autres classes de mots qui s'accordent avec le nom. Par exemple, l'adjectif grand s'écrit différemment dans « cet homme est grand » et « cette femme est grande ». De même, le nom lui-même varie en fonction du genre du référent. Par exemple, le nom masculin étudiant a pour équivalent féminin étudiante. Dans le Dictionnaire des francophones, ces deux noms forment des entrées distinctes, permettant de mieux les décrire et d'y associer des informations spécifiques telles que des exemples ou prononciations.

Certains mots sont épicènes, c'est-à-dire que leur forme ne varie pas selon le genre. Par exemple, les mots archéologue, svelte et les (l'article pluriel) s'écrivent toujours de cette manière, qu'ils désignent le genre masculin ou féminin.

Dans certains cas, le genre du nom n'est pas clairement identifié. Dans ce cas, on trouvera l'indication l'usage hésite. C'est notamment le cas du mot réglisse, puisqu'on peut dire le réglisse ou la réglisse.

Enfin, certains types de mots comme les verbes, les adverbes, les prépositions ou les conjonctions ne possèdent pas de genre, et ne s'accordent pas non plus. Aucune indication de genre n'est alors donnée.

La catégorie grammaticale du nombre comprend les valeurs principales de singulier, pluriel et ne varie pas en nombre. Un mot est au singulier quand il ne désigne qu'un référent, au pluriel quand il en désigne plusieurs.

Certains mots s'emploient uniquement au singulier, comme le vivant utilisé pour désigner l'ensemble des êtres vivants. De même, certains mots comme les lunettes (pour désigner l'objet qu'on porte pour mieux voir) s'emploient uniquement au pluriel.

Finalement, des mots sont également invariables quand ils ne changent pas ni en genre ni en nombre (adverbes, prépositions, conjonctions, etc.).`

export default function StepGenderNumber({values}) {

  const gS = globalStyles();

  function overloadData(data) {
    const addMe = {
      "id": "ddfa:gender/unspecifiedGender",     
      "prefLabel": "Je ne sais pas",
      "definition": "Si vous n'êtes pas sûr"      
    }

    data = data.filter((item) => item.id !== addMe.id);
    data.push(addMe);
    return data;
  }

  return (
    <StepTemplate title='Je rajoute le genre...' help={help}>

      <ConceptChooser
        hideInput={true}
        initialValue={values?.[genderFieldName]}
        name={genderFieldName}
        placeHolder='Genre'
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + genderFieldName}
        overloadData={(data) => overloadData(data)}
      />

      <div className={clsx(gS.stepTemplateSpacer, gS.stepTemplateTitle)}>... et le nombre</div>
      <ConceptChooser
        hideInput={true}
        initialValue={values?.[numberFieldName]}
        name={numberFieldName}
        placeHolder="Nombre"
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + numberFieldName}
        addIDontKnowConcept={true}
      />

    </StepTemplate>
  );
}


StepGenderNumber.propTypes = {
  values: PropTypes.shape({
    formWrittenRep: PropTypes.string.isRequired,
    definition: PropTypes.string,
    grammaticalCategory: PropTypes.object
  })
};