


import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';
import {useTranslation} from 'react-i18next';

const help = null;

const fieldName = "type";
// word 
export const distinctWrittenRep = {
  "id": 'distinctWrittenRep',
  "prefLabel": "Une relation avec une définition d'un autre mot",
  "definition": "synonyme, antonyme, hyponyme, variante graphique, etc."
};

// definition
export const sameWrittenRep = {
  "id": 'sameWrittenRep',
  "prefLabel": "Une relation avec une autre définition de ce mot",
  "definition": "par extension, au figuré, par hyponymie, etc."
};

const staticData = {
  "grammaticalProperties": {
    "edges": [{"node": distinctWrittenRep}, {"node": sameWrittenRep}],
    "__typename": "semanticRelationPropertyConnection"
  }
}

export function StepSenseRelationType({onChangeCallBack=false}) {
  const {t} = useTranslation();
  //title={t('CONTRIBUTION.ADD_SEMANTIC_RELATION')} 
  return (
    <StepTemplate help={help}>
      <ConceptChooser
        onSelectCallBack={onChangeCallBack}
        hideInput={true}
        name={fieldName}
        placeHolder='Relation entre mot ou définition ?'
        staticData={staticData}
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + fieldName}
      />

    </StepTemplate>
  );
}


StepSenseRelationType.propTypes = {
  values: PropTypes.shape({
    type: PropTypes.object
  })
};