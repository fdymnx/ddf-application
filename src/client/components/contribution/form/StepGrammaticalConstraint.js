import React from 'react';
import PropTypes from 'prop-types';
import {ConceptChooser} from '../../widgets/forms/ConceptChooser';
import StepTemplate from './StepTemplate';

const help = ``;

export default function StepGrammaticalConstraint({persistedData, placeHolder = 'Contrainte grammmaticale'}) {
  return (
    <StepTemplate title='Je rajoute une contrainte grammmaticale' help={help}>
      StepGrammaticalConstraint
      <ConceptChooser
        persistedData={persistedData}
        hideInput={false}
        name={"grammaticalConstraint"}
        placeHolder={placeHolder}
        schemeId={"http://data.dictionnairedesfrancophones.org/authority/" + "grammatical-constraint"}
      />
      end of StepGrammaticalConstraint
    </StepTemplate>
  );
}


StepGrammaticalConstraint.propTypes = {
  values: PropTypes.shape({
    grammaticalConstraint: PropTypes.object
  })
};