import React from 'react';
import PropTypes from 'prop-types';
import {TextField} from "../../widgets/FormV2";
import {isString} from 'lodash';
import StepTemplate from './StepTemplate';
import Config from '../../../Config';

const fieldName = "formWrittenRep";

/*
 in this step we add the form 

 textfield for formWrittenRep
*/
export default function StepForm({editMode, values}) {
  const title = editMode ? "Je change la forme du mot ou de l'expression" : "Je rajoute un nouveau mot ou expression";

  const ifFirstCharUpperCased = checkCase(values?.[fieldName]);

  return (
    <StepTemplate title={title} >
      {ifFirstCharUpperCased &&
        <>
          <span style={{color: Config.colors.darkpurple}}>Êtes-vous sûr que ce mot doit commencer par une majuscule ?</span><br />
          <i style={{color: Config.colors.darkgray}}>En français, seuls les noms propres et les noms de gentilés prennent une majuscule initiale.</i>
          <br /><br />
        </>
      }
      <TextField
        name={fieldName}
        required={true}
        autoFocus
        placeholder='Mot ou expression'
        aria-label="Saisir ici le mot ou l'expression"
      />
    </StepTemplate>
  );
}

StepForm.propTypes = {
  editMode: PropTypes.bool.isRequired
};

function isCharNumber(c) {
  return c >= '0' && c <= '9';
}
// check is string str have first letter in uppercase
function checkCase(str) {
  try {
    if (isString(str)) {
      str = str.trim();
      if (str?.length > 0) {
        const ch = str.charAt(0);
        if (isCharNumber(ch)) {
          return false;
        }
        if (ch === ch.toUpperCase()) {
          return true;
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
  return false;
}
