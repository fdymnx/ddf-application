import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {useFormikContext} from "formik";
import FormSearchInput from '../../widgets/FormSearchInput';
import {useMediaQueries} from '../../../layouts/MediaQueries';
import StepTemplate from './StepTemplate';

const help = ``;
const fieldName = "toFormWrittenRep";

export function StepSenseRelationSearchInput({onChangeCallBack=false}) {
  const {isMobile} = useMediaQueries();
  const {values, setFieldValue} = useFormikContext();
  const {t} = useTranslation();
  // title=' Je cherche le mot à lier'
   
  return (
    <StepTemplate help={help}>
      <FormSearchInput
        showIsRequired={!values?.[fieldName]}
        placeholder={t(isMobile ? 'INDEX.SEARCH_PLACEHOLDER_MOBILE' : 'INDEX.SEARCH_PLACEHOLDER')}
        value={values?.[fieldName]?.label || ""}
        onChange={handleOnChange}
        onSubmit={() => { }}
        theme={'light'}
        addForm={false}
      />
    </StepTemplate>
  );

  function handleOnChange(params) {
    setFieldValue(fieldName, params);
    onChangeCallBack({value:params?.id,name:fieldName});
  }
}

StepSenseRelationSearchInput.propTypes = {
  values: PropTypes.shape({
    toFormWrittenRep: PropTypes.object
  })
};