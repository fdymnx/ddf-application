import React, {useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {gql, useMutation} from "@apollo/client";
import {ContributionsListTable} from './ContributionsListTable';
import {Filters} from './Filters';
import {useSnackbar} from 'notistack';

ContributionsList.propTypes = {
  /** See prop hideColumns of ContributionsListTable */
  hideColumns: PropTypes.arrayOf(PropTypes.string),
  /** See prop pageSize of ContributionsListTable */
  pageSize: PropTypes.number,
  /** See prop filterOnLoggedUser of ContributionsListTable */
  filterOnLoggedUser: PropTypes.bool,
  /** Set this flag to enable the panel of filters */
  enableFiltering: PropTypes.bool,

};

/**
 *
 * @param {int} [pageSize] - Controls how many results are displayed
 * @param {boolean} filterOnLoggedUser
 * @return {*}
 * @constructor
 */

export function ContributionsList(props = {}) {

  const contribListTableReloadRef = useRef();

  const {enqueueSnackbar} = props?.notistackService || useSnackbar();
  let {pageSize, hideColumns, enableFiltering, filtersInitialValues} = props;
  const filterOnLoggedUser = !!props.filterOnLoggedUser; // force to be true or false

  let [updateProcessedMutation, {loading: savingAction}] = useMutation(gqlBatchUpdateLexicalSenseProcessed, {
    onCompleted: data => {
      enqueueSnackbar("Opération terminée", {variant: "success"});
      contribListTableReloadRef.current.reload();
    }
  });

  function newFilters() {
    return {
      filterOnLoggedUser,
      filterOnUserAccountId: null,
      filterOnReviewed: null,
      filterOnExistingSuppressionRating: null,
      filterOnProcessed: null,
    }
  }
  const [filters, setFilters] = useState(newFilters());
  const [selectedIDs, setSelectedIDs] = useState([]);


  function handleFilters(newfilters) {
    setFilters({
      filterOnLoggedUser, // force filterOnLoggedUser to have inital value to prevent another component to change it
      ...newfilters
    })
  }

  // mutation lancée lorsque l'utilisateur selectionne des elements et une action
  function handleOnSelectAction(action) {
    switch (action) {
      case "processed":
        updateProcessedMutation({variables: {"objectIds": selectedIDs, "processed": true}});
        break;
      case "unprocessed":
        updateProcessedMutation({variables: {"objectIds": selectedIDs, "processed": false, "reviewed": false}});
        break;
      default:
        console.warn("handleOnSelectAction unknown action name", action)
    }
  }
 
  return (
    <React.Fragment>
      <If condition={enableFiltering}>
        <Filters
          savingAction={savingAction}
          enableAction={selectedIDs?.length > 0}
          onFiltersUpdate={handleFilters}
          filtersInitialValues={filtersInitialValues}
          onResetFilters={() => setFilters(newFilters())}
          onSelectAction={handleOnSelectAction}
        />
      </If>
      <ContributionsListTable
        onSelectedIDsUpdate={setSelectedIDs}
        pageSize={pageSize}
        hideColumns={hideColumns}
        filters={filters}
        reloadRef={contribListTableReloadRef}
      />
    </React.Fragment>
  );

}



let gqlBatchUpdateLexicalSenseProcessed = gql`
  mutation BatchUpdateLexicalSenseProcessed($objectIds: [ID!], $processed: Boolean! ) {
    batchUpdateLexicalSense(
      input: { objectIds: $objectIds, objectsInput: { processed: $processed } }
    ) {
      updatedObjects {
        id
        processed
        reviewed
      }
    }
  }
`