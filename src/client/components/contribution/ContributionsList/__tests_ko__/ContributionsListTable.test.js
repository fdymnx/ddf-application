import React from 'react';
import {act} from 'react-dom/test-utils';
import waait from 'waait';

import {renderWithMocks} from '../../../../../../jest/utilities/renderWithMocks';
import {ContributionsListTable, gqlContributionsQuery} from '../ContributionsListTable';

describe('Empty results', () => {
  it('displays a warning message', async () => {
    let gqlMocks = [{
      request: {
        query: gqlContributionsQuery,
        variables: {
          "first": 20,
          "after": null,
          "filterOnLoggedUser": false,
          "sortings": [{
            "sortBy": "createdAt",
            "isSortDescending": true
          }]
        }
      },
      result: {
        "data": {
          "contributedLexicalSensesCount": 0,
          "contributedLexicalSenses": {
            "edges": [],
            "pageInfo": {
              "endCursor": "offset:1",
              "hasNextPage": false,
              "__typename": "PageInfo"
            },
            "__typename": "LexicalSenseConnection"
          }
        }
      }
    }];

    const {container} = renderWithMocks({
      gqlMocks: gqlMocks,
      element: <ContributionsListTable
        pageSize={20}
        filters={{filterOnLoggedUser: false}}
      />,
    });

    await act(waait);
    await act(waait);

    expect(container).toMatchSnapshot();
  });
});



