import React, {useContext} from 'react';
import {useTranslation} from 'react-i18next';
import {makeStyles} from '@material-ui/core/styles';
import {GraphQLErrorHandler} from '../../../services/GraphQLErrorHandler';
import {notificationService as appNotificationService} from '../../../services/NotificationService';
import {ValidationRatingAction} from '../../LexicalSense/LexicalSenseActions/ValidationRatingAction';
import {SuppressionRatingAction} from '../../LexicalSense/LexicalSenseActions/SuppressionRatingAction';
import {RemoveAction} from '../../LexicalSense/LexicalSenseActions/RemoveAction';
import {ProcessAction} from '../../LexicalSense/LexicalSenseActions/ProcessAction';
import {UserContext} from '../../../hooks/UserContext';
import Config from '../../../Config';

export const useStyles = makeStyles((theme) => ({
  flex: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: Config.spacings.small
  }
}));

export function Actions(props) {
  const classes = useStyles();
  let {t} = useTranslation();
  const {lexicalSense, onRemoveSuccess, reloadRef} = props;
  let notificationService = props.notificationService || appNotificationService;
  const {user} = useContext(UserContext);

  /**
   * Safeguard, actions should be displayed only for the admin panel
   */
  if (!(user?.userAccount?.isOperator || user?.userAccount?.isAdmin)) {
    return null;
  }

  return (
    <div className={classes.flex}>

      <If condition={user?.userAccount?.isOperator && !user?.userAccount?.isAdmin}>
        <ValidationRatingAction
          lexicalSense={lexicalSense}
          onActionError={onActionError}
          enableRefetchQuery
          actionButtonShowText={false}
        />

        <SuppressionRatingAction
          lexicalSense={lexicalSense}
          onActionError={onActionError}
          enableRefetchQuery
          actionButtonShowText={false}
        />
      </If>

      <If condition={user?.userAccount?.isAdmin}>
        <ProcessAction
          lexicalSense={lexicalSense}
          actionButtonShowText={false}
          onActionError={onActionError}
        />
        <RemoveAction
          lexicalSense={lexicalSense}
          actionButtonShowText={false}
          onActionError={onActionError}
          onRemoveSuccess={onRemoveSuccess}
          reloadRef={reloadRef}
        />
      </If>
    </div>
  );

  async function onActionError(error) {
    let {globalErrorMessage} = GraphQLErrorHandler(error, {t});
    await notificationService.error(globalErrorMessage);
  }
}
