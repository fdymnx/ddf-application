import React, {useEffect, useRef, useState} from 'react';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Config from '../../../Config';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DivButton from '../../widgets/DivButton';
import {PersonAutocompleteMUIA} from '../../widgets/PersonAutocompleteMUIA';
import ConfirmDialog from '../../widgets/ConfirmDialog';
import LoadingGif from "../../widgets/LoadingGif";
import CircularProgress from "@material-ui/core/CircularProgress";


Filters.propTypes = {
  onFiltersUpdate: PropTypes.func.isRequired,
};

export const useStyles = makeStyles((theme) => ({
  flexCol: {
    display: "flex",
    flexDirection: "column"
  },
  container: {
    margin: `${Config.spacings.small}px 0px`,
    backgroundColor: Config.colors.white,
    fontSize: Config.fontSizes.medium
  },
  rightBorder: {
    borderRight: `2px solid ${Config.colors.lightgray}`
  },
  mediumPadding: {
    padding: Config.spacings.large
  },
  purpleText: {
    color: Config.colors.purple
  },
  underline: {
    textDecoration: "underline",
    cursor: "pointer",
    fontSize: Config.fontSizes.small
  },
  formControl: {
    margin: theme.spacing(1),
    marginLeft: 0,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  borderInput: {
    color: "black",
    padding: '10.5px 14px',
    border: `1px solid rgba(0, 0, 0, 0.3)`,
    borderRadius: "5px"
  }
 
}));

const actionsMapping = [
  {code: "processed", label: "Marquer la sélection comme traité"},
  {code: "unprocessed", label: "Marquer la sélection comme NON traité"},
  // {code: "validAll", label: "Tout marquer comme valide"}
]

/***
 * RULES
 *
 * définitions relues
 * oui   : filterOnReviewed = true
 * non   : filterOnReviewed = false
 * 2     : filterOnReviewed = null
 *
 *
 * définitions en demande de suppression :
 * oui : filterOnExistingSuppressionRating = true
 * non : filterOnExistingSuppressionRating = false

 * définitions relues et validées :
 * oui : filterOnReviewed =true  + filterOnProcessed =true 
 * 
 *
 * définition non traitées
 * oui :  filterOnProcessed:false
 * non (donc les traitées ) filterOnProcessed: true
 */

/**
 * 
 * @param {function} onFiltersUpdate    update filters in parent component
 * @param {function} onSelectAction     when a grouped action is selected, dispatch to parent
 * @param {bool} savingAction           action is in progress, show loader
 * @param {bool} enableAction           if not id selected, action is disabled 
 * @param {object} filtersInitialValues inital values for filters
 * @param {function} onResetFilters     reset filters in parent component with initial state
 *
 */
export function Filters({onFiltersUpdate, savingAction = false, enableAction = false, onSelectAction, filtersInitialValues, onResetFilters}) {
  const classes = useStyles();
  const containerRef = useRef(null);
  // input value for person name only
  const [personInputValue, setPersonInputValue] = useState('');

  const [filterOnUserAccount, setFilterOnUserAccount] = useState({});
  const [filterOnReviewed, setFilterOnReviewed] = useState(null);
  const [filterOnExistingSuppressionRating, setFilterOnExistingSuppressionRating] = useState(null);
  const [filterOnProcessed, setFilterOnProcessed] = useState(filtersInitialValues?.processedStatus === undefined ? null : filtersInitialValues.processedStatus);

  /**
   * il faut mettre "" comme valeur null au select sinon erreur input non controllé, 
   * on ne peux pas utiliser les useState du dessus directement. 
   */
  // pour les actions uniquement,
  const [selectedAction, setSelectedAction] = useState("")
  // pour les actions uniquement
  const [selectedFilter, setSelectedFilter] = useState(filtersInitialValues?.processedStatus === undefined ? "" : filtersInitialValues.processedStatus)
  // pour le select sur traitement
  const [selectedListTwoCode, setSelectedListTwoCode] = useState("");

  // open Confirm Dialog
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);

  useEffect(() => {
    onFiltersUpdate({
      filterOnUserAccountId: filterOnUserAccount?.id || null,
      filterOnReviewed,
      filterOnExistingSuppressionRating,
      filterOnProcessed
    });
  }, [filterOnUserAccount, filterOnReviewed, filterOnExistingSuppressionRating, filterOnProcessed]);


  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="stretch"
      className={classes.container}
    >
      <Grid item xs={12} md={3} className={clsx(classes.mediumPadding, classes.rightBorder, classes.purpleText)}>
        Actions groupées
        {renderSelectAction()}
        <ConfirmDialog
          isOpen={openConfirmDialog}
          onClose={() => {
            setSelectedAction("");
            setOpenConfirmDialog(false)
          }}
          confirmText={"VALIDER"}
          onConfirm={() => {
            setOpenConfirmDialog(false);
            setSelectedAction("");
            onSelectAction(selectedAction)
          }}
        >
          {actionsMapping.find(action => action.code === selectedAction)?.label}.<br />
          Êtes-vous sûr ?
        </ConfirmDialog>

      </Grid>
      <Grid item xs={12} md={9} className={classes.mediumPadding}>

        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Grid item zeroMinWidth className={classes.purpleText}>
            Filtres
          </Grid>
          <Grid item zeroMinWidth className={clsx(classes.purpleText, classes.underline)}>
            <DivButton
              aria-label="Supprimer tous les filtres"
              onClick={() => {
                setPersonInputValue("")
                onResetFilters()
              }} >
              Supprimer tous les filtres
            </DivButton>
          </Grid>
        </Grid>
        <Grid container direction="row" justifyContent="space-between" alignItems="stretch" >
          <Grid item xs={12} md={4} className={classes.flexCol}>
            {renderSelectListOne()}
          </Grid>
          <Grid item xs={12} md={4} className={classes.flexCol}>
            {renderSelectListTwo()}
          </Grid>
          <Grid item xs={12} md={4} className={classes.flexCol} >
            {renderSelectAuthor()}
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );



  function renderSelectAction() {
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          inputProps={{'aria-label': 'Choisir une action groupée'}}
          value={selectedAction}
          displayEmpty
          onChange={(event) => {
            setSelectedAction(event.target.value);
            setOpenConfirmDialog(event.target.value !== "");
          }}
        >
          <MenuItem value={""}>
            {savingAction &&
              <LoadingGif small={true}   />
            }
            {enableAction ?
              <em>Choisir une action</em>
              :
              <em>Sélectionner déf avant</em>
            }
          </MenuItem>
          {enableAction && actionsMapping.map(({code, label}) =>
            <MenuItem key={code} value={code}>{label}</MenuItem>
          )}
        </Select>
      </FormControl>
    )
  }

  function renderSelectListOne() {
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          data-testid='selectListOne'
          inputProps={{'aria-label': 'Filtrer sur les définitions relues ou non'}}
          value={selectedFilter}
          displayEmpty
          onChange={(event) => {
            onResetFilters && onResetFilters()
            console.log("event.target.value", {value: event.target.value, true: event.target.value === true, string: event.target.value === "true"})
            switch (event.target.value) {
              case true:
                setFilterOnReviewed(true);
                setFilterOnProcessed(null);
                break
              case false:
                setFilterOnReviewed(false);
                setFilterOnProcessed(false);
                break;
              case "":
                setFilterOnReviewed(null);
                setFilterOnProcessed(null);
                break;
            }

            setSelectedFilter(event.target.value)
            //   handleListTwoChange("");
          }}
        >
          <MenuItem value={""}>
            <em>Relecture</em>
          </MenuItem>
          <MenuItem value={true}>Définitions relues</MenuItem>
          <MenuItem value={false}>Définitions non relues</MenuItem>
        </Select>
      </FormControl>
    )
  }

  function renderSelectListTwo() {
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          data-testid='selectListTwo'
          inputProps={{'aria-label': 'Filtrer sur définitions validées, supprimées ou non traitées'}}
          value={selectedListTwoCode}
          displayEmpty
          onChange={(event) => {
            setSelectedFilter("");
            handleListTwoChange(event.target.value);
          }}
        >
          <MenuItem value={""}>
            <em>Traitement</em>
          </MenuItem>
          <MenuItem value={"reviewed"}>Validées</MenuItem>
          <MenuItem value={"askedForSuppression"}>En demande de suppression</MenuItem>
          <MenuItem value={"notReviewed"}>Non traitées</MenuItem>
        </Select>
      </FormControl>
    )
  }

  function handleListTwoChange(value) {
    setSelectedListTwoCode(value)
    switch (value) {
      case "reviewed":
        setFilterOnReviewed(true);
        setFilterOnProcessed(null);
        setFilterOnExistingSuppressionRating(null);
        break;
      case "askedForSuppression":
        setFilterOnReviewed(true);
        setFilterOnExistingSuppressionRating(true);
        break;
      case "notReviewed":
        setFilterOnProcessed(false);
        setFilterOnExistingSuppressionRating(null);
        setFilterOnReviewed(false);
        break;
      default:
        setFilterOnProcessed(null);
        setFilterOnExistingSuppressionRating(null);
        setFilterOnReviewed(null);
    }
  };

  function renderSelectAuthor() {
    return (
      <FormControl variant="outlined" className={clsx(classes.formControl, classes.borderInput)} ref={containerRef} >
        <PersonAutocompleteMUIA
          showIsRequired={false}
          placeholder={"Auteur"}
          handleChange={handlePersonChange}
          handleSubmit={handlePersonSubmit}
          value={personInputValue || ""}
          inputRef={null}
          containerRef={containerRef}
          addForm={false}
          labelCreator={({node: {nickName, userAccount}}) => ({label: `${nickName} (${userAccount.username})`, id: userAccount.id})}
        />
      </FormControl>
    )
  }

  function handlePersonChange(newObj) {
    //  console.log("handlePersonChange", newObj);
    setPersonInputValue(newObj?.label || "")
    if (newObj?.id) {
      setFilterOnUserAccount(newObj)
    } else {
      setFilterOnUserAccount({})
    }
  }

  function handlePersonSubmit() {return null;}


}


