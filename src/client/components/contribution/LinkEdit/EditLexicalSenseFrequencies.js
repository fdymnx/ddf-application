import React from 'react';
import {gql} from "@apollo/client";
import {ROUTES} from '../../../routes';
import {validationSchemas, _FREQUENCY} from "../form/LexicalSenseEditorConst";
import StepFrequency from '../form/StepFrequency';
import {LinkEdit} from "./LinkEdit";


export function EditLexicalSenseFrequencies(props) {

  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier une fréquence"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="frequencies.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateFrequency}
      fieldnameForDelete="frequencyId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de fréquence"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="une fréquence"
      toStep={_FREQUENCY}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="frequency"
      formikValidationSchema={validationSchemas._FREQUENCY}
      formikRenderStep={(persistedData) => <StepFrequency title="Modifier la fréquence" persistedData={persistedData} />}
    />
  )

}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseFrequencies_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      frequencies {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateFrequency = gql`
 mutation UpdateLexicalSense_frequency(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $frequencyId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      frequencyId: $frequencyId
    }) {
      updatedObject {
        id         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_frequency( $lexicalSenseId: ID!,  $writtenRep: String!, $frequencyId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        frequencyIdToDelete : $frequencyId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;