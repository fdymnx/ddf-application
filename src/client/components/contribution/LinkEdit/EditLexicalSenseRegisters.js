import React from 'react';
import {gql} from "@apollo/client";
import {ROUTES} from '../../../routes';
import {validationSchemas, _REGISTER} from "../form/LexicalSenseEditorConst";
import StepRegister from '../form/StepRegister';
import {LinkEdit} from "./LinkEdit";



export function EditLexicalSenseRegisters(props) {


  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier un registre"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="registers.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateRegister}
      fieldnameForDelete="registerId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de registre"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="un registre"
      toStep={_REGISTER}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="register"
      formikValidationSchema={validationSchemas._REGISTER}
      formikRenderStep={(persistedData) => <StepRegister title="Modifier un registre" persistedData={persistedData} />}
    />
  )

}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseRegisters_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      registers {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateRegister = gql`
 mutation UpdateLexicalSense_register(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $registerId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      registerId: $registerId
    }) {
      updatedObject {
        id
         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_register( $lexicalSenseId: ID!,  $writtenRep: String!, $registerId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        registerIdToDelete : $registerId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;