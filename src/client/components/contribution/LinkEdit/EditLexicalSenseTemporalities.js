import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {gql} from "@apollo/client";
import {ROUTES} from '../../../routes';
import {validationSchemas, _TEMPORALITY} from "../form/LexicalSenseEditorConst";
import StepTemporality from '../form/StepTemporality';
import {LinkEdit} from "./LinkEdit";


const useStyles = makeStyles((theme) => ({

}));

export function EditLexicalSenseTemporalities(props) {

  const classes = useStyles();
  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier une temporalité"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="temporalities.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateTemporality}
      fieldnameForDelete="temporalityId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de temporalité"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="une temporalité"
      toStep={_TEMPORALITY}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="temporality"
      formikValidationSchema={validationSchemas._TEMPORALITY}
      formikRenderStep={(persistedData) => <StepTemporality title="Modifier la temporalité" persistedData={persistedData} />}
    />
  )

}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseTemporalities_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      temporalities {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateTemporality = gql`
 mutation UpdateLexicalSense_temporality(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $temporalityId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      temporalityId: $temporalityId
    }) {
      updatedObject {
        id         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_temporality( $lexicalSenseId: ID!,  $writtenRep: String!, $temporalityId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        temporalityIdToDelete : $temporalityId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;