

import React, {useState} from 'react';
import {Accordion, AccordionSummary, AccordionDetails} from "@material-ui/core";
import {useTranslation} from 'react-i18next';
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {useMutation, gql} from "@apollo/client";
import {useSnackbar} from 'notistack';
import {formatRoute} from 'react-router-named-routes';
import {useHistory, useRouteMatch, useParams} from 'react-router-dom';
import {Formik, Form} from "formik";
import {ROUTES} from '../../../routes';
import {Template} from "./Template";
import {validationSchemas, _SEMANTIC_RELATION} from "../form/LexicalSenseEditorConst";
import {StepSenseRelation} from '../form/StepSenseRelation';
import {FormLexicalSenseList} from '../../Form/FormLexicalSenseList';
import {StepSenseRelationSearchInput} from '../form/StepSenseRelationSearchInput';
import {distinctWrittenRep, StepSenseRelationType} from '../form/StepSenseRelationType';
import {LexicalSenseDefinition} from '../../LexicalSense/LexicalSenseDefinition';
import {formatSemanticRelations} from '../../LexicalSense/LexicalSenseSameSemanticRelationList';
import {LexicalSenseDistinctSemanticRelationList} from "../../LexicalSense/LexicalSenseDistinctSemanticRelationList";
import {BracketButton} from "../../widgets/BracketButton";
import {FormBottom} from "../../widgets/forms/FormBottom";
import DivButton from "../../widgets/DivButton";
import {ShowWhereIAM} from '../../widgets/ShowWhereIAM';
import globalStyles from "../../../assets/stylesheets/globalStyles";
import {PurifyHtml} from '../../../utilities/PurifyHtml';
import {upperCaseFirstLetter} from "../../../utilities/helpers/tools";
import {gqlLexicalSenseSemanticRelationTypeFragment} from '../../../utilities/helpers/lexicalSenseSemanticRelationList';

const currentPlace = {}; // use the same empty object to avoid FromLexicalSenseList to refresh with useEffect(currentPlace)
const _STEP_TYPE = "_STEP_TYPE";
const _STEP_DEF = "_STEP_DEF";
const _STEP_FORM = "_STEP_FORM";
const _STEP_RELATION = "_STEP_RELATION";
const _FINISH = "_FINISH";
const chooseRelType = "Je choisis le type de relation";

/**
 * Add a semantic relation between 
 * 1) the current definition1 to another definition2 of the same form (isDistinctWrittenRep will be true)
 * 
 * 2) the current definition1 to another definition2 of another form 
 * 
 * 
 * @param {*} props 
 * @returns 
 */
export function AddLexicalSenseSemanticRelation(props) {
  const {t} = useTranslation();
  const gS = globalStyles();
  const match = useRouteMatch();
  const history = useHistory();
  let params = useParams();

  const {enqueueSnackbar, closeSnackbar} = useSnackbar();
  /** Initializing state from query string params */
  // in some route match is a empty object, fallback with params then props
  let formWrittenRep = decodeURIComponent(match.params.formQuery || params.formQuery || props?.computedMatch?.params?.formQuery);
  let lexicalSenseId = decodeURIComponent(match.params.lexicalSenseId || params.lexicalSenseId || props?.computedMatch?.params?.lexicalSenseId);

  // keep a copy of formik values after mutation to check if data is well reader 
  // if not trigger a new query to reload them
  // need a copy because Formik in unmounted so values are lost
  const [formikValuesCopy, setFormikValuesCopy] = useState({});

  /* GraphQL query */
  const [updateMutation, {error, loading: isSubmitting}] = useMutation(gqlUpdateQuery, {
    onCompleted: data => {
      /*
        // check if we need to reload the data.  we cannot use returned data because it contain only an ID, adding anything else make api to crash
        
  
        if (formikValuesCopy) {
          const {isDistinctWrittenRep, persistedData} = extractFromValues(formikValuesCopy);
  
          if (!isDistinctWrittenRep) {
            const {senseRelation, toFormWrittenRep, selectedDefinition} = formikValuesCopy;
  
            console.log(JSON.stringify({persistedData}, null, 3));
  
            const searchMe = {
              prefLabel: senseRelation.prefLabel,
              defId: selectedDefinition.id
            }
            console.log(searchMe);
  
            let notFound = persistedData.filter((element) => {
              return element?.prefLabel === searchMe?.prefLabel && element?.defId === searchMe?.defId
            })?.length < 1;
  
            if (notFound) {
              enqueueSnackbar("Sauvegarde en cours…", {variant: "success"});
              setTimeout(() => {
                console.log("get data");
                getData();
              }, 3000);
            }
          }
        }
      */

    }
  });

  const [expanded, setExpanded] = useState(_STEP_TYPE);
  const [persistedDistinctWrittenRep, setPersistedDistinctWrittenRep] = useState([]);
  const [persistedSameWrittenRep, setPersistedSameWrittenRep] = useState([]);


  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  let initialValues = {};
  const initialStatus = {};


  /**
   * <form> handle data : 
   *  - type  : type of relation we want : sameWrittenRep or distinctWrittenRep 
   *  - toFormWrittenRep : selected writtenRep in autocomplete
   *  - selectedDefinition : the selected definition with id/text
   *  - senseRelation : selected semantic type from ConceptChooser 
   */
  return (
    <ShowWhereIAM path="AddLexicalSenseSemanticRelation.js">
      <Template error={error} {...props} formWrittenRep={formWrittenRep} lexicalSenseId={lexicalSenseId}
        showFormWrittenRep={true}
        title={'CONTRIBUTION.ADD_SEMANTIC_RELATION'}>

        <>
          <LexicalSenseDefinition
            formQuery={formWrittenRep}
            lexicalSenseId={lexicalSenseId}
            hideUsageExamples={true}
            setSemanticRelationsInParent={setSameWrittenRep}
          />

          <LexicalSenseDistinctSemanticRelationList
            lexicalSenseId={lexicalSenseId}
            formQuery={formWrittenRep}
            showGlossaries={false}
            setDataInParent={setDistinctWrittenRep}
          />
        </>


        <Formik
          initialValues={initialValues}
          initialStatus={initialStatus}
          enableReinitialize
          validationSchema={validationSchemas._SEMANTIC_RELATION}
          validateOnChange={false}
          validateOnBlur={false}
        >
          {({values, setFieldValue}) => {

            const cancelLabel = t('CONTRIBUTION.CREATE_LEXICAL_SENSE.CANCEL');
            const submitLabel = t('CONTRIBUTION.CREATE_LEXICAL_SENSE.SUBMIT');
            const {isValid, isDistinctWrittenRep, persistedData} = extractFromValues(values);

            const customFilters = {scopeNote: isDistinctWrittenRep ? "distinctWrittenRep" : "sameWrittenRep"};

            return (
              <Form noValidate aria-label="Ce formulaire permet de rajouter un lien entre des mots ou des définitions">

                <Accordion expanded={expanded === _STEP_TYPE} onChange={handleChange(_STEP_TYPE)}>
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <div className={gS.stepTemplateTitle}>
                      {!values?.type?.id ? chooseRelType : values?.type?.prefLabel}
                    </div>
                  </AccordionSummary>
                  <AccordionDetails>

                    <StepSenseRelationType onChangeCallBack={(params) => showNextStep({...params, values, isDistinctWrittenRep, setFieldValue})} />
                  </AccordionDetails>
                </Accordion>

                {isDistinctWrittenRep &&
                  <Accordion expanded={expanded === _STEP_FORM} onChange={handleChange(_STEP_FORM)}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                      <div className={gS.stepTemplateTitle}>
                        {renderStep2FormTitle({values})}
                      </div>
                    </AccordionSummary>
                    <AccordionDetails>
                      <StepSenseRelationSearchInput onChangeCallBack={(params) => showNextStep({...params, values, isDistinctWrittenRep, setFieldValue})} />
                    </AccordionDetails>
                  </Accordion>
                }

                <Accordion disabled={isDistinctWrittenRep ? !values?.toFormWrittenRep?.id : !values?.type?.id}
                  expanded={expanded === _STEP_DEF} onChange={handleChange(_STEP_DEF)}>
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <div className={gS.stepTemplateTitle}>
                      {renderStep2DefTitle({values})}
                    </div>
                  </AccordionSummary>
                  <AccordionDetails>
                    <FormLexicalSenseList
                      enableAdd={false}
                      formQuery={values?.toFormWrittenRep?.label || formWrittenRep}
                      currentPlace={currentPlace}
                      selectedDefId={values?.selectedDefinition?.id}
                      excludeLexicalSenseId={lexicalSenseId}
                      onItemClickCallBack={(item) => {
                        if (item?.id === values?.selectedDefinition?.id) {
                          setFieldValue("selectedDefinition", false);
                          showNextStep({name: "selectedDefinition", value: false, values, isDistinctWrittenRep, setFieldValue});
                        } else {
                          setFieldValue("selectedDefinition", item);
                          showNextStep({name: "selectedDefinition", value: true, values, isDistinctWrittenRep, setFieldValue});
                        }
                      }} />
                  </AccordionDetails>
                </Accordion>

                <Accordion disabled={!values?.selectedDefinition?.id} expanded={expanded === _STEP_RELATION} onChange={handleChange(_STEP_RELATION)}>
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                    <div className={gS.stepTemplateTitle}>
                      {upperCaseFirstLetter(!values?.senseRelation?.id ? chooseRelType : values?.senseRelation?.prefLabel)}
                    </div>
                  </AccordionSummary>
                  <AccordionDetails>
                    <StepSenseRelation
                      onChangeCallBack={(params) => showNextStep({...params, values, isDistinctWrittenRep, setFieldValue})}
                      customFilters={customFilters}
                      persistedData={persistedData}
                      placeHolder=""
                    />
                  </AccordionDetails>
                </Accordion>


                <br /><br />
                <FormBottom>
                  <div className={gS.buttonRow}>
                    <DivButton
                      type="button"
                      onClick={onCancel}
                      classesName={gS.cancelButton}
                      aria-label={cancelLabel}
                    >
                      {cancelLabel}
                    </DivButton>
                    <BracketButton
                      greyBg={true}
                      aria-label={submitLabel}
                      text={submitLabel}
                      type="submit"
                      disabled={isSubmitting || !isValid}
                      loading={isSubmitting}
                      onClick={() => handleSubmit({isDistinctWrittenRep, values})}
                    />
                  </div>
                </FormBottom>
              </Form>
            )
          }}
        </Formik>
      </Template>
    </ShowWhereIAM>
  )

  function showNextStep({name, value, values, isDistinctWrittenRep, setFieldValue}) {

    let nextStep = false;

    switch (name) {
      case 'type':
        nextStep = value?.id === distinctWrittenRep.id ? _STEP_FORM : _STEP_DEF;
        setFieldValue("toFormWrittenRep", false);
        setFieldValue("selectedDefinition", false);
        setFieldValue("senseRelation", false);
        break;
      case 'toFormWrittenRep':
        nextStep = _STEP_DEF;
        setFieldValue("selectedDefinition", false);
        setFieldValue("senseRelation", false);
        break;
      case 'selectedDefinition':
        nextStep = _STEP_RELATION;
        setFieldValue("senseRelation", false);
        break;
      case 'senseRelation':
        nextStep = _FINISH;
        break;
    }
    if (value && nextStep) {
      setTimeout(() => {
        setExpanded(nextStep)
      }, 1000);
    }
  }

  function renderStep2FormTitle({values}) {
    if (values?.toFormWrittenRep?.id && values?.toFormWrittenRep?.label) {
      return values?.toFormWrittenRep?.label;
    } else {
      return "Je choisi le mot à lier";
    }
  }

  function renderStep2DefTitle({values}) {
    if (values?.selectedDefinition?.id && values?.selectedDefinition?.definition) {
      return <PurifyHtml html={values?.selectedDefinition.definition} />
    } else {
      return "Je choisi la définition à lier";
    }
  }

  /**
   * this function receive data shape like this 
   * 
   * { 'hyponyme':[ {defId,writtenRep},{defId,writtenRep } ]}
   * we need data like this so we can filter on defId easly 
   * [ {defId, writtenRep, prefLabel:'hyponyme'} , {defId, writtenRep, prefLabel:'hyponyme' } ]
   * 
   * @param {*} data 
   */
  function setDistinctWrittenRep(data) {
    let ndata = [];
    for (const i in data) {
      for (const j of data[i]) {
        ndata.push({...j, prefLabel: i})
      }
    }
    setPersistedDistinctWrittenRep(ndata);
  }

  function setSameWrittenRep(semanticRelations) {
    setPersistedSameWrittenRep(formatSemanticRelations(semanticRelations, lexicalSenseId));
  }


  function onCancel() {
    navigateBack();
  }

  async function handleSubmit({isDistinctWrittenRep, values}) {
    console.log(" AddLexicalSenseSemanticRelation.js handleSubmit", {isDistinctWrittenRep, values})
    // formWrittenRep contain a selected word from autocomplete
    // selectedDefinition contain a selected definition
    const {senseRelation, toFormWrittenRep, selectedDefinition, type} = values;
    setFormikValuesCopy(values);

    let variables = {
      lexicalSenseId,
      writtenRep: formWrittenRep,
      semanticRelationInput: {
        semanticRelationType: {id: senseRelation.id},
        semanticRelationOfLexicalSenseInput: {id: selectedDefinition.id}
      }
    };

    /*  
    if (isDistinctWrittenRep && toFormWrittenRep?.id) {
      variables.semanticRelationInput.semanticRelationOfLexicalEntryInput = {id: toFormWrittenRep.id}
    }
    */
    if (isDistinctWrittenRep && selectedDefinition?.lexicalEntry?.id) {
      variables.semanticRelationInput.semanticRelationOfLexicalEntryInput = {id: selectedDefinition.lexicalEntry?.id}
    }

    try {
      await updateMutation({variables});
      if (!error) {
        navigateBack();
      }
    } catch (error) {
      console.log(error);
    }
  }

  function navigateBack() {
    // let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {formQuery: formWrittenRep, lexicalSenseId}) + "?allsteps=true";
    // redirect to this page to see all new added data 
    let senseUrl = formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_SEMANTIC_RELATIONS, {formQuery: formWrittenRep, lexicalSenseId});

    history.push(senseUrl);
  }

  function extractFromValues(values) {
    const isDistinctWrittenRep = values?.type?.id === distinctWrittenRep.id;
    const isValid = !!values?.type?.id && !!values?.selectedDefinition?.id && !!values?.senseRelation?.id && (isDistinctWrittenRep ? values?.toFormWrittenRep?.id : true);
    const persistedData = getPersistedData(values, isDistinctWrittenRep);
    return {isValid, isDistinctWrittenRep, persistedData};
  }


  function getPersistedData(values, isDistinctWrittenRep) {
    if (values?.selectedDefinition?.id) {
      let source = isDistinctWrittenRep ? persistedDistinctWrittenRep : persistedSameWrittenRep;
      return source?.filter(obj => {
        // sameWrittenRep doesn't have defId field    
        return obj?.defId ? obj.defId === values?.selectedDefinition?.id : true;
      });
    }
    return null;
  }
}


const gqlUpdateQuery = gql`
 mutation UpdateLexicalSense_semanticRelation(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $semanticRelationInput: SemanticRelationInput,
    $includeCreator: Boolean = true
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      semanticRelationInput: $semanticRelationInput    
    }) {
      updatedObject {
        id
        ...LexicalSenseSemanticRelationTypeFragment     
      }
    }
  }    
  ${gqlLexicalSenseSemanticRelationTypeFragment}
`;

