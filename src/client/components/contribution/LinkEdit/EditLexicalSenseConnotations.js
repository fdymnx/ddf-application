import React from 'react';
import {gql} from "@apollo/client";
import {validationSchemas, _CONNOTATION} from "../form/LexicalSenseEditorConst";
import StepConnotation from '../form/StepConnotation';
import {LinkEdit} from "./LinkEdit";


export function EditLexicalSenseConnotations(props) {

  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier une connotation"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="connotations.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateConnotation}
      fieldnameForDelete="connotationId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de connotation"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="une connotation"
      toStep={_CONNOTATION}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="connotation"
      formikValidationSchema={validationSchemas._CONNOTATION}
      formikRenderStep={(persistedData) => <StepConnotation title="Modifier la connotation" persistedData={persistedData} />}
    />
  )

}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseConnotations_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      connotations {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateConnotation = gql`
 mutation UpdateLexicalSense_connotation(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $connotationId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      connotationId: $connotationId
    }) {
      updatedObject {
        id         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_connotation( $lexicalSenseId: ID!,  $writtenRep: String!, $connotationId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        connotationIdToDelete : $connotationId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;