ajout LinkEdit

> src/client/MainView.js

const {EditLexicalSenseConnotations} = lazily(/* webpackChunkName: "contribution" */() => import('./components/contribution/LinkEdit/EditLexicalSenseConnotations'));

<ProtectedRoute
  isLogged={isLogged}
  isLoadingUser={isLoadingUser}
  exact
  path={ROUTES.FORM_LEXICAL_SENSE_EDIT_CONNOTATIONS}
  component={EditLexicalSenseConnotations}
/>


> routes.js

  {
    name: 'FORM_LEXICAL_SENSE_EDIT_CONNOTATIONS',
    path: '/form/:formQuery/sense/:lexicalSenseId/edit/connotations',
    isMainNavigationSpace: true,
    needsAuthentication: true,
  },


> src/client/components/contribution/EditLexicalSense.js

l242 : 
const gqlUpdateLexicalSense = gql`
  mutation UpdateLexicalSense(
   
    $connotationId:String
  ) {
    updateLexicalSense(
      input: {
        
        connotationId: $connotationId
      }
    ) {  



> src/client/components/contribution/form/GoTo.js

L18  

import {
  _EDIT_CONNOTATIONS,
} from "./LexicalSenseEditorConst"

L87 marqueUsageButtons

L145 
function createLink(step) {
	case _EDIT_CONNOTATIONS:
        return formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT_CONNOTATIONS, {formQuery, lexicalSenseId});
  
}

> src/client/components/contribution/form/LexicalSenseEditorConst.js
L42 
export const _CONNOTATION = "_CONNOTATION";
export const _EDIT_CONNOTATIONS = "_EDIT_CONNOTATIONS";

L111
  _CONNOTATION: Yup.object().shape({
    connotation: Yup.object().required("Champ obligatoire")
  }),


> create src/client/components/contribution/form/StepConnotation.js
> create src/client/components/contribution/LinkEdit/EditLexicalSenseConnotations.js

> src/client/components/contribution/LinkEdit/LinkEdit.js

L540
const allowedDeleteFields = {
  connotationId:"connotations",
}


> src/server/datamodel/ontologies/ontolex/graphql/LexicalSenseGraphQLDefinition.js

// all this field need to be added to getUpdateMutation.GraphQLUpdateMutation.extraInputArgs
export const allowedIdFields = {
  connotationId: "connotationInput",
};
// all this field need to be added to getUpdateMutation.GraphQLUpdateMutation.extraInputArgs
export const allowedToDeleteFields = { 
  connotationIdToDelete: "connotationInputToDelete",
};

L881 
static getUpdateMutation() {
    return new GraphQLUpdateMutation({
      extraInputArgs: `

        """ related connotation ID """
        connotationId: String
        """ connotation ID to delete """
        connotationIdToDelete: String

