import React from 'react';
import {gql} from "@apollo/client";
import {validationSchemas, _DOMAIN} from "../form/LexicalSenseEditorConst";
import StepDomain from '../form/StepDomain';
import {LinkEdit} from "./LinkEdit";

export function EditLexicalSenseDomains(props) {

  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier un domaine"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="domains.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateDomains}
      fieldnameForDelete="domainId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de domaine"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="un domaine"
      toStep={_DOMAIN}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="domain"
      formikValidationSchema={validationSchemas._DOMAIN}
      formikRenderStep={(persistedData) => <StepDomain title="Modifier le domaine" persistedData={persistedData} />}
    />
  )


}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseDomains_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      domains {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateDomains = gql`
 mutation UpdateLexicalSense_domain(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $domainId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      domainId: $domainId
    }) {
      updatedObject {
        id
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_domain( $lexicalSenseId: ID!,  $writtenRep: String!, $domainId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        domainIdToDelete : $domainId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;