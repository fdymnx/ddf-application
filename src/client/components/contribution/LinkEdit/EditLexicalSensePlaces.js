import React from 'react';
import {gql} from "@apollo/client";
import Config from '../../../Config';
import {ROUTES} from '../../../routes';
import {validationSchemas, _PLACE} from "../form/LexicalSenseEditorConst";
import {colorToCssClassName} from '../../../utilities/helpers/countries';
import {countryColor} from '../../../utilities/helpers/countryContinentMapping';
import {gqlLexicalSenseCountriesFragment} from '../../../utilities/helpers/countries';
import {LinkEdit} from "./LinkEdit";
import StepPlace from '../form/StepPlace';

export function EditLexicalSensePlaces(props) {

  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier une localisation"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="places.edges"
      gqlDelete={gqlDeleteLexicalSense}
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      gqlUpdate={gqlUpdatePlace}
      fieldnameForDelete="placeId"
      addLabel="une localisation"
      noDataText="Cette définition n'a pas encore de localisation"
      createStyle={(place) => {
        // some place have color props, some not so fallback on countryCode to get it because each color is for a continent
        const color = place?.color ? Config.colors[colorToCssClassName(place?.color)] : place?.countryCode ? countryColor(place?.countryCode) : Config.colors.darkgray;
        return {backgroundColor: color, color: Config.colors.white};
      }}
      itemToString={placeToString}
      renderItemLabel={(item) => <div>
        {placeToString(item)}
      </div>}
      toStep={_PLACE}

      enableLocalAdd={true}
      fieldnameForEdit="place"
      formikValidationSchema={validationSchemas._PLACE}
      formikRenderStep={(persistedData) => <StepPlace title="Modifier le domaine" persistedData={persistedData} />}
    />
  )


  function placeToString(item) {
    let locTmp = "";
    const {name, stateName, countryName} = item;
    if (countryName?.length > 0) {
      locTmp = countryName;
    }
    if (stateName?.length > 0 && stateName != countryName) {
      if (locTmp.length > 0) {
        locTmp += " - ";
      }
      locTmp += stateName;
    }
    if (name?.length > 0 && countryName != name && stateName != name) {
      if (locTmp.length > 0) {
        locTmp += " - ";
      }
      locTmp += name;
    }
    return locTmp;
  }
}


const gqlLexicalSenseQuery = gql`
  query LexicalSensePlaces_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      ...LexicalSenseCountriesFragment      
    }
  }
  ${gqlLexicalSenseCountriesFragment}
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_places( $lexicalSenseId: ID!,  $writtenRep: String!, $placeId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        placeIdsToDelete : [$placeId]
      }
    ) {
      updatedObject {
        id      
        ...LexicalSenseCountriesFragment
      }
    }
  }
  ${gqlLexicalSenseCountriesFragment}
`;


const gqlUpdatePlace = gql`
 mutation UpdateLexicalSense_places(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $placeId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      placeId: $placeId
    }) {
      updatedObject {
        id         
      }
    }
  }
`;