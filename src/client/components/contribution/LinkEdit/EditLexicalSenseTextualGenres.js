//EditLexicalSenseTextualGenres


import React from 'react';

import {gql} from "@apollo/client";
import {validationSchemas, _TEXTUALGENRE} from "../form/LexicalSenseEditorConst";
import StepTextualGenre from '../form/StepTextualGenre.js';
import {LinkEdit} from "./LinkEdit";



export function EditLexicalSenseTextualGenres(props) {
  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier un genre textuel"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="textualGenres.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateTextualGenre}
      fieldnameForDelete="textualGenreId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore de genre textuel"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="un genre textuel"
      toStep={_TEXTUALGENRE}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="textualGenre"
      formikValidationSchema={validationSchemas._TEXTUALGENRE}
      formikRenderStep={(persistedData) => <StepTextualGenre title="Modifier un genre textuel" persistedData={persistedData} />}
    />
  )

}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseTextualGenres_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      textualGenres {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateTextualGenre = gql`
 mutation UpdateLexicalSense_TextualGenre(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $textualGenreId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      textualGenreId: $textualGenreId
    }) {
      updatedObject {
        id
         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_TextualGenre( $lexicalSenseId: ID!,  $writtenRep: String!, $textualGenreId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        textualGenreIdToDelete : $textualGenreId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;