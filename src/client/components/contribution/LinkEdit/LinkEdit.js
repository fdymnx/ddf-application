import React, {useEffect, useState, useContext} from "react";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import invariant from "invariant";
import {useLazyQuery, useMutation, gql} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import Grid from "@material-ui/core/Grid";
import {useRouteMatch, useParams} from "react-router-dom";
import {Formik, Form} from "formik";
import get from "lodash/get";
import clsx from "clsx";
import {useSnackbar} from 'notistack';
import {useMediaQueries} from "../../../layouts/MediaQueries";
import Config from "../../../Config";
import {ROUTES} from '../../../routes';
import {Template} from "./Template";
import DivButton from "../../widgets/DivButton";
import {BracketButton} from "../../widgets/BracketButton";
import {FormBottom} from "../../widgets/forms/FormBottom";
import deleteIcon from "../../../assets/images/cross_purple.svg";
import deleteIconGrey from "../../../assets/images/cross_grey.svg";
import addYellowIcon from '../../../assets/images/add_yellow.svg';
import editIcon from "../../../assets/images/contributions_purple.svg";
import editIconGrey from "../../../assets/images/contributions_grey.svg";
import {_PLACE} from "../form/LexicalSenseEditorConst";
import globalStyles from "../../../assets/stylesheets/globalStyles.js";
import {IconButton} from "../form/GoTo";
import ConfirmDialog from "../../widgets/ConfirmDialog";
import {GraphQLErrorHandler} from "../../../services/GraphQLErrorHandler";
import {notificationService} from "../../../services/NotificationService";
import {DisplayEntry} from "../../widgets/forms/ConceptChooser";
import {ShowWhereIAM} from '../../widgets/ShowWhereIAM';
import LoadingGif from "../../widgets/LoadingGif";
import {LexicalSenseDefinition} from "../../LexicalSense/LexicalSenseDefinition";
import {showSnackbar} from "../../../utilities/helpers/showSnackbar";

const useStyles = makeStyles((theme) => ({
  tinyML: {
    marginLeft: Config.spacings.tiny,
  },
  title: {
    marginTop: Config.spacings.medium,
    marginBottom: Config.spacings.large
  },
  smallText: {
    fontSize: `${Config.fontSizes.medium}px`,
    fontStyle: "italic"
  },
  icon: {
    height: "20px",
    marginRight: Config.spacings.small
  },
  item: {
    border: `1px solid ${Config.colors.mediumgray}`,
    fontSize: `${Config.fontSizes.large - 2}px`,
    minHeight: '70px',
    display: "flex",
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: Config.spacings.small,
    padding: Config.spacings.small,
    backgroundColor: Config.colors.white,
  },
  leftMargin0: {
    marginLeft: "0px"
  },
  whiteBg: {
    backgroundColor: Config.colors.white
  },

  colorPurple: {
    color: Config.colors.darkpurple
  },

  loadingContainer: {
    minHeight: "200px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }

}));


/**
 * This component show specific linked items to a lexicalSense
 * it can add/edit/remove items 
 * 
 * add & edit use the same parameters : gqlUpdate, fieldnameForEdit, formikInitializeValue, formikValidationSchema, formikRenderStep
 * 
 * @param {gqlQuery} gqlQuery 
 * @param {string} gqlConnectionPath 
 * @param {object} customGqlVariables : pass custom props to gql variables
 * @param {string} templateTitle : main title
 * @param {string} customDataText : optional, text displayed on top of persisted data to replace the default one. for example when user cannot delete items.
 * @param {string} noDataText : text displayed when there is no data
 * @param {function} createStyle : optional, function who return custom style applied at GridItem (like place color for each country)
 * @param {function} customRenderDatas : optional, by default grid layer is used, some link need another display link semanticRelations
 * @param {function} renderItemLabel : how to display an item label inside the default grid layer. Each one can have different properties like place or usageExemple
 * @param {function} itemToString : stringify an item to be displayed in "delete confirm modal"
 * @param {string} toStep : if addLinkRoute is not provided, "add new item" will route to FORM_LEXICAL_SENSE_EDIT + "?step=" +toStep
 * @param {string} addLabel : label of "add new item" button
 * @param {string} addLinkRoute :  optional, use custom route for add button instead of  FORM_LEXICAL_SENSE_EDIT + "?step=" +toStep
 * @param {boolean} enableDelete 
 * @param {gqlQuery} gqlDelete 
 * @param {string} fieldnameForDelete 
 * @param {boolean} enableEdit 
 * @param {gqlQuery} gqlUpdate 
 * @param {string} fieldnameForEdit : fieldname for gql update mutation, 
 * @param {*} formikInitializeValue : optional, function to initialize formik, for textarea to prevent warning of uncontrolled data
 * @param {*} formikValidationSchema 
 * @param {*} formikRenderStep : which step to add link to ?
 * @param {boolean} enableLocalAdd :use this component to add an element, don't route to EditLexicalSense  
 * @param {boolean} showLexicalSenseDefinition : show lexicalSenseDefinition inside the linkEdit    
 * @param {boolean} showDeleteIconInLexicalSenseDefinition : show delete icon on each semantic relation 
 * 
 * NEED TO BE REMOVED and use canUpdate properties once link will change to reified  
 * for now we cannot know which user add which link because they are not reified ( only useExemple && semanticRelation )
 * @param {boolean} allowAnyOneToDeleteLink_until_reification : allow any user to remove this link,
 * @returns 
 */
export function LinkEdit({
  gqlQuery, gqlConnectionPath, customGqlVariables = {},
  templateTitle, noDataText, customDataText = null,
  createStyle, renderItemLabel, itemToString,
  customRenderDatas = null,
  toStep, addLabel, addLinkRoute = null,
  enableDelete = false, gqlDelete, fieldnameForDelete,
  enableEdit = false, gqlUpdate, fieldnameForEdit,
  formikInitializeValue = () => {return {}}, formikValidationSchema, formikRenderStep,
  enableLocalAdd = false,
  showLexicalSenseDefinition = true,
  showDeleteIconInLexicalSenseDefinition = false,
  allowAnyOneToDeleteLink_until_reification = false,
  ...props
}) {

  if (enableEdit || enableLocalAdd) {
    invariant(formikValidationSchema && formikRenderStep, "LinkEdit enableLocalAdd/enableEdit parameters not initialized well");
    invariant(gqlUpdate, "LinkEdit, gqlUpdate should be not null");
  } else {
    gqlUpdate = emptyMutation;
  }

  if (enableDelete) {
    invariant(gqlDelete, "LinkEdit, gqlDelete should be not null");
  } else {
    gqlDelete = emptyMutation
  }

  const {t} = useTranslation();

  const {enqueueSnackbar, closeSnackbar} = useSnackbar();
  const gS = globalStyles();
  const {isMobile} = useMediaQueries();
  const match = useRouteMatch();
  const classes = useStyles();
  // open modal to confirm item deletion
  const [confirmToDelete, setConfirmToDelete] = useState(false);
  // selected item to edit
  const [selectedItem, setSelectedItem] = useState(false);
  // show formik to add new element with <StepXXX />
  const [showStep, setShowStep] = useState(false);

  // keep a copy of formik values after mutation to check if data is well reader 
  // if not trigger a new query to reload them
  // need a copy because Formik in unmounted so values are lost
  const [formikValuesCopy, setFormikValuesCopy] = useState({});

  let params = useParams();
  /** Initializing state from query string params */
  // in some route match is a empty object, fallback with params then props
  let formWrittenRep = decodeURIComponent(match.params.formQuery || params.formQuery); // computedMatch?.params?.formQuery);
  let lexicalSenseId = decodeURIComponent(match.params.lexicalSenseId || params.lexicalSenseId); // || computedMatch?.params?.lexicalSenseId);

  /* GraphQL query */
  const [getData, {data: {lexicalSense} = {}, loading}] = useLazyQuery(gqlQuery, {
    variables: {lexicalSenseId, ...customGqlVariables},
    fetchPolicy: 'cache-and-network',
    onCompleted: n => {closeSnackbar && closeSnackbar()}
  });

  useEffect(() => {
    if (gqlQuery) {
      getData();
    }

    // to prevent `Warning: Can't perform a React state update on an unmounted component.` 
    return () => {
      setConfirmToDelete(false);
      setSelectedItem(false);
      setShowStep(false);
    }
  }, []);


  const datas = loading ? [] : get(lexicalSense, gqlConnectionPath, []).map(({node}) => node);

  const [deleteData, {loading: isDeleting, error: deleteError}] = useMutation(gqlDelete, {
    update: (cache, ndata) => {
      if (fieldnameForDelete === "semanticRelationId") {
        const normalizedId = cache.identify({id: ndata?.data?.removeEntity?.deletedId, __typename: 'SemanticRelation'});
        cache.evict({id: normalizedId});
        cache.gc();
      } else if (allowedDeleteFields?.[fieldnameForDelete]) {
        reloadData()
        /*
        const normalizedId = cache.identify({id: ndata?.data?.removeEntity?.deletedId, __typename: allowedDeleteFields?.[fieldnameForDelete]});
        cache.evict({id: normalizedId});
        cache.gc();
        */
      }
    }
  });


  function reloadData() {
    enqueueSnackbar("Rafraichissement des données…", {variant: "success"});
    //showSnackbar("Rafraichissement des données…", enqueueSnackbar);
    setTimeout(() => {
      if (!loading) {
        getData()
      }
    }, 2000);
  }


  const [updateData, {loading: isEditing, error: editError}] = useMutation(gqlUpdate, {
    onCompleted: data => {
      /**
       *  check if we need to reload the data. 
       * we cannot use returned data because it contain only an ID, adding anything else make api to crash
      */
      if (formikValuesCopy?.[fieldnameForEdit]) {
        const {value, prefLabel, id} = formikValuesCopy?.[fieldnameForEdit];
        // value is used by usageExemple, id by place 
        let searchMe = value || prefLabel || id;
        if (searchMe?.length > 0 && !JSON.stringify(lexicalSense).includes(searchMe)) {
          enqueueSnackbar("Sauvegarde en cours…", {variant: "success"});
          //showSnackbar("Sauvegarde en cours…", enqueueSnackbar);
          setTimeout(() => {
            if (searchMe?.length > 0 && !JSON.stringify(lexicalSense).includes(searchMe)) {
              getData();
            }
          }, 3000);
        }
      }
    }
  });



  useEffect(() => {
    if (deleteError) {
      let {globalErrorMessage} = GraphQLErrorHandler(deleteError, {t});
      notificationService.error(globalErrorMessage);
    }
    if (editError) {
      let {globalErrorMessage} = GraphQLErrorHandler(editError, {t});
      notificationService.error(globalErrorMessage);
    }
  }, [deleteError, editError]);

  return (
    <ShowWhereIAM path="LinkEdit">
      <>
        <Template error={false} {...props}
          formWrittenRep={formWrittenRep}
          lexicalSenseId={lexicalSenseId}
          title={templateTitle}
          showDesktopSubHeader={true}
          showFormWrittenRep={true}
        >
          <div className={classes.tinyML}>
            {showLexicalSenseDefinition &&
              <LexicalSenseDefinition
                formQuery={formWrittenRep}
                lexicalSenseId={lexicalSenseId}
                hideUsageExamples={true}
                setConfirmToDelete={setConfirmToDelete}
                showDeleteIcon={showDeleteIconInLexicalSenseDefinition}
              />
            }
            {renderContent()}
          </div>
        </Template>
        <ConfirmDialog
          isOpen={!!confirmToDelete}
          onClose={handleCloseConfirm}
          confirmText={"SUPPRIMER"}
          onConfirm={handleRemoveItem}
          loading={isDeleting}
        >
          Vous êtes sur le point de supprimer <span className={classes.colorPurple}>{itemToString(confirmToDelete)}</span>.<br />
          Êtes-vous sûr ?
        </ConfirmDialog>
      </>
    </ShowWhereIAM>
  )

  function renderContent() {
    // on affiche le loader que au premier chargement de la page
    if (loading && !lexicalSense) {
      return <div className={classes.loadingContainer}>
        <LoadingGif />
      </div>
    }
    if (showStep || selectedItem) {
      const cancelLabel = t('CONTRIBUTION.CREATE_LEXICAL_SENSE.CANCEL');
      const submitLabel = t('CONTRIBUTION.CREATE_LEXICAL_SENSE.SUBMIT');
      // pour éviter react-dom.development.js:88 Warning: `value` prop on `textarea` should not be null. Consider using an empty string to clear the component or `undefined` for uncontrolled components.
      const initialValues = formikInitializeValue(selectedItem);

      return <Formik

        initialValues={initialValues}
        validationSchema={formikValidationSchema}
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={handleEdit}>
        {({values, status, isValid, errors}) => {
          function isEmptyValue() {
            // const emptyValue = enableLocalAdd ? !values?.[fieldnameForEdit]?.id : !values?.[fieldnameForEdit]?.value?.length > 0;
            const emptyValue = !(values?.[fieldnameForEdit]?.id || values?.[fieldnameForEdit]?.value?.length > 0);
            // console.log({emptyValue, enableLocalAdd, values, fieldnameForEdit, "values?.[fieldnameForEdit]?.id": values?.[fieldnameForEdit]?.id})
            return emptyValue
          }

          return (
            <Form noValidate aria-label="Ce formulaire permet d'editer l'exemple et sa source">

              {formikRenderStep(datas)}

              <FormBottom>
                <div className={gS.buttonRow}>
                  <DivButton
                    type="button"
                    onClick={() => {setShowStep(false); setSelectedItem(false)}}
                    classesName={gS.cancelButton}
                    aria-label={cancelLabel}
                  >
                    {cancelLabel}
                  </DivButton>

                  <BracketButton
                    greyBg={true}
                    aria-label={submitLabel}
                    text={submitLabel}
                    type="submit"
                    disabled={isEmptyValue() || isEditing}
                    loading={isEditing}
                    onClick={() => {setFormikValuesCopy(values)}}
                  />

                </div>
              </FormBottom>
            </Form>
          )
        }}
      </Formik>
    } else {
      return <>
        <div className={clsx(gS.stepTemplateTitle, classes.title)}>
          {!gqlQuery || datas.length > 0 ?
            customDataText || <span>La <i>croix</i> permet de supprimer une information. Le <i>plus</i> permet d'ajouter une information. Il est possible que certaines informations parentes soient automatiquement ajoutées par inférence.</span>
            : noDataText}
        </div>

        {customRenderDatas ?
          <>
            {customRenderDatas({
              formWrittenRep,
              lexicalSenseId,
              setConfirmToDelete
            })}
            {renderAddNewItem(true)}
          </>
          :
          <Grid
            container
            direction="row"
            justifyContent={isMobile ? "flex-start" : "center"}
            justifyContent="flex-start"
            alignItems="flex-start"
          >
            {renderGrids()}
            {renderAddNewItem(false)}
          </Grid>
        }
      </>
    }
  }

  function renderGrids() {
    let res = [];
    if (datas?.length > 0) {
      res = datas.map((item, index) => {
        return <GridItem key={index} item={item} createStyle={createStyle} mine={isItemMine(item)}
          renderItemLabel={renderItemLabel}
          enableDelete={enableDelete} enableEdit={enableEdit} setConfirmToDelete={setConfirmToDelete} setSelectedItem={setSelectedItem}
        />
      })
    }
    return res;
  }

  function isItemMine(item) {
    if (allowAnyOneToDeleteLink_until_reification) {
      return true
    } else {
      console.log({
        ls: lexicalSense?.canUpdate, item
      })
      /**
       * it test lexicalSense?.canUpdate , if user own this lexicalSense it can edit whatever he want
       * if test item?.canUpdate if user doesn't own this lexicalSense but added this link
       */
      return lexicalSense?.canUpdate || item?.canUpdate;
    }
  }

  function renderAddNewItem(removeLeftMargin) {
    function btn() {
      if (addLinkRoute) {
        return <IconButton label={addLabel} keyIndex={0} removeMarginPadding={true} icon={addYellowIcon}
          to={formatRoute(addLinkRoute, {formQuery: formWrittenRep, lexicalSenseId})}
        />
      } else if (enableLocalAdd) {
        return <IconButton label={addLabel} keyIndex={0} removeMarginPadding={true} icon={addYellowIcon} onClick={() => {setShowStep(true)}} />
      } else {
        return <IconButton label={addLabel} keyIndex={0} removeMarginPadding={true} icon={addYellowIcon}
          to={formatRoute(ROUTES.FORM_LEXICAL_SENSE_EDIT, {formQuery: formWrittenRep, lexicalSenseId}) + "?step=" + toStep}
        />
      }
    }
    return <Grid item xs={12} md={6} xl={4} key={"addnewItem"}>
      <div className={clsx(classes.item, classes.whiteBg, removeLeftMargin && classes.leftMargin0)}>
        {btn()}
      </div>
    </Grid>
  }

  function handleCloseConfirm() {
    setConfirmToDelete(false);
  };

  async function handleRemoveItem() {

    if (!loading) {
      let variables = {
        lexicalSenseId,
        writtenRep: formWrittenRep,
        [fieldnameForDelete]: confirmToDelete?.id
      };
      try {
        await deleteData({variables});
        setSelectedItem(false);
        setShowStep(false);
      } catch (error) {
        console.log(error)
      } finally {
        handleCloseConfirm();
      }
    }
  }

  async function handleEdit(formikValues, formikOptions) {
    let variables = {
      lexicalSenseId,
      writtenRep: formWrittenRep
    };

    if (enableLocalAdd && fieldnameForEdit === "usageExample") {
      variables[fieldnameForEdit] = formikValues[fieldnameForEdit];
    } else {
      variables[fieldnameForEdit + "Id"] = formikValues[fieldnameForEdit]?.id;
    }

    try {
      await updateData({variables});
      setSelectedItem(false);
      setShowStep(false);
    } catch (error) {
      console.log(error)
    } finally {
      formikOptions.setSubmitting(false);
    }
  }
}


function GridItem({index, item, createStyle = () => { }, mine, enableDelete, enableEdit, setConfirmToDelete, setSelectedItem, renderItemLabel}) {
  const classes = useStyles();
  const deleteOnly = (!(enableDelete && enableEdit)) && enableDelete;
  const editOnly = (!(enableDelete && enableEdit)) && enableEdit;

  return (
    <Grid item xs={12} md={6} xl={4} item>
      <DivButton aria-label={item.name} classesName={classes.item} style={createStyle(item)}
        disabled={!mine}
        onClick={() => {
          // the whole button have onClick only one action is enabled
          if (deleteOnly) {
            setConfirmToDelete(item);
          } else if (editOnly) {
            setSelectedItem(item);
          }
        }}
        tooltipTitle={mine ? null : "Impossible d'éditer une information dont vous n'êtes pas l'auteur."}>
        {renderItemLabel ? renderItemLabel(item) : <DisplayEntry text={item?.prefLabel} subText={item?.definition} />}
        {enableDelete &&
          <img className={classes.icon} src={mine ? deleteIcon : deleteIconGrey} alt="Effacer" onClick={() => {deleteOnly && mine && setConfirmToDelete(item)}} />
        }
        {enableEdit &&
          <img className={classes.icon} src={mine ? editIcon : editIconGrey} alt="Editer" onClick={() => {editOnly && mine && setSelectedItem(item)}} />
        }
      </DivButton>
    </Grid>
  )
}


const emptyMutation = gql`
  mutation UpdateLexicalSense(
    $lexicalSenseId: ID!,
    $writtenRep: String!  
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep     
    }) {
      updatedObject {
        id       
      }
    }
  }
`;


const allowedDeleteFields = {
  domainId: "domains",
  frequencyId: "frequencies",
  glossaryId: "glossaries",
  grammaticalConstraintId: "grammaticalConstraints",
  placeId: "places",
  registerId: "registers",
  connotationId:"connotations",
  sociolectId: "sociolects",
  temporalityId: "temporalities",
  textualGenreId: "TextualGenres"
}