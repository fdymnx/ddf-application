import React from 'react';

import {gql} from "@apollo/client";
import {ROUTES} from '../../../routes';

import {validationSchemas, _SOCIOLECT} from "../form/LexicalSenseEditorConst";
import StepSociolect from '../form/StepSociolect';
import {LinkEdit} from "./LinkEdit";



export function EditLexicalSenseSociolects(props) {
  return (
    <LinkEdit
      {...props}
      templateTitle="Modifier un argot"
      gqlQuery={gqlLexicalSenseQuery}
      gqlConnectionPath="sociolects.edges"
      gqlDelete={gqlDeleteLexicalSense}
      gqlUpdate={gqlUpdateSociolect}
      fieldnameForDelete="sociolectId"
      enableDelete={true}
      allowAnyOneToDeleteLink_until_reification={true}
      noDataText="Cette définition n'a pas encore d'argot"
      itemToString={(selectedItem) => selectedItem.prefLabel}
      addLabel="un argot"
      toStep={_SOCIOLECT}
      enableLocalAdd={true}
      enableEdit={false}
      fieldnameForEdit="sociolect"
      formikValidationSchema={validationSchemas._SOCIOLECT}
      formikRenderStep={(persistedData) => <StepSociolect title="Modifier un argot" persistedData={persistedData} />}
    />
  )
}



const gqlLexicalSenseQuery = gql`
  query LexicalSenseSociolects_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      canUpdate
      sociolects {
        edges {
          node {
            id
            prefLabel
            definition
          }          
        }        
      }       
    }
  }
`;


const gqlUpdateSociolect = gql`
 mutation UpdateLexicalSense_sociolect(
    $lexicalSenseId: ID!,
    $writtenRep: String!,
    $sociolectId: String!, 
  ) {
    updateLexicalSense(input: {
      objectId: $lexicalSenseId,       
      writtenRep: $writtenRep,
      sociolectId: $sociolectId
    }) {
      updatedObject {
        id
         
      }
    }
  }
`;


const gqlDeleteLexicalSense = gql`
  mutation DeleteLexicalSense_sociolect( $lexicalSenseId: ID!,  $writtenRep: String!, $sociolectId: String! ) {
    updateLexicalSense(
      input: {
        objectId: $lexicalSenseId,
        writtenRep: $writtenRep,
        sociolectIdToDelete : $sociolectId
      }
    ) {
      updatedObject {
        id      
       }
    }
  }
 `;