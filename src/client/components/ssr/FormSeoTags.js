import React from 'react';
import {useQuery, gql} from "@apollo/client";
import {getFormSearchMetaSEO} from "../Form/utils/getFormSearchMetaSEO";
import {gqlLexicalSenseCountriesFragment} from "../../utilities/helpers/countries";

export const gqlFormSeoQuery = gql`
  query Form_Query($formQs: String) {
    lexicalSensesCount: lexicalSensesCountForAccurateWrittenForm(formQs: $formQs)
    lexicalSenses: lexicalSensesForAccurateWrittenForm(formQs: $formQs, first: 1, filterByPlaceId: "geonames:3017382") {
      edges {
        node {
          id
          definition
          lexicographicResourceName
          lexicalEntryGrammaticalPropertyLabels
          semanticPropertyLabels
          ...LexicalSenseCountriesFragment
        }
      }
    }
  }

  ${gqlLexicalSenseCountriesFragment}
`;

export function FormSeoTags({formQuery, url} = {}) {
  const {data : {lexicalSensesCount, lexicalSenses} = {}, loading} = useQuery(gqlFormSeoQuery,  {
    variables: {
      formQs: formQuery
    }
  });

  return getFormSearchMetaSEO({url, formQuery, lexicalSensesCount, lexicalSenses});
}
