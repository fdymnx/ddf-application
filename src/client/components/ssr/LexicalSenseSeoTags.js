import React from 'react';
import {useQuery, gql} from "@apollo/client";
import {gqlLexicalSenseCountriesFragment} from "../../utilities/helpers/countries";
import {getLexicalSenseMetaSEO} from "../LexicalSense/utils/getLexicalSenseMetaSEO";

export let gqlLexicalSense = gql`
  query LexicalSenseSEO_Query($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      definition
      lexicographicResourceName
      lexicalEntryGrammaticalPropertyLabels
      semanticPropertyLabels
      ...LexicalSenseCountriesFragment
    }
  }

  ${gqlLexicalSenseCountriesFragment}
`;

export function LexicalSenseSeoTags({lexicalSenseId, formQuery, url} = {}) {
  const {data: {lexicalSense} = {}, loading} = useQuery(gqlLexicalSense, {
    variables: {
      lexicalSenseId
    }
  });

  return getLexicalSenseMetaSEO({lexicalSense, formQuery, url});
}
