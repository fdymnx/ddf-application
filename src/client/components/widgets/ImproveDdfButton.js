import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import queryString from 'query-string';
import {Link} from 'react-router-dom';
import {ROUTES} from '../../routes';


import arcCercle from "../../assets/images/arc_cercle.png";
import ddfWhiteLogo from '../../assets/images/ddf_logo_white.svg';
import plusButtonIcon from '../../assets/images/circled_plus_white.svg';
import {useIsReadOnly} from '../../hooks/useIsReadOnly';
import HtmlTooltip from "./HtmlTooltip";
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({

  container: {
    display: "block",
    height: "60px",
    position: "relative",
    overflow: "hidden",
    backgroundImage: `url(${arcCercle})`,
    backgroundSize: "100% 100%",
    fontSize: Config.fontSizes.medium,
    fontFamily: "Raleway",
    fontWeight: Config.fontWeights.bold,
    color: Config.colors.white,
    userSelect: 'none',
    cursor: 'pointer',
    '&:active': {
      filter: 'brightness(80%)'
    }
  },

  img: {
    height: '100%'
  },
  ddfLogo: {
    position: "relative",
    top: "-4px",
    marginLeft: Config.spacings.small,
    marginRight: Config.spacings.small,
    height: "18px"
  },
  plusButton: {
    position: "relative",
    top: "-4px",
    height: "30px"
  },
  textLine: {
    marginTop: "25px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "& span": {
      flexShrink: 0
    }
  }
}));



ImproveDdfButton.propTypes = {
  currentForm: PropTypes.string,
};

export function ImproveDdfButton({currentForm}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const isReadOnly = useIsReadOnly();
  if (isReadOnly) {
    return null;
  }
  return (
    <HtmlTooltip title={t("FOOTER.IMPROVE_THE_DDF_TOOLTIP")}>
      <Link role="link" aria-label={t("FOOTER.IMPROVE_THE_DDF_TOOLTIP")}
        className={classes.container}
        to={{
          pathname: ROUTES.CREATE_LEXICAL_SENSE,
          search: currentForm && queryString.stringify({form: currentForm}),
        }}
      >
        <div className={classes.textLine}>
          <span>{t('FOOTER.IMPROVE_THE_DDF')}</span>
          <div className={classes.ddfLogo}>
            <img src={ddfWhiteLogo} alt="DDF" style={{fontSize: '20px'}} className={classes.img} />
          </div>
          <div className={classes.plusButton}>
            <img src={plusButtonIcon} alt="Enrichir" className={classes.img} />
          </div>
        </div>
      </Link>
    </HtmlTooltip>
  );
}
