import React from 'react';
import PinkChip from '../widgets/PinkChip';

import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import {formatRoute} from 'react-router-named-routes';
import {ROUTES} from '../../routes';
import {getTypeFromId, getTypeFromSchemes} from "../../utilities/helpers/tools";

const useStyles = makeStyles((theme) => ({
  chips: {
    cursor: "pointer !important",
    fontStyle: "italic"
  }
}));

// return a list of chips 
export function ChipsList({list, showDeleteIcon, setConfirmToDelete}) {
  const classes = useStyles();

  if (list.length === 0) return null;

  return list.map((item, index) => {
    if (item?.enableDelete && showDeleteIcon && !!setConfirmToDelete) {
      return (<PinkChip key={index} label={item?.prefLabel}
        isMine={item?.canUpdate === true}
        onDelete={() => {setConfirmToDelete(item)}} />)
    } else {

      const type = getTypeFromSchemes(item) || getTypeFromId(item?.id);

      if (item?.defId) {
        return renderLink({index, label: item.prefLabel, to: formatRoute(ROUTES.FORM_LEXICAL_SENSE, {formQuery: item.writtenRep, lexicalSenseId: item.defId})});
      } else if (type) {
        return renderLink({index, label: item.prefLabel, to: formatRoute(ROUTES.EXPLORE_CONCEPT_FORM, {type, id: item.id})});
      } else {
        return <PinkChip key={index} label={item?.prefLabel} />
      }
    }
  });



  function renderLink({index, label, to}) {
    return <Link
      key={index} role="link"
      className={classes.chips}
      aria-label={"Lancer une recherche sur " + label}
      to={to}
    >
      <PinkChip label={label} />
    </Link>
  }

}





