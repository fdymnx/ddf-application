
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  greySmallText: {
    color: Config.colors.verydarkgray,
    fontSize: Config.fontSizes.small
  }
}));
export function PersonAccountLabel({userAccount}) {
  const classes = useStyles();
  return <div >
    {userAccount?.userGroups?.edges.map(({node: userGroup}, indexGroup) =>
      <span key={indexGroup} className={classes.greySmallText}>{userGroup.label || userGroup.id} </span>
    )}
  </div>


}