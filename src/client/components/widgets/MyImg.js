import React from "react";


import ddfLogo from '../../assets/images/ddf_logo.svg';

import minus from '../../assets/images/signs/minus.svg';
import minusPink from '../../assets/images/signs/minus_pink.svg';
import minusWhite from '../../assets/images/signs/minus_white.svg';

import exclamation from '../../assets/images/signs/exclamation.svg';
import exclamationPink from '../../assets/images/signs/exclamation_pink.svg';
import exclamationOrange from '../../assets/images/signs/exclamation_orange.svg';
import exclamationWhite from '../../assets/images/signs/exclamation_white.svg';

import checkmark from '../../assets/images/signs/checkmark.svg';
import checkmarkPurple from '../../assets/images/signs/checkmark_purple.svg';
import checkmarkGreen from '../../assets/images/signs/checkmark_green.svg';
import checkmarkWhite from '../../assets/images/signs/checkmark_white.svg';

import remove from '../../assets/images/signs/remove.svg';
import removeWhite from '../../assets/images/signs/remove_white.svg';
import removeBlack from '../../assets/images/signs/remove_black.svg';

const svgs = {
  ddfLogo: {
    blank: ddfLogo,
  },
  minus: {
    blank: minus,
    pink: minusPink,
    white: minusWhite
  },
  checkmark: {
    blank: checkmark,
    purple: checkmarkPurple,
    green: checkmarkGreen,
    white: checkmarkWhite
  },
  exclamation: {
    blank: exclamation,
    orange: exclamationOrange,
    pink: exclamationPink,
    white: exclamationWhite
  },
  remove: {
    blank: remove,
    white: removeWhite,
    black: removeBlack
  },
};





/**
 * icon à partir d'un svg
 * remplit 100 % du parent
 * @param {*} param0
 * NEED TO INSTALL @svgr/webpack to work
 * and in webpack 
 * 
 * {
      test: /\.svg$/,
      use: ['@svgr/webpack', 'url-loader'],
    }    
 */

export default function MyImg({iconName, color = "white", ...props}) {

  const icon = svgs?.[iconName]?.[color] || svgs?.[iconName]?.['white'];

  const style = {
    width: "100%",
    height: "100%"
  };

  return (
    <div style={style}>
      <img src={icon} alt="Action" width="100%" height="100%" {...props} />
    </div>
  );
}
