import React from 'react';

import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  fontSize: Config.fontSizes.xxlarge,
  color: Config.colors.purple,
  fontWeight: Config.fontWeights.semibold,

  container: {
    display: "flex",
    justifyContent: "space-between",
    height: "35px"
  },
  link: {
    color: Config.colors.darkgray,
    fontSize: Config.fontSizes.small,
    flexGrow: "1",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",

    '&.active': {
      color: Config.colors.purple,
      fontWeight: Config.fontWeights.bold,
      borderBottom: `2px solid ${Config.colors.purple}`,       
    }
  }

}));




BannerMenu.propTypes = {
  /**
   * It's the same object format that the one used by DesktopSideMenu
   */
  entries: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired,
    })
  ),
};

export function BannerMenu({entries}) {

  const classes = useStyles();
  return (
    <nav className={classes.container}>
      {entries.map((entry, idx) => (
        <NavLink key={idx} className={classes.link} activeClassName={"active"} to={entry.to}>
          {entry.text}
        </NavLink>
      ))}
    </nav>
  );
}


