import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  

  container: {
    backgroundColor: Config.colors.white,
    paddingBottom: Config.spacings.large
  },
  title: {
    padding: Config.spacings.medium,
    color: Config.colors.pink,
    fontWeight: Config.fontWeights.light,
  },
  link: {
    paddingLeft: Config.spacings.medium,
    paddingRight: Config.spacings.medium,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny,
    display: "block",
    color: Config.colors.black,
    textDecoration: "none",
    "&.active": {
      color: Config.colors.white,
      backgroundColor: Config.colors.pink,
      fontWeight: Config.fontWeights.semibold,
    }
  }
}));


DesktopSideMenu.propTypes = {
  /**
   * It's the same object format that the one used by BannerMenu
   */
  entries: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired
  })),
  title: PropTypes.string
};

export function DesktopSideMenu(props) {
  const classes = useStyles();
  const {entries, title} = props;

  return (
    <div className={classes.container}>
      <div className={classes.title}>
        {title}
      </div>
      {entries.map((entry, idx) => (
        <NavLink
          key={idx}
          className={classes.link}
          activeClassName={"active"}
          to={entry.to}
        >
          {entry.text}
        </NavLink>
      ))}
    </div>
  );
}

