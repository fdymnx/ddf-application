/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { useField } from "formik";
import {Select, InputLabel, FormControl, MenuItem} from "@material-ui/core";

export const SelectField = ({label, value, name, inputProps, options, ...props} = {}) => {
  const [field, meta] = useField({name, ...props});

  return (
    <FormControl>
      <InputLabel>{label}</InputLabel>
      <Select name={name} {...field} style={{minWidth: "100px"}}>
        {(options || []).map(({label, value}) => (
          <MenuItem value={value} key={value}>
            {label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};
