import React from 'react';
import PropTypes from 'prop-types';
import {TextField as MuiTextField} from "formik-material-ui";
import {makeStyles} from '@material-ui/core/styles';
import {Field} from "formik";
import Config from '../../../Config';



const useStyles = makeStyles((theme) => ({
  border: {
    backgroundColor: Config.colors.white,
    color: Config.colors.darkpurple,
    minHeight: "50px",
    display: "flex",
    alignItems: "center",
    justifyContent: 'center',
    width: "100%",
    borderRadius: "15px",
    padding: '5px 15px',
    '& div': {
      '& textarea': {
        color: Config.colors.darkpurple,
      },
      '& input': {
        color: Config.colors.darkpurple,
      }
    },
  }
}));




export function TextField(props) {
  const classes = useStyles();

  return <Field
    fullWidth
    id={props.name}
    component={MuiTextField}
    className={classes.border}
    InputProps={{disableUnderline: true}}
    {...props}
    label=""
  />
}

TextField.propTypes = {
  name: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired
};

