
import React from 'react';
import PropTypes from 'prop-types';

import {TextField} from "./TextField";

export function TextAreaField(props) {

  return <TextField id={props.name} multiline rows={5}  {...props} />

};



TextAreaField.propTypes = {
  name: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired
};


/*import React from 'react';
import PropTypes from 'prop-types';
import {useField} from "formik";
import TextField from "@material-ui/core/TextField";

export function TextAreaField(props) {
  const [field, meta] = useField(props);

  if (field && field.value === null) {
    field.value = "";
  }
  return (
    <div style={{display: "flex", flexDirection: "column"}}>
      <TextField id={props.name} multiline rows={5} {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="MuiFormHelperText-root" style={{color: "red"}}>
          {meta.error}
        </div>
      ) : null}
    </div>
  );
};



TextAreaField.propTypes = {
  name: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired
};
*/
