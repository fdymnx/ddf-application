import React, {useState, useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';
import {finalize} from 'rxjs/operators';

import {makeStyles} from '@material-ui/core/styles';
import {useMediaQueries} from '../../layouts/MediaQueries';
import {getGeolocService} from '../../services/GeolocService';
import {usePlaceContext} from '../../hooks/PlaceContext';
import CircularProgress from "@material-ui/core/CircularProgress";
import geolocIconWhite from '../../assets/images/geoloc_white.svg';
import geolocIconPurple from '../../assets/images/geoloc_purple.svg';
import geolocMarkerWhiteIcon from '../../assets/images/geoloc_marker_white.svg';
import geolocMarkerGreyIcon from '../../assets/images/geoloc_marker_grey.svg';

import clsx from "clsx";
import {PlaceAutocompleteMUIA} from './PlaceAutocompleteMUIA';

import DivButton from './DivButton';
import Grid from '@material-ui/core/Grid';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: '50px',// same as FormSearchInput
    borderRadius: "50px",
    backgroundColor: Config.colors.mediumgray,
    color: Config.colors.purple,
  },
  containerGeolocated: {
    backgroundColor: Config.colors.purple,
    color: `${Config.colors.white} ! important`,
    '& input': {
      color: `${Config.colors.white} ! important`,
    }
  },
  purlpleBg: {
    backgroundColor: Config.colors.purple
  },
  darkBg: {
    backgroundColor: Config.colors.darkgray
  },
  autoGeoloc: {
    color: Config.colors.purple,
    fontSize: Config.fontSizes.small,
    display: "flex",
    alignItems: "center",
  },
  markerIcon: {
    height: "20px",
    width: '50px', // same as FormSearchInput
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  markerIconImg: {
    height: "100%"
  },
  geolocIconImg: {
    height: "100%"
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  spacer: {
    margin: "15px"
  },
  marginLR: {
    margin: "0px 15px"
  }
}));


GeolocPicker.propTypes = {
  geolocService: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object
  ]),
  notificationService: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.object
  ]),
  closeGeolocParentModal: PropTypes.func,
  onChange: PropTypes.func,
  displayInColumn: PropTypes.bool
};


/**
 * This component display an input Autocompleted for Geolocalisation and a search button
 * 
 * by default any change are saved in usePlaceContext and the place is used by all the application
 * 
 * but this behavior can be disabled and a onChange function are called instead of saving in usePlaceContext
 * this is used to return a selected place while creating a new contribution for example
 * 
 * @param {fct} closeGeolocParentModal : callback to close parent Modal in mobile mode 
 * @param {fct} onChange : callback who also disable saving place in usePlaceContext
 * @param {fct} initialPlace :can force initial place insted of reading them from usePlaceContext
 * //, this is used for form creation when user click on step back from step 4 to step 3
 * 
 * @returns 
 */
export function GeolocPicker({notificationService, closeGeolocParentModal = false, onChange = false, initialPlace = null, ...props}) {

  const {t} = useTranslation();
  const classes = useStyles();
  const {isMobile} = useMediaQueries();


  let _onChange = false;
  const displayInColumn = props?.displayInColumn === true;

  let geolocService = props.geolocService || getGeolocService(!!onChange ? {setCurrentPlace: _onChange} : {});
  let currentPlace, setCurrentPlace;

  if (!!onChange) {
    // read currentPlace from usePlaceContext even if not needed or i get :
    // Warning: React has detected a change in the order of Hooks called by GeolocPicker. 
    let currentPlaceTmp = usePlaceContext().currentPlace;
    ([currentPlace, setCurrentPlace] = useState(initialPlace || currentPlaceTmp));

    // if we use onChange to prevent usePlaceContext to be used. we overide it
    _onChange = (newValue) => {
      setCurrentPlace(newValue);
      onChange(newValue)
    }

  } else {
    ({currentPlace, setCurrentPlace} = usePlaceContext())
  }

  useEffect(() => {
    if (!!onChange && currentPlace) { // at component init, set formik field value
      onChange(currentPlace)
    }
  }, []);

  const [geolocated, setGeolocated] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const [autoGeolocIsLoading, setAutoGeolocIsLoading] = useState(false);
  const containerRef = useRef(null);


  /**
   * just initializing geolocated don't work because it's not updated on currentPlace change
   * const [geolocated, setGeolocated] = useState(!!currentPlace?.id);
   */
  useEffect(() => {
    setGeolocated(!!currentPlace?.id);
    setInputValue(currentPlace?.name);
  }, [currentPlace]);

  if (!displayInColumn) {
    return (
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        spacing={2}
      >
        <Grid item xs={isMobile ? 12 : 10} lg={isMobile ? 12 : 7}>
          {renderInputAC()}
        </Grid>
        {!isMobile &&
          <Grid item xs={2} lg={5}>
            {renderGeolocButton(geolocIconPurple)}
          </Grid>
        }
      </Grid>
    )
  } else {
    return <div className={classes.center}>
      {renderInputAC()}
      {!isMobile &&
        <>
          <div className={classes.spacer} />
          {renderGeolocButton(geolocIconPurple)}
        </>
      }
    </div>
  }

  function renderGeolocButton(imgSrc) {
    return <DivButton data-testid="autoGeolocButton" onClick={setAutoPlace} aria-label={t('INDEX.AUTO_GEOLOC')}    >
      <div className={classes.autoGeoloc}>
        <span className={classes.markerIcon}>
          <img src={imgSrc} alt="Géolocalisation" className={classes.geolocIconImg} />
        </span>
        {!isMobile && <span>{t('INDEX.AUTO_GEOLOC')}</span>}
        {autoGeolocIsLoading && <div data-testid="autoGeolocLoading">
          <CircularProgress size={20} className={classes.marginLR} />
        </div>}
      </div>
    </DivButton>
  }

  function renderInputAC() {
    return <div className={clsx(classes.container, geolocated && classes.containerGeolocated)} role="search" ref={containerRef} >
      <PlaceAutocompleteMUIA
        showIsRequired={false}
        placeholder={"Choisir une localisation"}
        handleChange={(newObj) => {
          setInputValue(newObj?.label || "");
          setGeolocated(true);
          if (newObj?.id) {
            if (_onChange) {
              _onChange(newObj);
            }
            setCurrentPlace(newObj);
          }
        }}
        handleSubmit={onSubmit}
        value={inputValue}
        inputRef={null}
        containerRef={containerRef}
        startAdornment={
          <div className={classes.markerIcon} onClick={() => closeGeolocParentModal && closeGeolocParentModal()} >
            <img alt="Géolocalisation" src={geolocated ? geolocMarkerWhiteIcon : geolocMarkerGreyIcon} className={classes.markerIconImg} />
          </div>
        }
        endAdornment={isMobile && renderGeolocButton(geolocated ? geolocIconWhite : geolocIconPurple)}
      />
    </div>
  }

  function onSubmit(a) {
    closeGeolocParentModal && closeGeolocParentModal();
  }

  function setAutoPlace() {
    if (autoGeolocIsLoading) return;

    setAutoGeolocIsLoading(true);

    geolocService.setAutoPlace()
      .pipe(finalize(() => setAutoGeolocIsLoading(false)))
      .subscribe({
        error: handleAutoGeolocError,
      });
  }

  function handleAutoGeolocError(error) {
    console.warn('geolocService.setAutoPlace() emitted the following error', error);
    let alertMsg = '';

    if (error.status == 'browser_geolocation_error' && error.error.code == 1) {
      alertMsg = t('GEOLOC.ERRORS.PERMISSION_DENIED');
    } else if (error.status != 'aborted') {
      alertMsg = t('GEOLOC.ERRORS.UNKNOWN');
    }

    alertMsg && notificationService && notificationService.alert(alertMsg, {type: 'error'});
  }


}
