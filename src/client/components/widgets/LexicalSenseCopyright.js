import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Config from '../../Config';
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: Config.colors.white,
    padding: Config.spacings.medium,
    fontSize: Config.fontSizes.medium,
    color: Config.colors.verydarkgray
  },
  marginTop: {
    marginTop: "12px"
  }
}));


export function LexicalSenseCopyright({addMarginTop = false}) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, addMarginTop && classes.marginTop)}>
      Note : Les définitions et les exemples que vous allez ajouter seront publiés sous licence CC BY-SA 3.0 et rattachés à votre compte utilisateur.
      Les indications complémentaires (de registre, de domaine, etc.) seront placées sous licence CC-0 et non rattachées aux comptes utilisateurs
    </div>
  )
}