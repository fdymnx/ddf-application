import React, {useState, useEffect, useRef, forwardRef, useImperativeHandle} from 'react';
import PropTypes from 'prop-types';
import clsx from "clsx";
import {makeStyles} from "@material-ui/core/styles";
import searchIcon from '../../assets/images/signs/search.svg';
import searchIconPurple from '../../assets/images/signs/search_purple.svg';
import requiredIcon from '../../assets/images/signs/exclamation_required.svg';
import {FormSearchInputAutocomplete} from "./FormSearchInputAutocomplete";


import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  fontSize: Config.fontSizes.xxlarge,
  color: Config.colors.purple,
  fontWeight: Config.fontWeights.semibold,
  marginBottom: Config.spacings.medium,


  rightHandPlaceholder: {
    width: Config.spacings.medium
  },
  requiredIcon: {
    flexShrink: 0,
    color: Config.colors.yellow
  },
  flexShrinkPointer: {
    flexShrink: 0,
    cursor: "pointer"
  },

  container: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    borderRadius: "99999px", /* arbitrary large value */
    backgroundColor: Config.colors.lightgray,
    /* Dimension related rules */
    "&.medium": {
      height: "40px",
      "& form input": {
        fontSize: Config.fontSizes.medium
      },
      "&.text-size-small form input": {
        fontSize: Config.fontSizes.small
      },
      "& .searchIcon, .closeIcon , .requiredIcon": {
        width: "40px",
        height: "40px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
      },
      "& .searchIcon img": {
        width: `${0.45 * 40}px`,
        height: `${0.45 * 40}px`
      },
      "& .closeIcon img , .requiredIcon img": {
        width: `${0.60 * 40}px`,
        height: `${0.60 * 40}px`,
      }
    },
    "&.large": {
      height: "50px",

      "& form input": {
        fontSize: Config.fontSizes.large
      },
      "& .searchIcon, .closeIcon ,.requiredIcon": {
        width: "50px",
        height: "50px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
      },
      "& .searchIcon img": {
        width: `${0.45 * 50}px`,
        height: `${0.45 * 50}px`
      },
      "& .closeIcon img , .requiredIcon img": {
        width: `${0.60 * 50}px`,
        height: `${0.60 * 50}px`,
      }
    },
    /* Theme related rules */
    "&.dark": {
      backgroundColor: Config.colors.lightgray,
      "& form input": {
        color: Config.colors.black
      }
    },
    "&.light": {
      backgroundColor: Config.colors.white,
      "& form input": {
        color: Config.colors.black
      }
    },
    "&.purple": {
      backgroundColor: Config.colors.darkpurple,
      "& form input": {
        color: Config.colors.white
      }
    },
  },

}));



/**
 *
 * Usage :
 *
 * <FormSearchInput
 *  theme='dark'
 *  size='large'
 *  autofocus={false}
 *  searchResetRef={searchResetRef}
 * />
 *
 *
 * Properties :
 *
 * - theme : 'dark'|'light'|'purple' (default)
 * - size  : 'large'(default)|'medium'
 * - autofocus : true (default)|false
 * - ref created with useRef() in parent for calling resetInput() from parent directly  ;
 * WARNING changing 'ref' param name don't work, don't change it
 */
const FormSearchInput = forwardRef((props, ref) => {
  const classes = useStyles();
  const inputRef = useRef(null);
  const containerRef = useRef(null);
  const [value, setValue] = useState(props.value?.label || '');
  const theme = props.theme || 'light';
  const size = props.size || 'large';
  const [textSize, setTextSize] = useState('normal');
  const [autofocus] = useState(props.autofocus !== false);

  // for calling resetInput from parent
  useImperativeHandle(ref, () => ({
    resetInput({inputValue} = {}) {
      setValue(inputValue || '');
      if (autofocus) {
        //  setTimeout(() => inputRef?.current.focus(), 100);
      }
    },
  }));

  useEffect(() => {
    if (autofocus) {
      inputRef?.current?.focus && inputRef?.current.focus();
    }
  }, [autofocus]);


  return (
    <div className={clsx(classes.container, theme, size, `text-size-${textSize}`)} role="search" id={"containerRef"} ref={containerRef} >
      <FormSearchInputAutocomplete
        showIsRequired={props?.showIsRequired}
        placeholder={props.placeholder}
        addForm={props?.addForm}
        handleSubmit={handleSubmit}
        handleChange={handleChange}
        value={value}
        inputRef={inputRef}
        containerRef={containerRef}
        startAdornment={<div className={clsx("searchIcon", classes.flexShrinkPointer)} role="button" aria-label="Recherche la chaine saisie" onClick={handleSubmit}>
          <img src={theme !== "purple" ? searchIconPurple : searchIcon} alt="Recherche" />
        </div>}
      />

      {props?.showIsRequired &&
        <div className={clsx("requiredIcon", classes.requiredIcon)} aria-label="Veillez saisir le terme recherché" role="alert">
          <img src={requiredIcon} alt="Requis" />
        </div>
      }

      <Choose>
        <When condition={props.closeButton}>
          <div role="button"
            aria-label={"Supprimer la recherche"}
            className={clsx("closeIcon", classes.flexShrinkPointer)} onClick={handleOnClose}>
            &#x2715;
          </div>
        </When>
        <Otherwise>
          <div className={classes.rightHandPlaceholder} />
        </Otherwise>
      </Choose>
    </div>
  );

  function handleChange(inputValue) {
    let textSize = inputValue?.length > 20 ? 'small' : 'normal';
    setValue(inputValue?.label);
    setTextSize(textSize);

    if (props.onChange) {
      props.onChange(inputValue);
    }
  }

  function handleSubmit(evt) {
    evt?.preventDefault && evt.preventDefault();
    if (props.onSubmit) {
      props.onSubmit();
    }
  }

  function handleOnClose() {
    if (props.onClose) {
      props.onClose();
      setValue('');
    }
  }
});


FormSearchInput.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func,
  theme: PropTypes.oneOf(['dark', 'light', 'purple']),
  size: PropTypes.oneOf(['large', 'medium']),
  closeButton: PropTypes.bool,
  showIsRequired: PropTypes.bool,
  autofocus: PropTypes.bool,
};

export default FormSearchInput;

