import React, {useState} from 'react';
import Config from '../../Config';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


import DivButton from './DivButton';
import logoTablette from '../../assets/images/tablette.svg';
import AppleStore from '../../assets/images/appstore.png';
import GooglePlay from '../../assets/images/googleplay.png';
import {isAndroid, isIOS} from "react-device-detect"; //isBrowser, isMobile, isMacOs,, osName, osVersion
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  storesSectionContainer: {
    marginTop: '10px',
    height: `${Config.heights.footerStoresLinks}px`,
    backgroundColor: Config.colors.white,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  storesSection: {
    maxWidth: '600px',
    paddingTop: `${Config.spacings.tiny}px`,
    paddingBottom: `${Config.spacings.tiny}px`,
    display: "flex",
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
  },
  text: {
    "textDecoration": "none",
    "fontFamily": "Raleway",
    "color": `${Config.colors.purple}`,
    "fontSize": `${Config.fontSizes.sm}`
  },
  cursorPointer: {
    cursor: "pointer"
  },
  logoTablette: {
    marginRight: `${Config.spacings.medium}`,
    height: "75px"
  },
  logoStore: {
    height: "50px",
  },
  center: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  }
}));

const googleUrl = "https://play.google.com/store/apps/details?id=fr.gouv.culture.dicofr&hl=fr"
const appleUrl = "https://apps.apple.com/fr/app/dictionnaire-des-francophones/id1498941772"


export function StoresSectionMobile() {

  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  function renderLinks() {
    if (isAndroid) {
      return <a href={googleUrl} aria-label="Installer l'application Android" target='_blank'>
        Application pour Android
      </a>
    } else if (isIOS) {
      <a href={appleUrl} aria-label="Installer l'application Iphone et Ipad" target='_blank'>
        Application pour Iphone et Ipad
      </a>
    } else {
      return <DivButton
        aria-label="Application pour mobile et tablette"
        onClick={handleClickOpen} >Application pour mobile et tablette
      </DivButton>

    }

  }

  function renderDialog() {
    return <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Applications mobiles et tablettes</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Retrouvez le Dictionnaire des francophones tout le temps et n'importe où.
        </DialogContentText>
        <div className={classes.center}>
          <a href={googleUrl} aria-label="installer l'application Android" target='_blank'>
            <img className={classes.logoStore} src={GooglePlay} alt="installer l'application Android" />
          </a>
          <a href={appleUrl} aria-label="installer l'application Iphone et Ipad" target='_blank'>
            <img className={classes.logoStore} src={AppleStore} alt="installer l'application Iphone et Ipad" />
          </a>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary" autoFocus>
          Fermer
        </Button>
      </DialogActions>
    </Dialog>

  }

  return (
    <>
      {renderLinks()}
      {renderDialog()}
    </>
  )
}


export function StoresSectionDesktop({classesName}) {

  const classes = useStyles();

  const [showLinks, setShowLinks] = useState(false);

  return <div className={clsx(classes.storesSectionContainer, classes.cursorPointer, classesName)} onClick={() => setShowLinks(!showLinks)}>
    <div className={classes.storesSection} aria-label="installer l'application sur votre Android ou Apple">
      <img className={classes.logoTablette} src={logoTablette} alt="installer l'application sur votre Android ou Apple" />
      {!showLinks ?
        <div className={classes.text} >
          Retrouvez le Dictionnaire des francophones tout le temps et n'importe où grâce à nos <b>applications mobiles et tablettes</b>.
        </div>
        : (
          <>
            <a href={googleUrl} aria-label="installer l'application Android" target='_blank'>
              <img className={classes.logoStore} src={GooglePlay} alt="installer l'application Android" />
            </a>
            <a href={appleUrl} aria-label="installer l'application Iphone et Ipad" target='_blank'>
              <img className={classes.logoStore} src={AppleStore} alt="installer l'application Iphone et Ipad" />
            </a>
          </>
        )}
    </div>
  </div>
}