import Config from '../../../Config';
import {makeStyles} from '@material-ui/core/styles';


let overrideInputAutoFillStyling = {
  "&:-webkit-autofill,  &:-webkit-autofill:hover,   &:-webkit-autofill:focus,   &:-webkit-autofill:active": {
    "WebkitBoxShadow": `0 0 0 30px ${Config.colors.mediumgray} inset !important`,
    "WebkitTextFillColor": `${Config.colors.black} !important`
  },
}
/**
 * TODO Autocomplete need to be remove and use Autocomplete from Material ui Instead like for PersonAutocompleteMUIA
 * 
 * for now some css classes need to be targeted directly as autosuggestContainer child className (like input, .react-autosuggest__suggestions-container and  .react-autosuggest__suggestion )
 * but they are also used alone with className={FormStyle.input} in others components
 * so some style are duplicated here
 * 
   input,
   suggestion,
   suggestionsContainer,
   autosuggestContainer: {
    position: "relative",
    color: Config.colors.black,
    "& input": input,
    "& .react-autosuggest__suggestions-container": suggestionsContainer,
    "& .react-autosuggest__suggestion": suggestion
  },
 * 
 */
const input = {
  display: "block",
  width: "100%",
  paddingLeft: Config.spacings.small,
  paddingRight: Config.spacings.small,
  paddingTop: Config.spacings.tiny,
  paddingBottom: Config.spacings.tiny,
  marginTop: Config.spacings.tiny,
  marginBottom: Config.spacings.tiny,
  border: "none",
  fontSize: Config.fontSizes.small,
  fontFamily: "Lato",
  backgroundColor: Config.colors.white,
  color: Config.colors.darkgray,
  "&:focus": {
    outline: "none",
  },
  "&.purple": {
    backgroundColor: Config.colors.purple
  },
  "& .white": {
    backgroundColor: Config.colors.white
  },
  "&:required": {
    "&::selection": {
      background: Config.colors.mediumgray
    },
    "&::placeholder": {
      color: "#000000"
    }
  },
  height: "35px",
  borderRadius: "99999px", /* arbitrary large value */
  ...overrideInputAutoFillStyling,

  "&:required": {
    ...overrideInputAutoFillStyling,
  }

};

const suggestionsContainer = {
  position: "absolute",
  width: "100%",
  marginTop: Config.spacings.tiny,
  borderRadius: "15px",
  backgroundColor: Config.colors.mediumgray,
  overflow: "hidden",
  zIndex: 2,
  "& ul": {
    listStyle: "none"
  }
};
const suggestion = {
  paddingLeft: Config.spacings.small,
  paddingRight: Config.spacings.small,
  fontSize: Config.fontSizes.small,
  color: Config.colors.darkgray,
  "& > div": {
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
  },
  "&:hover": {
    cursor: "pointer"
  },
  "&.noResults": {
    cursor: "initial",
    fontStyle: "italic"
  }
};

const formStyles = makeStyles((theme) => ({

  info: {
    fontSize: Config.fontSizes.small,
    marginBottom: Config.spacings.small,
    marginTop: Config.spacings.small,

    "&:first-child": {
      marginTop: 0
    },
    "&.secondary": {
      fontSize: Config.fontSizes.xsmall,
      color: Config.colors.darkgray
    }
  },
  inputErrorMessage: {
    fontSize: Config.fontSizes.small,
    color: Config.colors.pink,
    marginLeft: Config.spacings.tiny
  },

  input: {
    ...input,
    display: "inline-flex",
    position: "relative",
  },

  textarea: {
    display: "block",
    width: "100%",
    marginTop: Config.spacings.small,
    marginBottom: Config.spacings.small,
    paddingLeft: Config.spacings.small,
    paddingRight: Config.spacings.small,
    paddingTop: Config.spacings.tiny,
    paddingBottom: Config.spacings.tiny,
    border: "none",
    fontSize: Config.fontSizes.small,
    fontFamily: "Lato",
    backgroundColor: Config.colors.white,
    color: Config.colors.darkgray,
    "&:focus": {
      outline: "none",
    },
    "&:required": {
      "&::selection": {
        background: Config.colors.mediumgray
      },
      "&::placeholder": {
        color: "#000000"
      }
    },
    /* 
    * 25 is approximation of input text default height with current font settings, in order to have
    * the same border radius for textareas and inputs  
    *
    */
    borderRadius: "12.5px",
    resize: "none",
    /*
     * Height to have 3 lines of text with current font settings 
     */
    height: "60px",
    "&:required": {
      ...overrideInputAutoFillStyling,
    }
  },

  autosuggestContainer: {
    position: "relative",
    color: Config.colors.black,
    "& input": input,
    "& .react-autosuggest__suggestions-container": suggestionsContainer,
    "& .react-autosuggest__suggestion": suggestion
  },
  suggestionsContainer,
  suggestion,

  checkboxRow: {
    display: "flex",
    alignItems: "center",
    fontSize: Config.fontSizes.small,
    paddingLeft: Config.spacings.tiny,
    marginBottom: Config.spacings.small,
  },
  customCheckbox: {
    marginRight: Config.spacings.small,
    appearance: "none",
    backgroundColor: Config.colors.lightgray,
    borderStyle: "solid",
    borderColor: Config.colors.purple,
    borderWidth: "1.5px",
    boxSizing: "border-box",
    width: "20px",
    height: "20px",
    borderRadius: "2px",
    position: "relative",
    borderRadius: "2px",
    position: "relative",

    "&:checked": {
      backgroundColor: Config.colors.purple,

      "&::after": {
        content: "U+02714",
        fontSize: "14px",
        position: "absolute",
        top: "0px",
        left: "3px",
        color: Config.colors.lightgray
      }
    },
    "&:checked": {
      backgroundColor: Config.colors.purple,
      "&::after": {
        content: "U+02714",
        fontSize: "14px",
        position: "absolute",
        top: "0px",
        left: "3px",
        color: Config.colors.lightgray,
      }
    }
  },
  customRadioButton: {
    marginRight: Config.spacings.small,
    appearance: "none",
    backgroundColor: Config.colors.lightgray,
    borderStyle: "solid",
    borderColor: Config.colors.purple,
    borderWidth: "1.5px",
    boxSizing: "border-box",
    width: "20px",
    height: "20px",
    borderRadius: "99999px",
    "&:checked": {
      borderWidth: '6px'
    }
  },
  button: {
    display: "block",
    width: "100%",
    paddingTop: Config.spacings.small,
    paddingBottom: Config.spacings.small,
    border: "none",
    fontFamily: "Lato",
    fontSize: Config.fontSizes.small,
    color: Config.colors.white,
    textAlign: "center",
    marginBottom: Config.spacings.small,
    backgroundColor: props => Config.colors?.[props.color] || Config.colors.purple,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      width: "auto",
      whiteSpace: "nowrap",
      paddingLeft: Config.spacings.medium,
      paddingRight: Config.spacings.medium
    },
    "&.secondary": {
      backgroundColor: Config.colors.mediumgray,
      color: Config.colors.darkgray
    },
  }

}));

/**
 * REPLACE formsStyle with 
  import formStyles from './FormStyles.js';
  const formStyle = formStyles({color: colorTheme});
 */
export default formStyles;