import React from 'react';
import Config from '../../../Config';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    "& > *": {
      marginLeft: Config.spacings.small,
      "&:first-child": {
        marginLeft: 0
      }
    }
  }
}));


export function ButtonRow({children}) {
  const classes = useStyles();
  return <div className={classes.container}>{children}</div>;
}
