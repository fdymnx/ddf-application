import React from 'react';
import {Form as FormikForm} from 'formik';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Config from '../../../../Config';

export const useStyles = makeStyles((theme) => ({
  form: {
    flexGrow: "1",
    display: "flex",
    flexDirection: "column",

    "& > .input, & > .autosuggestContainer, & > .button, & > .textarea, & > .checkboxRow": {
      marginBottom: Config.spacings.small,
      "&:last-child": {
        marginBottom: 0
      }
    },

    "& .button:not(.secondary)": {
      backgroundColor: Config.colors.purple,
      color: Config.colors.white
    },

    "& h2": {
      color: Config.colors.purple,
      fontSize: Config.fontSizes.medium,
      fontWeight: Config.fontWeights.normal,
      marginLeft: Config.spacings.tiny,
      marginBottom: Config.spacings.tiny,
    }

  }

}));


Form.propTypes = {
  /**
   * A color theming for the elements of the form.
   * Default color is purple
   */
  colorTheme: PropTypes.string,
};
export function Form({colorTheme, ...otherProps}) {
  const classes = useStyles({color: colorTheme});
  return <FormikForm className={classes.form} {...otherProps} />;
}
