import React from 'react';
import PropTypes from 'prop-types';

import {ConceptAutocomplete} from './ConceptAutocomplete';

DomainAutocomplete.propTypes = {
  maxSuggestionsLength: PropTypes.number
};
export function DomainAutocomplete(props) {
  return (
    <ConceptAutocomplete
      schemeId="http://data.dictionnairedesfrancophones.org/authority/domain"
      {...props}
    />
  );
}

