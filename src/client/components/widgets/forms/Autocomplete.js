import React, {useState, useEffect} from 'react';
import Autosuggest from 'react-autosuggest';
import clsx from "clsx";
import PropTypes from 'prop-types'; 
import {useTranslation} from 'react-i18next';

import {useAutocompleteController} from './AutocompleteController';
import useDebounce from '../../../hooks/useDebounce';
import formStyles from './FormStyles.js';

/**
 *
 * Usage : 
 *
 * <Autocomplete 
 *    controller={controller}
 *    placeholder="Placeholder"
 * />
 *
 */
Autocomplete.propTypes = {
  /**
   * This object is meant to be an instance of AutocompleteController, or a class extending AutocompleteController, although it can be
   * another object as long as it implements the methods of AutocompleteController and uses the `this.updateSuggestions` method setup
   * by the hook `useAutocompleteController()`.
   *
   * The controller must implements all functions used by the Autosuggest component, regarding fetching, selecting and clearing suggestions :
   * onSuggestionsFetchRequested, getSuggestionValue,  onSuggestionsClearRequested, onSuggestionSelected. 
   *
   * The controller can implement optionnal rendering functions : renderSuggestion, renderSuggestionText.
   *
   */
  controller: PropTypes.shape({
    onSuggestionsFetchRequested: PropTypes.func.isRequired,
    getSuggestionValue: PropTypes.func.isRequired,
    onSuggestionsClearRequested: PropTypes.func.isRequired,
    onSuggestionSelected: PropTypes.func.isRequired,
    /**
     * Passed directly to Autosuggest's renderSuggestion prop, bypass the default rendering done by this component
     */
    renderSuggestion: PropTypes.func,
    /**
     * Transform the suggestion object into displayable text to be shown in the suggestions list. If not provided, the suggestion object
     * must natively be a string. This is assuming we use this component default suggestion rendering. If controller.renderSuggestion is provided,
     * renderSuggestionText is not needed.
     */
    renderSuggestionText: PropTypes.func,
    shouldRenderSuggestions: PropTypes.func,
    /**
     * Displays a "no results" infobox when the returned suggestions are empty
     */
    displayNoResults: PropTypes.bool
  }).isRequired,
  placeholder: PropTypes.string,
  /**
   * To control the input component inside
   */
  inputValue: PropTypes.string,
  /**
   * To control the input component inside
   */
  inputOnChange: PropTypes.func,
  /**
   * Handler bound to the internal input onBlur event
   */
  inputOnBlur: PropTypes.func,
  /**
   * Name transferred to the internal input component
   */
  inputName: PropTypes.string,
  /**
   * Default color is purple
   */
  colorTheme: PropTypes.string,
  /**
   * If this is true, the autocomplete will call controller.onSuggestionSelected with null when the 
   * input is empty. 
   */
  selectNullOnEmpty: PropTypes.bool,
  /**
   * Flag to indicate if the input is required 
   */
  isRequired: PropTypes.bool
};

export function Autocomplete({
  controller,
  placeholder,
  theme,
  colorTheme,
  selectNullOnEmpty,
  isRequired,
  ...otherProps
}) {

  const formStyle = formStyles({color: colorTheme});
  const {t} = useTranslation();
  let [inputValue, setInputValue] = useState('');
  let {suggestions, noResults} = useAutocompleteController(controller);

  let [inputSuggestionsFetchRequested, setInputSuggestionsFetchRequested] = useState({value: ""});
  // We call our hook, passing in the current inputSuggestionsFetchRequested value.
  // The hook will only return the latest value (what we passed in) if it's been more than 500ms since it was last called.
  // Otherwise, it will return the previous value of inputSuggestionsFetchRequested.
  // The goal is to only have the API call fire when user stops typing so that we aren't hitting our API rapidly.  
  const debouncedSearchTerm = useDebounce(inputSuggestionsFetchRequested, 500);
  useEffect(() => {
    if (debouncedSearchTerm) {
      controller.onSuggestionsFetchRequested(debouncedSearchTerm);
    }
  }, [debouncedSearchTerm]);

  if (placeholder && isRequired) {
    placeholder += " *";
  }

  function renderSuggestion(suggestion) {
    let suggestionText = controller.renderSuggestionText ? controller.renderSuggestionText(suggestion) : suggestion;
    return (
      <div>
        {suggestionText}
      </div>
    );
  }

  /**
   * This is a wrapper around the inputOnChangeHandler (either custom or default one), in order to detect
   * when the input is empty, and add a call to controller.onSuggestionSelected with null
   * (if the component was configured to do so with props.selectNullOnEmpty)
   */
  function inputOnChangeWrapper(e, params) {
    const {newValue} = params;
    let nextHandler = otherProps.inputOnChange || defaultInputOnChangeHandler;
    nextHandler(e, params);
    if (selectNullOnEmpty && !newValue) {
      controller.onSuggestionSelected(null, {suggestion: null})
    }
  }

  function defaultInputOnChangeHandler(e, {newValue}) {
    setInputValue(newValue);
  }

  return (
    <div className={formStyle.autosuggestContainer}>
      <Autosuggest
        suggestions={suggestions}
        // onSuggestionsFetchRequested={controller.onSuggestionsFetchRequested}
        onSuggestionsFetchRequested={setInputSuggestionsFetchRequested}
        getSuggestionValue={controller.getSuggestionValue}
        onSuggestionsClearRequested={controller.onSuggestionsClearRequested}
        onSuggestionSelected={controller.onSuggestionSelected}
        renderSuggestion={controller.renderSuggestion || renderSuggestion}
        shouldRenderSuggestions={controller.shouldRenderSuggestions}
        inputProps={
          {
            value: otherProps.inputValue || inputValue,
            onChange: inputOnChangeWrapper,
            onBlur: otherProps.inputOnBlur,
            "aria-label": placeholder,
            placeholder: placeholder,
            className: theme?.autocompleteInput,
            name: otherProps.inputName,
            required: isRequired,
            autoCapitalize: otherProps.autoCapitalize || otherProps.autoCorrect
          }
        }
        theme={theme}
      />
      <If condition={noResults && controller.displayNoResults}>
        <div className={formStyle.suggestionsContainer}>
          <ul><li className={clsx(formStyle.suggestion, "noResults")}><div>{t('AUTOCOMPLETE.NO_RESULTS')}</div></li></ul>
        </div>
      </If>
    </div>
  );
}


 