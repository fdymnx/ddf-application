import React from 'react';
import clsx from "clsx";
import PropTypes from 'prop-types';
import formStyles from './FormStyles.js';
import globalStyles from '../../../assets/stylesheets/globalStyles.js';

Button.propTypes = {
  secondary: PropTypes.bool,
  /**
   * Default color is purple
   */
  colorTheme: PropTypes.string,
  /** 
   * By default the HTML element generated is <button>, but you can use another element or React Component, 
   * which will be applied the same styling
   */
  as: PropTypes.elementType,
  className: PropTypes.string,
};

export function Button({secondary, loading, colorTheme, as, className, ...otherProps}) {
  const ComponentToUse = as || 'button';
  const formStyle = formStyles({color: colorTheme});
  const gS = globalStyles();

  return (
    <ComponentToUse
      className={clsx(
        formStyle.button,
        secondary && "secondary",
        loading && gS.loadingOverlay,
        className,
      )}
      {...otherProps}
    />
  );
}
