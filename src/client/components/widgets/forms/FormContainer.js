import React from 'react';
import PropTypes from 'prop-types';
import {useStyles} from "./formik/Form";

FormContainer.propTypes = {
  /**
   * A color theming for the elements of the form.
   * Default color is purple
   */
  colorTheme: PropTypes.string,
};

/**
 *
 * A simple <form> component without extra functionnality (unlike "./formik/Form"),
 * but with the same styling
 */
export function FormContainer({colorTheme, ...otherProps}) {
  const classes = useStyles({color: colorTheme});
  return <form className={classes.form} {...otherProps} />;
}
