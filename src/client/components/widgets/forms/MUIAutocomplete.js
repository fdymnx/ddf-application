import React, {useState, useEffect} from 'react';
import useAutocomplete from "@material-ui/lab/useAutocomplete";
import {makeStyles} from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import InputBase from '@material-ui/core/InputBase';
import {useLazyQuery} from "@apollo/client";
import CircularProgress from "@material-ui/core/CircularProgress";
import useDebounce from '../../../hooks/useDebounce';
import Config from '../../../Config';


const resultBoxStyle = {
  position: "absolute",
  marginTop: 15,
  color: Config.colors.darkgray,
  backgroundColor: Config.colors.mediumgray,
  borderRadius: "30px",
  boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)",
  zIndex: 100
};

const useStyles = makeStyles((theme) => ({
  input: {
    width: "100%",
    height: "100%",
    color: "inherit",
    border: "none",
    fontFamily: "Raleway",
    textOverflow: "ellipsis",
  },
  inputRequiredPlaceholder: {
    width: "100%",
    height: "100%",
    border: "none",
    fontFamily: "Raleway",
    textOverflow: "ellipsis",
    "&::placeholder": {
      fontWeight: Config.fontWeights.semibold,
      color: Config.colors.darkpurple
    },
  },
  inputFocused: {
    "*:focus:not(.focus-visible)": {
      outline: "none"
    },
    "&:focus-visible": {
      outline: "#FFB400 solid 1px",
      outlineOffset: "2px",
      borderRadius: "2rem"
    }
  },

  listDiv: {
    width: "auto",
    listStyle: "none",
    overflow: "auto",
    maxHeight: "80vh",
    padding: "0px 20px 20px 20px",
    margin: "0 20px 0 0",
    zIndex: 99,
    scrollbarColor: `${Config.colors.white} ${Config.colors.purple}`,
    scrollbarWidth: 'thin',
    '& div ': {
      padding: '5px',
      display: 'flex',
      '& span': {
        flexGrow: 1
      }
    },
    /* '& div[aria-selected="true"]': { // element sélectionné
       backgroundColor: 'white',
       border: `1px solid ${Config.colors.mediumgray}`,
       borderBottomColor: Config.colors.purple
     },*/
    '& div[data-focus="true"]': {
      // lorsqu'on parcour avec la sourie/clavier
      color: Config.colors.purple,
      cursor: 'pointer'
    }
  },
  marginRight: {
    marginRight: "10px"
  },

  wrapper: {
    width: "100%"
  },
  box: {
    width: "auto"
  },

}));

/**
 * 
 * @param {Boolean} addForm : add or not a form surrounding the input. if you add one Form pressing the "enter" on keyboard will submit it . 
 * @param containerRef : used to compute it's size to display results the same width, 
 *          just add `ref={containerRef}` to a parent div with the good size 
 *          (and don't forget the `const containerRef = useRef(null);`) 
 * @param {string} inputAccessKey use to add a keyboard shortcut (search for AccessKey )
 */
export function MUIAutocomplete({
  inputName, inputAriaLabel, inputAccessKey, placeholder,
  gql, dataExtractor,
  value, handleChange = () => {console.log("MUIAutocomplete no handleChange() provided")}, handleSubmit = () => {console.log("MUIAutocomplete no handleSubmit() provided")}, showIsRequired,
  containerRef,
  startAdornment, endAdornment, inputClassName,
  addForm = true
}) {

  // to customize results of autocomplete div width
  const containerWidth = containerRef?.current?.offsetWidth || 300;

  const classes = useStyles();
  // this should be always a string, contain the QS query
  const [qs, setQs] = useState(value || '');
  // contain the AC query result's
  const [options, setOptions] = useState([]);
  // this should be always be an object, contain the selected value from AC ( label + id )
  const [selectedOption, setSelectedOption] = useState({label: value || ''});

  const [loadData, {loading}] = useLazyQuery(gql, {
    variables: {first: 10, qs},
    fetchPolicy: 'network',
    onCompleted: (data) => {
      setOptions(dataExtractor(data));
    }
  });

  let {
    getRootProps,
    getInputProps,
    getListboxProps,
    getOptionProps,
    groupedOptions,
    getOptionSelected,
    focused,
    setAnchorEl,
    anchorEl,
    inputValue
  } = useAutocomplete({
    id: inputName,
    value: {label: value || ""},
    options: options,
    getOptionLabel: (option) => {
      if (option?.sublabel) {
        return `${option.label} (${option.sublabel})`
      }
      return option.label || "";
    },
    getOptionSelected: (option, value) => {

      return option?.label?.toLowerCase() === value?.label?.toLowerCase() || option?.id === value?.id;
    },
    selectOnFocus: true,
    onInputChange: (event, newvalue, reason) => {
      // console.log("oninputchange", {newvalue, reason, event});
      // retourne une string newvalue à chaque changement de l'input lorsqu'on tape au clavier      
      if (reason === "clear" || (reason === "input" && newvalue === "" && !!value)) {
        handleChange({});
        setQs(null);
      } else if (reason !== "reset") {
        handleChange({label: newvalue, id: null, test: "eeee"});
        setQs(newvalue);
      }
    },
    onChange: (event, option) => {
      // console.log("onChange", {option, event})
      // option est l'object selectionné dans l'autocomplete 
      setQs(option?.label);
      setSelectedOption(option);
      handleChange(option);
      // submit only when value is not null, prevent to close 
      // autocomplete when user erase all input to type something else         
      if (!addForm && option?.label) {
        handleSubmit()
      }
    }
  });


  const inputProps = getInputProps();
  const handleFocus = inputProps.onFocus;
  inputProps.onFocus = (e) => {
    anchorEl.select();
    handleFocus(e);
  };

  const debouncedQs = useDebounce(inputValue, 500);
  useEffect(() => {
    if (debouncedQs) {
      loadData();
    } else {
      setOptions([])
    }
  }, [debouncedQs]);

  const resultBox = {...resultBoxStyle, width: containerWidth};

  function createInput(inputProps) {
    const inputStyle = showIsRequired ? classes.inputRequiredPlaceholder : classes.input;
    return <InputBase
      inputRef={setAnchorEl}
      inputProps={{"aria-label": "Localisation"}}
      fullWidth={true}
      autoCorrect="off"
      accessKey={inputAccessKey || null}
      classes={{focused: classes.inputFocused, input: inputStyle, root: inputClassName}}
      aria-required={showIsRequired}
      placeholder={placeholder}
      startAdornment={startAdornment}
      endAdornment={loading ? <CircularProgress size={20} className={classes.marginRight} /> : endAdornment}
      {...inputProps}
    />
  }

  return (
    <div className={classes.wrapper} id={'autocomplete-input'} >
      <div {...getRootProps()} className={classes.box}>
        {addForm ?
          <form onSubmit={handleSubmit} aria-label={inputAriaLabel}>
            {createInput(inputProps)}
          </form>
          :
          createInput(inputProps)
        }
      </div>

      {focused && groupedOptions.length > 0 && !loading && (
        <Paper {...getListboxProps()} style={resultBox} elevation={2}>
          <div className={classes.listDiv}>
            {renderResults(groupedOptions)}
          </div>
        </Paper>
      )}
    </div>
  );

  function renderResults(results) {
    return results.map((option, index) => {
      return (
        <div
          className={classes.listEntry}
          // fix dic-684
          /*onClick={() => handleSelectOption(option)}*/
          {...getOptionProps({option, index})}
        >
          <span>{option.label}</span>
        </div>
      )
    })
  }


  function handleSelectOption(option) {
    handleChange(option);
    setQs(option?.label);
    // fix dic-684
    setTimeout(() => {
      handleSubmit(null);
    }, 1000);

  }




}