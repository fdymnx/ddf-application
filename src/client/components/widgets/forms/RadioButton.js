import React from 'react';
import PropTypes from 'prop-types';

import formStyles from './FormStyles.js';


let uidCounter = 0;
function generateUid() {
  return `radiobutton-${uidCounter++}`;
}

RadioButton.propTypes = {
  label: PropTypes.node
};
export function RadioButton(props) {
  const formStyle = formStyles();
  const {label, ...otherProps} = props;
  let uid = generateUid();

  return (
    <div className={formStyle.checkboxRow}>
      <input
        id={uid}
        className={formStyle.customRadioButton}
        type="radio"
        {...otherProps}
      />
      <label htmlFor={uid}>{label}</label>
    </div>
  );
}
