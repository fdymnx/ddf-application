import React from 'react';

import {makeStyles} from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import clsx from 'clsx';
import Config from '../../Config';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme) => ({
  centerPagination: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginTop: '20px',
    marginBottom: '10px'
  },
  hide: {
    visibility: 'hidden'
  },
  ul: {
    "& .MuiPaginationItem-root": {
      color: Config.colors.darkpurple
    },
    "& .Mui-selected": {
      color: "#ff2882"
    }
  },
  text: {
    marginLeft: "20px",
    color: Config.colors.darkpurple,
    display: 'flex',
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  mb: {
    marginBottom: "4px"
  },
  ml: {
    marginLeft: "10px"
  }
}));


/**
 * Pagination 
 * @param {int} siblingCount : You can specify how many digits to display either side of current page with the siblingRange prop, 
 * and adjacent to the start and end page number with the boundaryRange prop.
 * @returns 
 */
export function Paginator({currentPage, pageCount, setCurrentPage, siblingCount = 1}) {
  const classes = useStyles();
  const hide = parseInt(pageCount) <= 1;
  return (
    <div key="pagination" className={clsx(classes.centerPagination, hide && classes.hide)}>

      <Pagination classes={{ul: classes.ul}} size="medium" siblingCount={siblingCount} page={currentPage} count={pageCount} onChange={(event, page) => setCurrentPage(page)} />

      {pageCount > 20 && <div className={classes.text}>
        <div className={classes.mb}>Page :</div>
        <div className={classes.ml}>
          <TextField
            id="standard-number"
            type="number"
            value={currentPage}
            onChange={(event) => {
              const newPage = parseInt(event.target.value);
              if (!isNaN(newPage)) {
                if (newPage >= 1 && newPage <= pageCount) {
                  setCurrentPage(newPage)
                }
              }
            }}
            style={{width: 65}}
            inputProps={{style: {color: Config.colors.darkpurple}}}
            InputProps={{disableUnderline: true}}

          />
        </div>
      </div>}
    </div>
  );
}

// 