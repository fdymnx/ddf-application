import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(2),
    minWidth: '200px',
    color: theme.palette.getContrastText('#8200a0'),
    backgroundColor: '#8200a0',
    '&:hover': {
      backgroundColor: '#600C88'
    },
    borderRadius:'1px'
  },
}));

export function PurpleButton({ children, ...otherProps }) {
  const classes = useStyles();

  return (
    <Button variant="contained" color="primary" className={classes.button} {...otherProps}>
      {children}
    </Button>
  );
}
