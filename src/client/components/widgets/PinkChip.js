

import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Chip from '@material-ui/core/Chip';
import clsx from 'clsx';
import Config from '../../Config';
import deleteIcon from "../../assets/images/cross_purple.svg";
import deleteIconGrey from "../../assets/images/cross_grey.svg";
import globalStyles from '../../assets/stylesheets/globalStyles.js';

const useStyles = makeStyles((theme) => ({
  pinkText: {
    backgroundColor: Config.colors.lightgray,
    color: Config.colors.purple,
    marginRight: '5px',
    marginTop: "5px",
    whiteSpace: 'normal',
    cursor: "inherit !important"
  },
  whiteBackground: {
    backgroundColor: Config.colors.white,
  },
  icon: {
    height: "12px",
    marginRight: Config.spacings.small
  }
}));



export default function PinkChip({label, isMine, onDelete = false, useWhiteBackground = false, ...props}) {

  const classes = useStyles();

  if (onDelete) {
    return <Chip
      className={clsx(classes.pinkText, useWhiteBackground && classes.whiteBackground)}
      label={label}
      {...props}
      deleteIcon={<TooltipIcon isMine={isMine} onDelete={onDelete} />}
      onDelete={() => {null}}
    />
  } else {
    return <Chip
      className={clsx(classes.pinkText, useWhiteBackground && classes.whiteBackground)}
      label={label}
      {...props}
    />
  }
}

function TooltipIcon({isMine, onDelete}) {
  const classes = useStyles();
  const gS = globalStyles();
  if (isMine) {
    return <img className={clsx(classes.icon, gS.clickable)} src={isMine ? deleteIcon : deleteIconGrey} alt="Effacer" onClick={onDelete} />
  } else {
    return <Tooltip title={isMine ? null : "Impossible d'éditer une information dont vous n'êtes pas l'auteur."}>
      <img className={classes.icon} src={isMine ? deleteIcon : deleteIconGrey} alt="Effacer" />
    </Tooltip>
  }
}