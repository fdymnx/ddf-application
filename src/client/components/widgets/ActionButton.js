import React from 'react';
import clsx from 'clsx';
import CircularProgress from "@material-ui/core/CircularProgress";
import {makeStyles} from '@material-ui/core/styles';
import DivButton from './DivButton';
import MyImg from "./MyImg";
import Config from "../../Config";

const useStyles = makeStyles((theme) => ({
  actionButton: {
    flexShrink: 0,
    display: "flex",
    padding: "5px",
    margin: "2px 6px 2px 0px",
    boxSizing: "border-box",
    borderRadius: "50px",
    height: "35px",
    minWidth: "35px",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    padding: "1px",
    height: "20px",
    width: "20px"
  },
  iconPaddingRight: {
    marginRight: "8px"
  },
  paddingBig: {
    padding: "0px 10px"
  },
  unactive: {
    opacity: 0.5
  },
}));


/**
 * Usage :
 *
 * <ActionButton
 *    text="text"
 *    icon={iconSrc}
 *    color="orange"
 * />
 *
 * Properties :
 *
 * - text
 * - icon
 * - color: is one of "orange" or "purple" for choosing the right colored svg
 * - showText : show or not the text used also as title
 * - unactive : currently active/enabled or not , to fill or not the cercle with color
 *  i was using MySvg with just color Code in hexa before but it fail JEST test. so have to add all the colored svg...
 * 
 */
export function ActionButton({color, loading = false, unactive, text, showText, onClick, iconName}) {

  const classes = useStyles();
  // colorCode : is hexa code used to fill or create border button style 
  const colorCode = Config.colors[color];
  const style = {
    border: `2px solid ${colorCode}`,
    backgroundColor: unactive ? "white" : colorCode,
    color: unactive ? colorCode : "white"
  }
  const iconColor = unactive ? color : "white";

  return (
    <DivButton
      onClick={!loading ? onClick : undefined}
      title={text}
      aria-label={text}
      classesName={clsx(
        classes.actionButton,
        unactive && classes.unactive,
        showText && classes.paddingBig
      )}
      style={style}
    >
      <>
        {iconName &&
          <div className={clsx(classes.icon, showText && classes.iconPaddingRight)}>
            {loading ?
              <CircularProgress size={20} style={{'color': iconColor}}  />
              :
              <MyImg iconName={iconName} color={iconColor} />
            }
          </div>
        }
        {showText && text}
      </>
    </DivButton>
  );


}
/*
<MySvg iconName={iconName} fill={iconColor} stroke={iconColor} />
<img className={classNames(style.icon, theme?.icon)} src={icon} alt="Action" />


*/