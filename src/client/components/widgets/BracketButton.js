import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import CircularProgress from "@material-ui/core/CircularProgress";
import clsx from 'clsx';
import Config from '../../Config';
import globalStyles from '../../assets/stylesheets/globalStyles.js';

BracketButton.propTypes = {
  /**
   * This class name will be applied to the top level DOM element of this component
   */
  classNameButton: PropTypes.string,
  classNameText: PropTypes.string,
  text: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  loading: PropTypes.bool
};



const borderWidth = "3px";
const useStyles = makeStyles((theme) => ({

  button: {
    display: "flex",
    justifyContent: 'space-between',
    alignItems: 'center',
    border: `${borderWidth} solid ${Config.colors.purple}`,
    backgroundColor: "white",
    cursor: "pointer",
    fontFamily: "Raleway",
    fontWeight: Config.fontWeights.semibold,
    fontSize: `${Config.fontSizes.large}px`,
    minHeight: '30px'
  },
  text: {
    marginLeft: `7px`,
    marginRight: `7px`,

    paddingLeft: borderWidth,
    paddingRight: borderWidth,

    paddingTop: `${Config.spacings.tiny}px`,
    paddingBottom: `${Config.spacings.tiny}px`,

    marginTop: `-${borderWidth}`,
    marginBottom: `-${borderWidth}`,

    backgroundColor: Config.colors.white,
    color: Config.colors.black,
    minHeight: '30px',
    display: "flex",
    alignSelf: "center",
    alignItems: "center"
  },
  disabledText: {
    color: Config.colors.verydarkgray
  },
  greyBg: {
    backgroundColor: Config.colors.lightgray
  }

}));



/**
 *
 * Display a button with "[ Rechercher ]" style
 * Usage :
 *  
 * <BracketButton 
 *  text="Rechercher"
 *  disabled={false}
 *  loading={false}
 *  onClick={() => this.performSearch()}
 *  classNameButton={classes.searchButton}
 *  classNameText={classes.text}
 *  greyBg={bool}
 * />
 */

export function BracketButton({text, ariaLabel, onClick, greyBg = false, classNameButton, classNameText, disabled = false, loading = false,...props}) {
  const classes = useStyles();
  const gS = globalStyles();

  return (
    <button className={clsx(classes.button, gS.outlineFocus, greyBg && classes.greyBg, classNameButton)}
      onClick={onClickHandler} disabled={disabled}
      aria-label={ariaLabel || text}
      {...props}>
      <div className={clsx(classes.text, greyBg && classes.greyBg, disabled && classes.disabledText, classNameText)}>
        {text}
        {loading && <CircularProgress size={20} style={{'color': Config.colors.verydarkgray, marginLeft: '10px'}} />}
      </div>
    </button>
  );

  function onClickHandler() {
    if (onClick) {
      onClick();
    }
  }
}
