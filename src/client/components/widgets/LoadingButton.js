/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import clsx from "clsx";
import {makeStyles} from "@material-ui/core/styles";
import {Button, CircularProgress} from "@material-ui/core";
import {useMediaQueries} from '../../layouts/MediaQueries';


const useStyles = makeStyles((theme) => ({
  loadingButton: {
    display: "inline-block",
    position: "relative"
  },
  buttonProgress: {
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12
  },
  smallButton: {
    padding: "4px 12px",
    minWidth: "32px"
  }
}));

// show a button with loading gif when action in progress
export function LoadingButton({loading, ...props} = {}) {
  const classes = useStyles();
  const {isMobile} = useMediaQueries() ; 

  return (
    <div className={classes.loadingButton}>
      <Button {...props} className={clsx( props.className, isMobile && classes.smallButton )} disabled={props.disabled || loading} />
      <If condition={loading}>
        <CircularProgress size={24} className={classes.buttonProgress} />
      </If>
    </div>
  );
}
