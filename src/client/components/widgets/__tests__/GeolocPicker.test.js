import React from 'react';
import {cleanup, fireEvent} from '@testing-library/react';
import {act} from 'react-dom/test-utils';
import {Subject, throwError} from 'rxjs';

import {renderWithMocks} from '../../../../../jest/utilities/renderWithMocks';
import {GeolocPicker} from '../GeolocPicker';
import {GeolocServiceMock} from '../../../services/__mocks__/GeolocService.mock';
import {NEVER, EMPTY} from 'rxjs';

class CortexNotificationServiceMock {
  constructor () {
    this.showLoadingBar = jest.fn();
    this.hideLoadingBar = jest.fn();
    this.alert = jest.fn();
    this.alertMutationError = jest.fn();

    this.loadingBarMessage = NEVER;
    this.loadingBarValue = NEVER;
    this.loadingBarDisplayStatus = NEVER;
    this.alertMessage = NEVER;
  }
}

let geolocServiceMock, notificationServiceMock;
let findByTestId;
let queryByTestId;


beforeEach(() => {
  geolocServiceMock = new GeolocServiceMock();
  notificationServiceMock = new CortexNotificationServiceMock();
  ({findByTestId, queryByTestId} = renderWithMocks({
    element: <GeolocPicker geolocService={geolocServiceMock} notificationService={notificationServiceMock} />,
  }));
});

afterEach(cleanup);

/* Disable console.warn for these tests, to avoid polluting the test output (console logging is to be expected) */
let consolewarn;
beforeAll(() => {
  consolewarn = console.warn;
  console.warn = jest.fn();
});
afterAll(() => {
  console.warn = consolewarn;
});

describe('auto geoloc button', () => {
  let autoGeolocButton, resultSubject;

  async function getAutoGeolocButton() {
    resultSubject = new Subject();
    geolocServiceMock.setAutoPlace.mockImplementation(() => resultSubject);
    autoGeolocButton = await findByTestId('autoGeolocButton');
    return autoGeolocButton;
  }

  describe('loading animation', () => {
    it('has loading animation while the request is pending', async () => {
      autoGeolocButton = await getAutoGeolocButton();
      fireEvent.click(autoGeolocButton);
      expect(await queryByTestId('autoGeolocLoading')).not.toEqual(null);
      act(() => {
        resultSubject.complete();
      });
      expect(await queryByTestId('autoGeolocLoading')).toEqual(null);
    });

    it('removes the loading animation if the request errors', async () => {
      autoGeolocButton = await getAutoGeolocButton();
      fireEvent.click(autoGeolocButton);
      expect(await queryByTestId('autoGeolocLoading')).not.toEqual(null);
      act(() => {
        resultSubject.error('my error');
      });
      expect(await queryByTestId('autoGeolocLoading')).toEqual(null);
    });

  });

  describe('error messages', () => {
    it('displays specific message when user denied permission', async () => {
      autoGeolocButton = await getAutoGeolocButton();
      geolocServiceMock.setAutoPlace.mockImplementation(() =>
        throwError({
          status: 'browser_geolocation_error',
          error: {
            code: 1,
            message: 'User denied Geolocation',
          },
        })
      );
      fireEvent.click(autoGeolocButton);

      expect(await notificationServiceMock.alert.mock.calls[0][0]).toEqual("Nous n'avons pas la permission d'accéder à votre localisation");
    });

    it('displays generic message when error is unknown', async () => {
      autoGeolocButton = await getAutoGeolocButton();
      geolocServiceMock.setAutoPlace.mockImplementation(() =>
        throwError({
          any: 'error',
        })
      );
      fireEvent.click(autoGeolocButton);
      expect(notificationServiceMock.alert.mock.calls[0][0]).toEqual('Un problème nous empêche de vous géolocaliser');
    });

    it("doesn't display message when geoloc is aborted", async () => {
      autoGeolocButton = await getAutoGeolocButton();
      geolocServiceMock.setAutoPlace.mockImplementation(() =>
        throwError({
          status: 'aborted',
        })
      );
      fireEvent.click(autoGeolocButton);
      expect(notificationServiceMock.alert.mock.calls.length).toEqual(0);
    });

  });
});
