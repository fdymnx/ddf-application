import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';


const TooltipWithStyle = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 400,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip);


export default function HtmlTooltip({title, children}) {
  return <TooltipWithStyle title={
    <React.Fragment>
      <Typography color="inherit">{title}</Typography>
    </React.Fragment>
  } >
    {children}
  </TooltipWithStyle>
}