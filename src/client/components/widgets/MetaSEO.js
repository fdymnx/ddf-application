import React from 'react';
import {Helmet} from 'react-helmet-async';
import {justPurifyHtml} from '../../utilities/PurifyHtml';

//  La commande "import './assets/images/ddf_logo_squared.png'" ne fonctionne pas en compilation côté NodeJS.
//  Il faudrait utiliser un plugin babel de type "file-loader" utilisé dans Webpack.
//  Elle donc est reportée dans main.js pour avoir la ressource dans les assets compilés.
// TODO: Trouver un workaround qui fonctionne côté SSR.
const ddfIcon = '/a/ddf_logo_squared.png';

/**
 * Add meta information for SEO and facebook and twitter accounts
 * @param {string} [url]
 * @param {string} [title]
 * @param {string} [description]
 * @param {string} [ogDescription]
 * @param {string} name optionnal field
 * @param {boolean} addScript optionnal
 * @param {object} extraLabels
 * @param {array} keywords
 */
export function MetaSEO({title, description, ogDescription, name = null, addScript = false, url, extraLabels = {}, keywords} = {}) {

  if (description) {
    description = justPurifyHtml(description);
  }

  if (ogDescription) {
    ogDescription = justPurifyHtml(ogDescription);
  } else {
    ogDescription = description;
  }

  if (typeof window !== "undefined") {
    url = window.location.href;
  }

  const baseUrl = (new URL(url)).origin;

  const scripts = !addScript ? [{
    "testid": 1,
    "@context": "https://schema.org",
    "@type": "WebSite",
    "@id": "https://www.dictionnairedesfrancophones.org/#website",
    "url": "https://www.dictionnairedesfrancophones.org",
    "name": "Dictionnaire des francophones",
    "potentialAction": {
      "@type": "SearchAction",
      "target": "https://www.dictionnairedesfrancophones.org/form/{search_term_string}",
      "query-input": "required name=search_term_string"
    }
  },
  {
    "testid": 2,
    "@context": "https://schema.org",
    "@type": ["DefinedTermSet", "Book"],
    "@id": "https://www.dictionnairedesfrancophones.org",
    "name": "Dictionnaire des francophones"
  }] :
    [
      {
        "testid": 3,
        "@context": "https://schema.org",
        "@type": "WebPage",
        "name": title,
        "url": url,
        "description": description,
        "isPartOf": {
          "@id": "https://www.dictionnairedesfrancophones.org/#website"
        }
      },
      {
        "testid": 4,
        "@context": "https://schema.org",
        "@type": "WebSite",
        "@id": "https://www.dictionnairedesfrancophones.org/#website",
        "url": "https://www.dictionnairedesfrancophones.org",
        "name": "Dictionnaire des francophones",
        "potentialAction": {
          "@type": "SearchAction",
          "target": "https://www.dictionnairedesfrancophones.org/form/{search_term_string}",
          "query-input": "required name=search_term_string"
        }
      },
      {
        "testid": 5,
        "@context": "https://schema.org",
        "@type": ["DefinedTermSet", "Book"],
        "@id": "https://www.dictionnairedesfrancophones.org",
        "name": "Dictionnaire des francophones"
      },
      {
        "testid": 6,
        "@context": "https://schema.org",
        "@type": "DefinedTerm",
        "@id": url,
        "name": name || description,
        "description": description,
        "inDefinedTermSet": "https://www.dictionnairedesfrancophones.org"
      }
    ];


  removeNode("helmet-schema.org");
  removeNode("helmet-schema.org-index");

  return (
    <Helmet >
      <If condition={!baseUrl.includes("www.dictionnairedesfrancophones.org")}>
        <meta name="robots" content="noindex, nofollow" />
      </If>

      <title>{title}</title>
      {description &&
        <meta name="description" content={description} />
      }

      <If condition={keywords?.length > 0}>
        <meta name="keywords" content={keywords.join(", ")} />
      </If>

      <meta property="og:title" content={title} />
      <meta name="twitter:title" content={title} />

      <meta property="og:url" content={url} />
      <meta name="twitter:url" content={url} />

      <meta property="og:image" content={baseUrl + ddfIcon} />
      <meta name="twitter:image" content={baseUrl + ddfIcon} />
      <meta property="og:image:width" content="1211" />
      <meta property="og:image:height" content="1211" />

      {/* don't use {description && <> <meta/> <meta/> <meta/> </>} , it don't work */}
      {ogDescription &&
        <meta property="og:description" content={ogDescription} />
      }
      {ogDescription &&
        <meta name="twitter:description" content={ogDescription} />
      }

      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content="@DFrancophones" />
      <meta property="og:type" content="website" />

      {Object.keys(extraLabels).map((label, index) => (
        <meta key={`twitter:label${index + 1}`} name={`twitter:label${index + 1}`} content={label} />
      ))}

      {Object.values(extraLabels).map((data, index) => (
        <meta key={`twitter:data${index + 1}`} name={`twitter:data${index + 1}`} content={data} />
      ))}

      <script id="helmet-schema.org" type="application/ld+json">
        {JSON.stringify(scripts)}
      </script>
    </Helmet>
  );

  function removeNode(id) {
    // If SSR document does not exist.
    if(typeof document !== "undefined"){
      const elem = document.getElementById(id);
      if (elem?.parentNode) {
        elem.parentNode.removeChild(elem)
      }
    }
  }
}
