import React from 'react';
import {MUIAutocomplete} from "./forms/MUIAutocomplete";
import {gql} from "@apollo/client";

import {unique} from "../../utilities/helpers/tools";

export const gqlSearchPersons = gql`
 query SearchPersons($qs: String!, $first: Int){
  persons(qs: $qs, first: $first) {
      edges {
        node {
          id
          nickName
          userAccount{
            id
            username            
          }
        }
      }
    }
 }
`;

export function PersonAutocompleteMUIA({labelCreator, inputName, ...props}) {

  return (
    <MUIAutocomplete
      {...props}
      inputName={inputName || "person-autocomplete"}
      inputAriaLabel="Saisir le nom à rechercher"
      gql={gqlSearchPersons}
      dataExtractor={(data) => {
        /* strictSearch Qs search is used to get results most of the time, but it return unwanted data, 
        *             for exemple search "Alain" will return ["Alain", ... "alpha314"] 
        *             using strictSearch remove entries that did'nt strictly contain the user input
        */
        // filter + uniq + sort

        let items = unique(data?.persons?.edges?.map((node) => labelCreator(node)), 'label').sort((a, b) => (a.label > b.label ? 1 : -1))
        if (props?.value && props.value.length > 0) {
          return items.filter(node => node?.label?.toLowerCase()?.includes(props.value.toLowerCase()))
        } else {
          return items;
        }

      }}
    />
  );
}
