import {useState, useEffect} from 'react';
import {useLazyQuery} from '@apollo/client';
import invariant from 'invariant';
import get from 'lodash/get';
import set from 'lodash/set';

/**
 * Enhance apollo's useQuery, when used on a query with a field acting as a connection (relay style pagination).
 *
 * This hooks returns the same object than useQuery, with the additionnal properties : 
 *
 * - loadNextPage (function) : performs a fetchMore() with the correct variable to load the next connection page, and do the correct updateQuery() so that
 *                  the connection data returned by the hook is updated out of the box
 * - hasNextPage (boolean) : equivalent of data[connectionPath].pageInfo.hasNextPage, for a more practical use
 *
 * @param query - gql query, same as useQueryt
 * @param options {Object} - Options object, has the same options than useQuery (will be passed as is), plus a few additionnal options described below
 * @param options.pageSize {number} 
 * @param options.connectionPath {string} - The path to access the connection field in the data object returned by the query
 */
export function useLazyPaginatedQuery(query, options = {}) {
  let {connectionPath, pageSize, ...otherOptions} = options;
  if (!otherOptions.variables) {
    otherOptions.variables = {};
  }
  otherOptions.variables.first = pageSize;
  let [fetch, useQueryReturnObject] = useLazyQuery(query, otherOptions);
  let [hasNextPage, setHasNextPage] = useState();
  let [refetching, setRefetching] = useState(false);

  invariant(connectionPath, "You must provide the option 'connectionPath'");
  invariant(pageSize, "You must provide the option 'pageSize'");

  let {fetchMore, data, loading} = useQueryReturnObject;
  let {variables} = otherOptions;
  let connectionField = get(data, connectionPath);
  useEffect(() => {
    setHasNextPage(connectionField?.pageInfo?.hasNextPage);
  }, [connectionField?.pageInfo?.hasNextPage]);

  let loadNextPage = ({variables = {}} = {}) => {
    setRefetching(true);
    fetchMore({
      variables: {
        ...otherOptions?.variables,
        ...variables,
        after: connectionField.pageInfo.endCursor,
      },
      updateQuery: (previousResult, {fetchMoreResult}) => {
        setRefetching(false);
        let newResultConnectionField = get(fetchMoreResult, connectionPath);
        let previousResultConnectionField = get(previousResult, connectionPath);
        const newEdges = newResultConnectionField.edges;
        const pageInfo = newResultConnectionField.pageInfo;
        setHasNextPage(pageInfo.hasNextPage);

        if (newEdges.length) {
          let newResult = Object.assign({}, previousResult);
          set(newResult, connectionPath, {
            __typename: previousResultConnectionField.__typename,
            edges: [...previousResultConnectionField.edges, ...newEdges],
            pageInfo
          });
          return newResult;
        } else {
          return previousResult;
        }
      }
    });
  }

  useQueryReturnObject.loadNextPage = loadNextPage;
  useQueryReturnObject.hasNextPage = hasNextPage;
  useQueryReturnObject.loading = loading || refetching;
  return [fetch, useQueryReturnObject];
}
