import { gql} from "@apollo/client";

export const gqlRefreshLexicalSenseReviewedStatus = gql`
  query lexicalSenseReviewedStatus($lexicalSenseId: ID!) {
    lexicalSense(id: $lexicalSenseId) {
      id
      reviewed
    }
  }
`;
