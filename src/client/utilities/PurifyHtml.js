import React from 'react';
import { useHistory } from 'react-router-dom';
import DOMPurify from 'isomorphic-dompurify';

DOMPurify.addHook('afterSanitizeAttributes', function (node) {
  if (node.hasAttribute('href')) {
    if (node.href && !isLocalLink(node.href)) {
      node.setAttribute('target', '_blank');
      // prevent https://www.owasp.org/index.php/Reverse_Tabnabbing
      node.setAttribute('rel', 'noopener noreferrer');
    } else {
      /**
       * dans sanitizedHtml il y avait ca : pas reussi a trouver un cas d'usage
       * //si on n'enleve pas les <i></i> ca crée des liens du type "/form/<i>poma</i>" par exemple
       */
      node.href = node.href.replace('<i>', '').replace('</i>', '').replace('%3Ci%3E', '').replace('%3C/i%3E', '');
    }
  }
});

function isLocalLink(href) {
  let targetHostname = new URL(href, window.location).hostname;
  return window.location.hostname === targetHostname;
}

function _purifyHtml(props = {}) {
  const { as, html, className, forwardedRef } = props;
  const ContainerComponent = as || 'div';
  let history = useHistory();
  history = props.history || history;

  return (
    <ContainerComponent
      data-testid="purified-html-container"
      onClick={clickHandlerForNavigation}
      className={className}
      dangerouslySetInnerHTML={{ __html: customPurifyHtml(html) }}
      ref={forwardedRef}
    />
  );

  function customPurifyHtml(input) {
    const { removeAllowedTags = [] } = props;
    let defaultAllowedTags = [
      'b',
      'em',
      'i',
      'small',
      'strong',
      'sub',
      'sup',
      'ins',
      'del',
      'mark',
      'h3',
      'h4',
      'h5',
      'h6',
      'blockquote',
      'p',
      'a',
      'ul',
      'ol',
      'nl',
      'li',
      'strike',
      'code',
      'hr',
      'br',
      'caption',
      'pre',
    ];
    let allowedTags = defaultAllowedTags.filter((tag) => !removeAllowedTags.includes(tag));
    return DOMPurify.sanitize(input, {
      ALLOWED_TAGS: allowedTags,
      ALLOWED_ATTR: ['href', 'target'],
    });
  }

  function clickHandlerForNavigation(e) {
    const targetLink = e.target.closest('a');
    if (!targetLink || !isLocalLink(targetLink.href)) return;
    e.preventDefault();
    history.push(targetLink.pathname);
  }
}

export const PurifyHtml = React.forwardRef((props, ref) => {
  const InnerComponent = _purifyHtml;
  return <InnerComponent forwardedRef={ref} {...props} />;
});

// remove all html
export function justPurifyHtml(input) {
  return DOMPurify.sanitize(input, {ALLOWED_TAGS: ['b'], ALLOWED_ATTR: []});
}
