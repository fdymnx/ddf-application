import React from 'react';

import {makeStyles} from '@material-ui/core/styles';
import Config from '../../Config';
import validIcon from '../../assets/images/signs/checkmark_green.svg';


const useStyles = makeStyles((theme) => ({
  snackContainer: {
    backgroundColor: Config.colors.purple,
    color: Config.colors.white,
    padding: '10px 20px',
    minWidth: '260px',
    borderRadius: '8px',
    fontSize: "0.875rem",
    boxShadow: "0px 3px 5px -1px rgb(0 0 0 / 20%), 0px 6px 10px 0px rgb(0 0 0 / 14%), 0px 1px 18px 0px rgb(0 0 0 / 12%)",
    alignItems: "center",
    fontFamily: "Roboto,Helvetica,Arial,sans-serif",
    fontWeight: "400",
    lineHeight: "1.43",
    borderRadius: "4px",
    letterSpacing: "0.01071em",
    display: 'flex',
    justifyContent: 'start'
  },
  icon: {
    height: "12px",
    marginRight:"6px"
  },
}));

export function showSnackbar(message, enqueueSnackbar) {

  const classes = useStyles();

  enqueueSnackbar(message, {
    content: (id, message) => {
      return (<div className={classes.snackContainer}>
        <img className={classes.icon} src={validIcon} />
        {message}
      </div>);
    },
    anchorOrigin: {
      vertical: "bottom",
      horizontal: "right"
    }
  });
}