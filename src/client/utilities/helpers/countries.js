import {gql} from '@apollo/client';
import uniq from 'lodash/uniq';

/**
 * An object that contains the data needed for the UI, relative to a list of countries associated to a localized entity.
 *
 * @typedef {Object} CountryData
 * @property {Array<string>} names  - The list of country names
 * @property {string} color         - The color associated with the localisation. TODO list the possible color values.
 *                                    'green', 'orange', 'yellow', 'pink', 'black', 'purple', 'gray'
 *
 */

/**
 * @param {string} colorName The color name as expected in the database (in french, with specific color name like "vert d'eau").
 * @return {string} The generic CSS class name describing the color ('green', 'orange', 'yellow', 'pink', 'black', 'purple', 'gray')
 */
export function colorToCssClassName(colorName) {
  switch (colorName) {
    case 'jaune':
      return 'yellow';
    case "vert d'eau":
      return 'green';
    case 'rose':
      return 'pink';
    case 'orange':
      return 'orange';
    case 'noir':
      return 'black';
    default:
      return null;
  }
}

/**
 * get an array of places (with countryName, stateName & name ), return a tree with
 *  first level : the countryName,
 *  second level: the stateName
 *  and third level : the (city) name
 * @param { array } places : array of object places
 */
export function getLocalizedTreePlaces(places) {
  let resTree = {};

  function addCountry(name, place) {
    if (!resTree[name]) {
      resTree[name] = {...place, states: {}};
    }
  }

  function addState(countryName, stateName, place) {
    if (!resTree[countryName].states[stateName]) {
      resTree[countryName].states[stateName] = {...place, cities: {}};
    }
  }

  function addCity(countryName, stateName, cityName, place) {
    if (!resTree[countryName].states[stateName].cities[cityName]) {
      resTree[countryName].states[stateName].cities[cityName] = place;
    }
  }

  places.map((place) => {
    let {countryName, stateName, __typename, name} = place;

    switch (__typename.toLowerCase()) {
      case 'country':
        // {id: 'geonames:2395170', name: 'France', color: 'jaune', __typename: 'Country' },
        addCountry(name, place);

        break;
      case 'state':
        /*
         { name: 'Québec', color: "vert d'eau", countryName: 'Canada', __typename: 'State' },
        */
        addCountry(countryName, place);
        addState(countryName, name, place);
        break;
      case 'city':
      default:
        /* { __typename: 'City',color: 'orange', countryName: 'Italy',stateName: null , name: 'venice' },
        { __typename: 'City',color: 'rose', countryName: 'France', stateName: 'Nouvelle-Aquitaine', name: 'Bordeaux' }  */

        addCountry(countryName, place);
        addState(countryName, stateName, place);
        addCity(countryName, stateName, name, place);

        break;
    }
  });

  return resTree;
}

/**
 * Returns a list of localisation names, depending on number of localisation return names or region
 *
 * https://mnemotix.atlassian.net/browse/DIC-437
 * si il y a autant de region que de ville autant afficher les villes directement
 * si il y a plus de ville que de region mais nbr villes inférieur à maxNumber ( differente selon mobile:4  ou deskto:7) on affiche quand meme les villes
 *
 * dans tout les cas si pas de name alors StateName sinon countryName
 * @param places
 * @return {*}
 */
export function getLocalizedEntityData(places = {}, maxNumber = 7) {
  places = places?.edges.map(({node}) => node);

  // on recupère les valeurs de names
  const placeNames = places
    .map((place) => place.name || place.stateName || place.countryName)
    .filter((value, index, array) => array.indexOf(value) === index)
    .sort();

  // on recupère les valeurs de regions
  const placeStateNames = places
    .map((place) => place.stateName || place.countryName || place.name)
    .filter((value, index, array) => array.indexOf(value) === index)
    .sort();

  let names;

  if (placeNames.length <= placeStateNames.length || (placeNames.length > placeStateNames.length && placeNames.length <= maxNumber)) {
    names = placeNames;
  } else {
    names = placeStateNames;
  }

  return {
    names,
    color: colorFromPlaceList(places),
  };
}

/**
 * Returns a list of country names.
 *
 * @borrows gqlFormCountriesFragment . GraphQL FormCountriesFragment must be added in in localizedEntity GraphQL query.
 *
 * @param localizedEntity
 * @return {*}
 */
export function getLocalizedEntityCountryData(localizedEntity) {
  let places = localizedEntity.places.edges.map(({node}) => node);
  let placeNames = places.map((place) => place.countryName || place.name);
  /* Filter out duplicate country names */
  placeNames = placeNames.filter((value, index, array) => array.indexOf(value) === index).sort();

  return {
    names: placeNames,
    color: colorFromPlaceList(places),
  };
}

/*
 * Extracts the color to use for displaying the group of countries given in input
 *
 * @param {Array<Place>} places - An array of Place object as returned when using the gqlCountryFragment
 * @return {string} A string describing a color (see CountryData.color}
 */
export function colorFromPlaceList(places) {
  if (!places.length) return 'gray';
  let colors = uniq(places.map((place) => colorToCssClassName(place.color)).filter((color) => color));
  return colors.length == 1 ? colors[0] : 'purple';
}

/**
 * Extract the id corresponding to the country of the place. In order to handle cases where place is directly the country
 * (in this case we want place.id) or a lower geographical division (in this case we want place.countryId).
 *
 *
 * @param {Place} place - A place object
 */
export function getPlaceCountryId(place) {
  return place && (place.countryId || place.id);
}

/**
 * Returns the GQL fragment corresponding to `getLocalizedEntityCountries` helper.
 * @type {gql}
 */

export let gqlCountryFragment = gql`
  fragment CountryFragment on PlaceInterface {
    id
    name
    color
    countryCode
    updatedAt
    ... on City {
      stateName
      countryId
      countryName
    }
    ... on State {
      countryId
      countryName
    }
  }
`;

/**
 * Returns the GQL fragment corresponding to `getFormCountries` helper.
 * @type {gql}
 */
export let gqlFormCountriesFragment = gql`
  fragment FormCountriesFragment on Form {
    places {
      edges {
        node {
          id
          ...CountryFragment
        }
      }
    }
  }

  ${gqlCountryFragment}
`;

/**
 * Returns the GQL fragment corresponding to `getLexicalSenseCountries` helper.
 * @type {gql}
 */
export let gqlLexicalSenseCountriesFragment = gql`
  fragment LexicalSenseCountriesFragment on LexicalSense {
    places {
      edges {
        node {
          id
          ...CountryFragment
        }
      }
    }
  }
  ${gqlCountryFragment}
`;



/**
 * Check if a place object is valid to be exploited by the application as the user current location.
 * It needs to have at least an id and a name.
 */
export function isValidPlace(place) {
  return !!(place && place.id && place.name);
}
