import React from "react";

/**
 * This is the top level HTML component
 */
export function Html({content, state, helmet = {}, chunkExtractor} = {}) {
  const htmlAttrs = helmet?.htmlAttributes?.toComponent() || {};
  const bodyAttrs = helmet?.bodyAttributes?.toComponent() || {};

  return (
    <html {...htmlAttrs}>
      <head>
        <Choose>
          <When condition={helmet?.title}>
            {helmet?.title?.toComponent()}
          </When>
          <Otherwise>
            <title>{"Dictionnaire des francophones"}</title>
          </Otherwise>
        </Choose>

        {helmet?.meta?.toComponent()}
        {helmet?.link?.toComponent()}
        {helmet?.script?.toComponent()}

        <meta charSet="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i|Raleway:400,500,600,700&display=swap" />
      </head>
      <body {...bodyAttrs}>
        <div id="react-root" dangerouslySetInnerHTML={{
          __html: content,
        }}/>

        <script dangerouslySetInnerHTML={{
          __html: `window.__APOLLO_STATE__=${JSON.stringify(state).replace(/</g, '\\u003c')};`,
        }} />
        {chunkExtractor?.getScriptElements()}
      </body>
    </html>
  );
}