export function NotistackServiceMock() {
  return {
    enqueueSnackbar: () => jest.fn(),
    closeSnackbar: () => jest.fn(),
  };
}
