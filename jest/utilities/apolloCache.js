import {inMemoryCache} from "../../src/client/services/DDFApolloClient"

export function generateApolloCache() {
  return inMemoryCache;
}

 