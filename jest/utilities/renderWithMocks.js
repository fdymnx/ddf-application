import React from 'react';
import {render} from '@testing-library/react';
import {MemoryRouter, Route} from 'react-router-dom'
import {CustomMockedProvider} from './CustomMockedProvider';
import {generateApolloCache} from './apolloCache';



/**
 * This helper function uses render() from @testing-library/react. It has side effects and it's important to call
 * cleanup() after the test in your test suite
 *
 */
export function renderWithMocks({element, locationPath, gqlMocks, routePath}) {
  locationPath = locationPath || "/";
  routePath = routePath || "/";
  let apolloCache = generateApolloCache();

  return render(
    <MemoryRouter initialEntries={[locationPath]}>
      <CustomMockedProvider mocks={gqlMocks} cache={apolloCache}>
        <Route path={routePath} render={() => element}/>
      </CustomMockedProvider>
    </MemoryRouter>
  );
}
