/* This jest transform module is meant to mock javascript imports of static asset files, for example 
 *
 * import myIcon from './icon.svg';
 *
 */

const path = require('path');

module.exports = {
  process(src, filename, config, options) {
    return 'module.exports = ' + JSON.stringify(path.basename(filename)) + ';';
  },
};
