/* 
 * We disable the component Helmet from 'react-helmet-async', otherwise every render of a component that have a Helmet 
 * in its children needs to be wrapped in a HelmetProvider component.
 */

export function setupReactHelmetAsync() {
  jest.mock('react-helmet-async', () => {
    return {
      __esModule: true, 
      Helmet: () => null
    };
  })
}
